module.exports = {
  servers: {
    one: {
      // TODO: set host address, username, and authentication method
      host: '206.189.37.148', // Che Pheap SR, Te Taily
      username: 'root',
      pem: '~/.ssh/id_rsa',
      // password: 'server-password'
      // or neither for authenticate from ssh-agent
    },
  },

  app: {
    // TODO: change app name and path
    name: 'restaurant-pheap',
    // name: 'restaurant-taily',
    path: '../',
    volumes: {
      '/data/file_uploads': '/data/file_uploads',
    },

    servers: {
      one: {},
    },

    buildOptions: {
      serverOnly: true,
    },

    env: {
      // TODO: Change to your app's url
      // If you are using ssl, it needs to start with https://
      // Pheap
      ROOT_URL: 'http://206.189.37.148:3030',
      MONGO_URL: 'mongodb://mongodb/restaurant-pheap',
      MONGO_OPLOG_URL: 'mongodb://mongodb/local',
      PORT: 3030,

      // Taily
      // ROOT_URL: 'http://206.189.37.148:5555',
      // MONGO_URL: 'mongodb://mongodb/restaurant-taily',
      // MONGO_OPLOG_URL: 'mongodb://mongodb/local',
      // PORT: 5555,
    },

    docker: {
      // change to 'abernix/meteord:base' if your app is using Meteor 1.4 - 1.5
      image: 'abernix/meteord:node-8.4.0-base',
    },

    // The maximum number of seconds it will wait
    // for your app to successfully start (optional, default is 60)
    deployCheckWaitTime: 300,

    // Show progress bar while uploading bundle to server
    // You might need to disable it on CI servers
    enableUploadProgressBar: true,
  },

  mongo: {
    version: '3.6.4',
    servers: {
      one: {},
    },
  },

  // (Optional)
  // Use the proxy to setup ssl or to route requests to the correct
  // app when there are several apps

  // proxy: {
  //   domains: 'mywebsite.com,www.mywebsite.com',

  //   ssl: {
  //     // Enable Let's Encrypt
  //     letsEncryptEmail: 'email@domain.com'
  //   }
  // }
}
