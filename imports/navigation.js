//------ Module -----
import AppHeader from '/imports/ui/HeaderMenu.vue'
import AppAside from '/imports/ui/AsideMenu.vue'
import BlogHeader from '/imports/modules/blog/ui/HeaderMenu.vue'
import BlogAside from '/imports/modules/blog/ui/AsideMenu.vue'
import RestHeader from '/imports/modules/rest/ui/HeaderMenu.vue'
import RestAside from '/imports/modules/rest/ui/AsideMenu.vue'

const navigation = {
  header: [RestHeader, AppHeader],
  aside: [RestAside, AppAside],
}

export default navigation
