import NotFound from './pages/NotFound.vue'
import Login from './pages/Login.vue'
import Home from './pages/Home.vue'
import Company from './pages/Company.vue'
import Branch from './pages/Branch.vue'
import BranchNew from './pages/BranchNew.vue'
import BranchEdit from './pages/BranchEdit.vue'
import User from './pages/User.vue'
import UserNew from './pages/UserNew.vue'
import UserEdit from './pages/UserEdit.vue'
import UserProfile from './pages/UserProfile.vue'
import Changelog from './pages/Changelog.vue'
import Setting from './pages/Setting.vue'
import Employee from './pages/Employee.vue'
import EmployeeNew from './pages/EmployeeNew.vue'
import EmployeeEdit from './pages/EmployeeEdit.vue'
import AccountType from './pages/AccountType.vue'
import ChartAccount from './pages/ChartAccount.vue'
import Classification from './pages/Classification.vue'
import Exchange from './pages/Exchange.vue'
import Journal from './pages/Journal.vue'
import JournalNew from './pages/JournalNew.vue'
import JournalEdit from './pages/JournalEdit.vue'

import IndexReport from './reports/Index.vue'
import AccountListReport from './reports/AccountList.vue'
import JournalReport from './reports/Journal.vue'
import GeneralLedgerReport from './reports/GeneralLedger.vue'
import TrialBalanceReport from './reports/TrialBalance.vue'
import ProfitAndLossReport from './reports/ProfitAndLoss.vue'
import BalanceSheetReport from './reports/BalanceSheet.vue'

import Demo from './pages/Demo.vue'
import DemoAgGrid from './pages/DemoAgGrid.vue'
import DemoJSReport from './pages/DemoJSReport.vue'

const routes = [
  // Not Found
  {
    path: '*',
    component: NotFound,
    meta: {
      pageTitle: 'Not Found',
      breadcrumb: {
        title: 'Not Found',
        ignore: true,
      },
    },
  },
  // Login
  {
    path: '/login',
    name: 'login',
    component: Login,
    meta: {
      layout: 'login',
      notRequiresAuth: true,
      pageTitle: 'Login',
    },
  },
  // Home
  {
    path: '',
    name: 'home',
    component: Home,
    meta: {
      pageTitle: 'Dashboard',
      breadcrumb: {
        title: 'Dashboard',
        icon: 'fas fa-tachometer-alt',
      },
    },
  },
  // App
  {
    path: '/app',
    component: {
      render(h) {
        return h('router-view')
      },
    },
    children: [
      // Demo
      {
        path: 'demo',
        name: 'app.demo',
        component: Demo,
        meta: {
          pageTitle: 'Demo',
          breadcrumb: {
            title: 'Demo',
            parent: 'home',
          },
        },
      },
      {
        path: 'ag-grid',
        name: 'app.agGrid',
        component: DemoAgGrid,
        meta: {
          pageTitle: 'AgGrid',
          breadcrumb: {
            title: 'AgGrid',
            parent: 'home',
          },
        },
      },
      {
        path: 'jsreport',
        name: 'app.jsreport',
        component: DemoJSReport,
        meta: {
          pageTitle: 'JSReport',
          breadcrumb: {
            title: 'JSReport',
            parent: 'home',
          },
        },
      },
      /**********************
       * Admin Setting
       *********************/
      // Company
      {
        path: 'company',
        name: 'app.company',
        component: Company,
        meta: {
          pageTitle: 'Company',
          breadcrumb: {
            title: 'Company',
            parent: 'home',
          },
        },
      },
      // Branch
      {
        path: 'branch',
        name: 'app.branch',
        component: Branch,
        meta: {
          pageTitle: 'Branch',
          breadcrumb: {
            title: 'Branch',
            parent: 'home',
          },
        },
      },
      {
        path: 'branch-new',
        name: 'app.branchNew',
        component: BranchNew,
        meta: {
          pageTitle: 'New Branch',
          linkActiveClass: 'app.branch',
          breadcrumb: {
            title: 'New',
            parent: 'app.branch',
          },
        },
      },
      {
        path: 'branch-edit/:_id',
        name: 'app.branchEdit',
        component: BranchEdit,
        meta: {
          pageTitle: 'Edit Branch',
          linkActiveClass: 'app.branch',
          breadcrumb: {
            title: 'Edit',
            parent: 'app.branch',
          },
        },
      },
      // User
      {
        path: 'user',
        name: 'app.user',
        component: User,
        meta: {
          pageTitle: 'User',
          breadcrumb: {
            title: 'User',
            parent: 'home',
          },
        },
      },
      {
        path: 'user-new',
        name: 'app.userNew',
        component: UserNew,
        meta: {
          pageTitle: 'New User',
          linkActiveClass: 'app.user',
          breadcrumb: {
            title: 'New',
            parent: 'app.user',
          },
        },
      },
      {
        path: 'user-edit/:_id',
        name: 'app.userEdit',
        component: UserEdit,
        meta: {
          pageTitle: 'Edit User',
          linkActiveClass: 'app.user',
          breadcrumb: {
            title: 'Edit',
            parent: 'app.user',
          },
        },
      },
      // User Profile
      {
        path: 'profile/:_id',
        name: 'app.profile',
        component: UserProfile,
        meta: {
          pageTitle: 'Profile',
          breadcrumb: {
            title: 'Profile',
            parent: 'home',
          },
        },
      },
      // Changelog
      {
        path: 'changelog',
        name: 'app.changelog',
        component: Changelog,
        meta: {
          pageTitle: 'Changelog',
          breadcrumb: {
            title: 'Changelog',
            parent: 'home',
          },
        },
      },
      /**********************
       * Accounting Setting
       *********************/
      {
        path: 'setting',
        name: 'app.setting',
        component: Setting,
        meta: {
          pageTitle: 'Setting',
          breadcrumb: {
            title: 'Setting',
            parent: 'home',
          },
        },
      },
      // Employee
      {
        path: 'employee',
        name: 'app.employee',
        component: Employee,
        meta: {
          pageTitle: 'Employee',
          linkActiveClass: 'app.setting',
          breadcrumb: {
            title: 'Employee',
            parent: 'rest.setting',
          },
        },
      },
      {
        path: 'employee-new',
        name: 'app.employeeNew',
        component: EmployeeNew,
        meta: {
          pageTitle: 'New Employee',
          linkActiveClass: 'rest.setting',
          breadcrumb: {
            title: 'New',
            parent: 'app.employee',
          },
        },
      },
      {
        path: 'employee-edit/:_id',
        name: 'app.employeeEdit',
        component: EmployeeEdit,
        meta: {
          pageTitle: 'Edit Employee',
          linkActiveClass: 'rest.setting',
          breadcrumb: {
            title: 'Edit',
            parent: 'app.employee',
          },
        },
      },
      // Account Type
      {
        path: 'accountType',
        name: 'app.accountType',
        component: AccountType,
        meta: {
          pageTitle: 'Account Type',
          linkActiveClass: 'app.setting',
          breadcrumb: {
            title: 'Account Type',
            parent: 'app.setting',
          },
        },
      },
      // Chart Account
      {
        path: 'chartAccount',
        name: 'app.chartAccount',
        component: ChartAccount,
        meta: {
          pageTitle: 'Chart of Account',
          linkActiveClass: 'app.setting',
          breadcrumb: {
            title: 'Chart of Account',
            parent: 'app.setting',
          },
        },
      },
      // Classification
      {
        path: 'classification',
        name: 'app.classification',
        component: Classification,
        meta: {
          pageTitle: 'Classification',
          linkActiveClass: 'app.setting',
          breadcrumb: {
            title: 'Classification',
            parent: 'app.setting',
          },
        },
      },
      /**********************
       * Data
       *********************/
      // Exchange
      {
        path: 'exchange',
        name: 'app.exchange',
        component: Exchange,
        meta: {
          pageTitle: 'Exchange',
          breadcrumb: {
            title: 'Exchange',
            parent: 'rest.setting',
          },
        },
      },
      // Journal
      {
        path: 'journal',
        name: 'app.journal',
        component: Journal,
        meta: {
          pageTitle: 'Journal Entry',
          breadcrumb: {
            title: 'Journal Entry',
            parent: 'home',
          },
        },
      },
      {
        path: 'journal-new',
        name: 'app.journalNew',
        component: JournalNew,
        meta: {
          pageTitle: 'New Journal Entry',
          linkActiveClass: 'app.journal',
          breadcrumb: {
            title: 'New',
            parent: 'app.journal',
          },
        },
      },
      {
        path: 'journal-edit/:_id',
        name: 'app.journalEdit',
        component: JournalEdit,
        meta: {
          pageTitle: 'Edit Journal Entry',
          linkActiveClass: 'app.journal',
          breadcrumb: {
            title: 'Edit',
            parent: 'app.journal',
          },
        },
      },
      /**********************
       * Report
       *********************/
      {
        path: 'report',
        name: 'app.report',
        component: IndexReport,
        meta: {
          pageTitle: 'Report',
          breadcrumb: {
            title: 'Report',
            parent: 'home',
          },
        },
      },
      // Account List
      {
        path: 'report-accountList',
        name: 'app.accountListReport',
        component: AccountListReport,
        meta: {
          pageTitle: 'Account List Report',
          linkActiveClass: 'app.report',
          breadcrumb: {
            title: 'Account List',
            parent: 'app.report',
          },
        },
      },
      // Journal
      {
        path: 'report-journal',
        name: 'app.journalReport',
        component: JournalReport,
        meta: {
          pageTitle: 'Journal Report',
          linkActiveClass: 'app.report',
          breadcrumb: {
            title: 'Journal',
            parent: 'app.report',
          },
        },
      },
      // General Ledger
      {
        path: 'report-generalLedger',
        name: 'app.generalLedgerReport',
        component: GeneralLedgerReport,
        meta: {
          pageTitle: 'General Ledger Report',
          linkActiveClass: 'app.report',
          breadcrumb: {
            title: 'General Ledger',
            parent: 'app.report',
          },
        },
      },
      // Trail Balance
      {
        path: 'report-trialBalance',
        name: 'app.trialBalanceReport',
        component: TrialBalanceReport,
        meta: {
          pageTitle: 'Trial Balance Report',
          linkActiveClass: 'app.report',
          breadcrumb: {
            title: 'Trial Balance',
            parent: 'app.report',
          },
        },
      },
      // Profit and Loss
      {
        path: 'report-profitAndLoss',
        name: 'app.profitLossReport',
        component: ProfitAndLossReport,
        meta: {
          pageTitle: 'Profit And Loss Report',
          linkActiveClass: 'app.report',
          breadcrumb: {
            title: 'Profit And Loss',
            parent: 'app.report',
          },
        },
      },
      // Balance Sheet
      {
        path: 'report-balanceSheet',
        name: 'app.balanceSheetReport',
        component: BalanceSheetReport,
        meta: {
          pageTitle: 'Balance Sheet Report',
          linkActiveClass: 'app.report',
          breadcrumb: {
            title: 'Balance Sheet',
            parent: 'app.report',
          },
        },
      },
    ],
  },
]

export default routes
