# Meteor - Vue (Element UI)

### Structure

```js
client/
imports/
    api/
    ui/
    modules/
        blog/                                   // Modules
            api/
                posts/
                    server/
                        hooks.js
                        publications.js
                    schema.js
                    collection.js
                    methods.js
                reports/
                startup-api.js
                startup-report.js
            ui/
                components/
                lib/                            // All libs run on client
                pages/
                    Setting.vue
                    Post.vue
                    PostNew.vue
                    PostEdit.vue
                reports/
                    Index.vue
                    Post.vue
                styles/
                    main.scss
                AsideMenu.vue
                HeaderMenu.vue
                routes.js
            startup/
                client/
                    index.js
                server/
                    fixtures.js
                    index.js                    // Startup: API, Units...
            utils/                              // All libs run on server
                lookup-methods.js               // Get lookup value from server
                validate-methods.js             // Custom validation from server
                other-lib.js
                startup-method.js
    startup/
    utils/                                      // global libraries
    app.js                                      // Create app context
    both.js                                     // Both startup
    client.js                                   // Client startup
    navigation.js                               // Config navigation menu
    plugins.js                                  // Vue plugins
    routes.js                                   // Config routes
    server.js                                   // Server startup
private/                                        // Place for server's assets
public/                                         // Place for client's assets
server/
```

### Module (Blog)

- Config `router` in `/imports/routes.js`
- Config `navigation menu` in `/imports/navigation.js`;
- Config `client, server and both` startup in `/imports/client.js, server.js and both.js`
- Assets `/private/modules` and `/public/modules`
- Config `user roles` in `/private/roles.json` and check it where that your want such as: `Header Menu, Aside Menu, Method...`

### Utils (`/imports/utils`)

- Auto Incrementing Sequence Field

```js
// Server
import getNextSeq from 'imports/utils/get-next-seq'

const seq = getNextSeq({
    // Mandatory
    _id: 'collectionName', //Eg. with branchId _id: `collectionName${branchId}`
    // Optional
    opts: {
        seq: 1,
        paddingType: 'start',
        paddingLength: 6,
        paddingChar: '0',
        prefix: ''
        seq: 1,
        paddingType: null, // null/empty, start, end
        paddingLength: 0, // Integer
        paddingChar: '0',
        prefix: '',
        toString: true, // default: true
    },
})
```

- Auto Incrementing Ref Number (RefNo) Field

```js
// Client
import getNextRef from '/imports/utils/get-next-ref'
...
getNextRef.callPromise({
    collectionName: 'journals',
    opts: {
        field: 'refNum',
        selector: { branchId: '001' },
        paddingType: 'start',
        paddingLength: 4,
        paddingChar: '0',
        prefix: '',
    },
}).then(result => {
    if (result) {
        this.form.refNum = result
    }
}).catch(error => {
    Notify.error({message: error})
})
```

```js
// Server
import { getNextRefServer } from '/imports/utils/get-next-ref'
...
let refNo = getNextRefServer({
    collectionName: 'journals',
    opts: {
        field: 'refNum',
        selector: { branchId: '001' },
        paddingType: 'start',
        paddingLength: 4,
        paddingChar: '0',
        prefix: '',
    },
})
```

- Rate Limit (Method Call Limit)

```js
// Methods
import rateLimit from '/imports/utils/rate-limit'
...
rateLimit({
  methods: [findOneCompany, insertCompany, updateCompany],
})
```

- Data Tables Query
- Security (Check user is in roles)

### Vue Plugin (`/imports/ui/plugins`)

- Lodash
- Moment
- Numeral

```js
<div>{{$_.upperCase('Rabbit Tech')}}</div>
<div>{{$moment().format('dd/mm/yyyy')}}</div>
<div>{{$numeral(123.456).format('$0,0.00')}}</div>
```

### Vue Mixin (`/imports/ui/mixins`)

- Data Tables Mixin

```js
const dataTablesMixin = {
  data() {
    return {
      tableProps: {
        border: false,
        stripe: false,
        defaultSort: {
          prop: 'code',
          order: 'ascending',
        },
      },
      tableInitialQuery: {
        type: 'init',
        page: 1,
        pageSize: 20,
        sortInfo: { prop: 'tranDate', order: 'descending' },
        filters: [],
      },
    }
  },
  methods: {},
}

export default dataTablesMixin
```

- Soft Remove Mixin

```js
softRemove(row) {
    this.$_softRemoveMixin({
    meteorMethod: softRemoveJournal,
    selector: { _id: row._id },
    successMethod: 'getData',
    successParams: this.tableInitialQuery,
    loading: 'loading',
    title: row.code,
    })
},
```

- Restore Mixin

```js
restore(row) {
    this.$_restoreMixin({
    meteorMethod: restoreJournals,
    selector: { _id: row._id },
    successMethod: 'getData',
    successParams: this.tableInitialQuery,
    loading: 'loading',
    title: row.code,
    })
},
```

- Remove Mixin

```js
remove(row) {
    this.$_removeMixin({
    meteorMethod: removeJournal,
    selector: { _id: row._id },
    successMethod: 'getData',
    successParams: this.tableInitialQuery,
    loading: 'loading',
    title: row.code,
    })
},
```

- CSV Export Mixin

```js
this.$_csvExportMixin({
  data: this.reportData,
  fields: fields,
  fileName: this.reportTitle,
})
```

### Client Lib (`/imports/ui/lib`)

- Msg
- Notify
- Wrap Current Time (Use when push date/time picker to server)

```js
let newDate = wrapCurrentTime(date) // Date, [Date]
```

- Date range picker opts

```js
<template>
  <el-date-picker
      v-model="datePicker"
      :picker-options="pickerOpts"
      type="daterange"
      format="dd/MM/yyyy"
      start-placeholder="Start Date"
      end-placeholder="End Date"
  >
  </el-date-picker>
</template>

<script>
import { dateRangePickerOpts } from '/imports/ui/lib/date-picker-opts'

data(){
    return {pickerOpts: dateRangePickerOpts}
}
</script>
```

### Client Component (`/imports/ui/components`)

- Table Toolbar

```js
<template>
    <!-- Table Toolbar -->
    <TableToolbar
      v-model="tableFilters"
      :col-span="{ action: 12, filter: 12 }"
      @new="addNew"
    >
      <!-- Custom Toolbar -->
      <template slot="action"></template>
      <template slot="filter"></template>
    </TableToolbar>
</template>
```

- Table Action

```js
<template>
  <!-- ... -->
  <el-table-column
      label="Actions"
      align="center"
      fixed="right"
      width="65"
  >
      <template slot-scope="scope">
          <table-action
          :actions="['edit', 'softRemove', 'restore', 'remove']"
          :row="scope.row"
          @action-click="tableActionClick"
          ></table-action>
      </template>
  </el-table-column>
</template>

<script>
import TableAction from '/imports/ui/components/TableAction.vue'

export default {
  ...
  components: { TableAction },
  methods:{
    tableActionClick(command) {
      this[command.action](command.row)
    },
    edit(row) {
      this.updateDoc = row
      this.currentModal = PostEdit
      this.$nextTick(() => {
        this.modalVisible = true
      })
    },
    softRemove(row) {
      this.$_softRemoveMixin({
        meteorMethod: softRemovePost,
        selector: { _id: row._id },
        successMethod: 'getData',
        loading: 'loading',
        title: row.title,
      })
    },
    restore(row) {
      this.$_restoreMixin({
        meteorMethod: restorePost,
        selector: { _id: row._id },
        successMethod: 'getData',
        loading: 'loading',
        title: row.title,
      })
    },
    remove(row) {
      this.$_removeMixin({
        meteorMethod: removePost,
        selector: { _id: row._id },
        successMethod: 'getData',
        loading: 'loading',
        title: row.title,
      })
    },
  }
}
</script>
```

### Vuex/Store (`/imports/ui/store`)

- Company Setting
- Current User
- Current User Full Name
- Current Branch
- Current Branch ID
- Branch Permissions
- Check User Is In Roles

```js
import {mapState} from 'vuex';
...
computed: {
    ...mapState({
        company(state) {
        return state.app.company
        },
        currentUser(state) {
        return state.app.currentUser
        },
        currentBranch(state) {
        return state.app.currentBranch
        },
        branchPermissions(state) {
        return state.app.branchPermissions
        },
    }),
    userFullName() {
        return this.$store.getters['app/userFullName']
    },
    currentBranchId() {
        return this.$store.getters['app/currentBranchId']
    },
    // Check use is in roles
    userIsInRole() {
        return this.$store.getters['app/userIsInRole'](['admin', 'accountant'])
    },
},
```

### Auto logout if no action (15m) or browse closed and refresh page

```js
/imports/startup/server/accounts.js
/imports/ui/layouts/AppLayout.vue
```

### Report Layout

- Template

```js
<template>
    <!--Form-->

    <!--REPORT Layout-->
    <report-layout
      :paper-size="paperSize"
      :export-csv="exportCSV"
      :css-text="cssText"
    >

      <!--REPORT HEADER-->
      <template slot="header">
        <div class="report-name">
          {{ reportTitle }}
        </div>
        <div class="report-period">
          {{ reportPeriod }}
        </div>
      </template>

      <!--REPORT FILTER-->
      <template slot="filter">
        <el-row :gutter="20">
          <el-col class="colspan-12">
            <span class="title">Filter1:</span> {{ filter1 }}
          </el-col>
          <el-col class="colspan-12">
            <span class="title">Filter2:</span> {{ filter2 }}
          </el-col>
        </el-row>
      </template>

      <!--REPORT CONTENT-->
      <div v-loading="loading">
        <table class="table-content">
          <thead>
            <tr>
              <th>#</th>
              <th>Field1</th>
              <th>Field2</th>
              <th>Field3</th>
              <th>Field4</th>
            </tr>
          </thead>
          <tbody>
            <tr
              v-for="(doc, index) in reportData"
              :key="index"
            >
              <td class="text-center">{{ index + 1 }}</td>
              <td>{{ doc.field1 }}</td>
              <td>{{ doc.field2 }}</td>
              <td>{{ doc.field3 }}</td>
              <td class="text-right">{{ doc.field4}}</td>
            </tr>
          </tbody>
        </table>
      </div>

      <!-- Footer -->
      <template slot="footer">
        <el-row :gutter="20">
          <el-col class="colspan-8">
            <span class="title css-custom">Subtotal:</span>
          </el-col>
          <el-col class="colspan-8">
            <span class="title css-custom">Discount:</span>
          </el-col>
          <el-col class="colspan-8">
            <span class="title css-custom">Total:</span>
          </el-col>
        </el-row>
      </template>
    </report-layout>
</template>

<script>
import _ from 'lodash'
import moment from 'moment'

import Notify from '/imports/ui/lib/notify'
import { dateRangePickerOpts } from '/imports/ui/lib/date-picker-opts'
import wrapCurrentTime from '/imports/ui/lib/wrap-current-time'

// Report Layout
import ReportLayout from '/imports/ui/layouts/ReportLayout.vue'

import { reportSample } from '../../api/reports/post'

export default {
  name: 'SampleReport',
  components: { ReportLayout },
  data() {
    return {
      // Form
      pickerOpts: dateRangePickerOpts,
      form: {
        input1: '',
        input2: '',
        reportPeriod: [moment().toDate(), moment().toDate()],
      },
      rules: {
        reportPeriod: [
          { required: true, message: 'Report period is required' },
        ],
      },

      // Report
      loading: false,
      paperSize: 'a4-p', // a4-p, a4-l, a5-p, a5-l, mini
      reportTitle: 'Sample Report',
      reportData: [],
      exportCSV: {
        data: this.reportData,
        fields: [
          { label: 'Field1', value: 'Field1' },
          { label: 'Field2', value: 'Field2' },
          { label: 'Field3', value: 'Field3' },
          { label: 'Field4', value: 'Field4' },
        ],
        fileName: 'Sample',
      },
      cssText: `.custom-css { color: red }`,
    }
  },
  computed: {
    filter1() {
      return ...
    },
    filter2() {
      return ...
    },
    reportPeriod() {
      const period = `${moment(this.form.reportPeriod[0]).format('DD/MM/YYYY')}
                  To ${moment(this.form.reportPeriod[1]).format('DD/MM/YYYY')}
                `
      return period
    },
  },
  methods: {
    submitForm() {
      this.$refs['form'].validate(valid => {
        if (valid) {
          this.loading = true

          // Pick doc
          this.form.reportPeriod = wrapCurrentTime(this.form.reportPeriod)

          reportSample
            .callPromise(this.form)
            .then(result => {
              this.reportData = result
              this.loading = false
            })
            .catch(error => {
              this.loading = false
              Notify.error({ message: error })
            })
        } else {
          return false
        }
      })
    },
    resetForm() {
      this.$refs['form'].resetFields()
    },
  },
}
</script>

<style lang="scss" scoped>
@import '~imports/ui/styles/report.scss';

.custom-css { color: red }
</style>
```

- Note:

```js
// Report layout props
paper-size: String // a4-p, a4-l, a5-p, a5-l, mini
export-csv: Object // {data, fields, fileName}
css-text: String
trigger: Boolean // false
show: Object // {header: true, filter: true, content:true, footer: true, signature: false, timestamp: true}
---
// Kh font
kh-moul
kh-battambang
// Grid layout
colspan: 6, 8, 12
// Text align
text-align: text-left, text-right, text-center
```

### Changelog

Please check `changelog` in `/private/CHANGELOG.md`
