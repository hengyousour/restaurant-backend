import { Meteor } from 'meteor/meteor'
import { Session } from 'meteor/session'
import _ from 'lodash'

import Msg from '../lib/message'

const app = {
  namespaced: true,
  state: {
    company: null,
    branchPermissions: [],
    // Use session for refresh page
    currentBranch: Session.get('currentBranch'),
    currentUser: null,
  },
  getters: {
    langUI(state) {
      return state.lang == 'en' ? 'KH' : 'EN'
    },
    userFullName(state) {
      return state.currentUser ? state.currentUser.profile.fullName : 'Unknown'
    },
    userProfilePic(state) {
      return state.currentUser ? state.currentUser.profile.profilePic : ''
    },
    currentBranchId(state) {
      return state.currentBranch ? state.currentBranch._id : null
    },
    branchPermissionOpts(state) {
      let opts = []
      state.branchPermissions.forEach(val => {
        opts.push({ label: `${val._id} : ${val.name}`, value: val._id })
      })

      return opts
    },
    userIsInRole: state => roles => {
      roles = _.isString(roles) ? [roles] : roles
      return _.some(roles, val => {
        return _.includes(state.currentUser && state.currentUser.roles, val)
      })
    },
  },
  mutations: {
    updateCompany(state, company) {
      state.company = company
    },
    updateBranchPermissions(state, branches) {
      state.branchPermissions = branches

      // Set current branch
      // if (branches.length > 0 && !Session.get('currentBranch')) {
      if (branches && branches.length > 0) {
        let currentBranch = branches[0]
        Session.setAuth('currentBranch', currentBranch)
        state.currentBranch = currentBranch
      }
    },
    updateCurrentBranch(state, branch) {
      // const branch = _.find(state.branchPermissions, {_id: branchId});
      Session.setAuth('currentBranch', branch)
      state.currentBranch = branch
    },
    updateCurrentUser(state, user) {
      state.currentUser = user
    },
    logout(state, self) {
      Meteor.logout(() => {
        Session.clear()
        self.$nextTick(() => {
          Msg.success('You are logout!')
          self.$router.push({ name: 'login' })
        })
      })
    },
  },
}

export default app
