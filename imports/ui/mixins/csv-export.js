import json2csv from 'json2csv'
import moment from 'moment'

import Notify from '/imports/ui/lib/notify'

const csvExportMixin = {
    methods: {
        $_csvExportMixin({ data, fields, fileName }) {
            const currentDate = moment().format('YYYY-MM-DD')
            try {
                let result = json2csv({
                    data: data,
                    fields: fields,
                })

                let csvContent = 'data:text/csvcharset=GBK,\uFEFF' + result
                let encodedUri = encodeURI(csvContent)
                let link = document.createElement('a')

                link.setAttribute('href', encodedUri)
                link.setAttribute(
                    'download',
                    `${fileName || 'CSVExport'}_${currentDate}.csv`
                )
                document.body.appendChild(link)
                link.click()
                document.body.removeChild(link)
            } catch (error) {
                Notify.error({ message: error })
            }
        },
    },
}

export default csvExportMixin
