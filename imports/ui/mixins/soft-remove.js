import Msg from '/imports/ui/lib/message'
import Notify from '/imports/ui/lib/notify'

const softRemoveMixin = {
    methods: {
        $_softRemoveMixin({
            meteorMethod,
            selector,
            successMethod,
            successParams,
            loading,
            title,
        }) {
            selector = selector || {}
            successParams = successParams || {}
            loading = loading ? this[loading] : ''
            title = title ? ` [${title}]` : ''

            this.$confirm(
                `This will softly remove ${title}. Continue?`,
                'Warning',
                { type: 'warning' }
            )
                .then(() => {
                    loading = true

                    meteorMethod
                        .callPromise(selector)
                        .then(result => {
                            loading = false
                            successMethod
                                ? this[successMethod](successParams)
                                : ''
                            Msg.success()
                        })
                        .catch(error => {
                            loading = false
                            Notify.error({ message: error })
                        })
                })
                .catch(() => {
                    Msg.warning('Your transaction is canceled')
                })
        },
    },
}

export default softRemoveMixin
