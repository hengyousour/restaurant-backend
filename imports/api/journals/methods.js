import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'
import moment from 'moment'

import { throwError } from '../../utils/security'
import rateLimit from '/imports/utils/rate-limit'
import getNextSeq from '../../utils/get-next-seq'
import markDataTablesQuery from '/imports/utils/data-tables-query'
import calAmountOfJournalDetail from '../../utils/cal-amount-journal-detail'

import Journals from './journals'
import JournalDetails from './journal-details'
import JournalsView from '../views/Journals'

// Data table
export const getJournalsDataTable = new ValidatedMethod({
  name: 'app.getJournalsDataTable',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
    options: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector, options }) {
    if (Meteor.isServer) {
      selector = selector || {}
      options = options || {}

      return JournalsView.find(selector, options).fetch()
    }
  },
})

export const dataTableJournals2 = new ValidatedMethod({
  name: 'app.dataTableJournals2',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    tableQuery: {
      type: Object,
      blackbox: true,
    },
    params: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ tableQuery, params }) {
    if (Meteor.isServer) {
      tableQuery = tableQuery || {}
      params = params || {}

      // // Mark Query
      // const fieldType = {
      //   refNum: String,
      //   tranDate: Date,
      //   journalType: String,
      //   currency: String,
      // }

      // const getQuery = markDataTablesQuery(query, fieldType)
      // // console.log('get query', getQuery);

      // Selector
      const selector = {}
      if (params.date) {
        selector.tranDate = {
          $gte: moment(params.date[0])
            .startOf('day')
            .toDate(),
          $lte: moment(params.date[1])
            .endOf('day')
            .toDate(),
        }
      }

      // Options
      const skip = (tableQuery.page - 1) * tableQuery.pageSize
      const limit = tableQuery.pageSize
      const sort = {
        [tableQuery.sortInfo.prop]:
          tableQuery.sortInfo.order === 'ascending' ? 1 : -1,
      }
      const options = { sort, skip, limit }

      let data = Journals.find(selector, options).fetch()
      let total = Journals.find(selector).count()

      return { data, total }
    }
  },
})

// Find
export const findJournals = new ValidatedMethod({
  name: 'app.findJournals',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
    options: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector, options }) {
    if (Meteor.isServer) {
      selector = selector || {}
      options = options || {}

      return Journals.find(selector, options).fetch()
    }
  },
})

// Find One
export const findOneJournal = new ValidatedMethod({
  name: 'app.findOneJournal',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector }) {
    if (Meteor.isServer) {
      selector = selector || {}

      let data = Journals.aggregate([
        {
          $match: selector,
        },
        {
          $lookup: {
            from: 'app_journalDetails',
            localField: '_id',
            foreignField: 'journalId',
            as: 'detailDoc',
          },
        },
      ])

      return data[0]
    }
  },
})

// Insert
export const insertJournal = new ValidatedMethod({
  name: 'app.insertJournal',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    doc: Journals.schema,
    details: {
      type: Array,
    },
    'details.$': _.clone(JournalDetails.schema).omit('journalId'),
  }).validator(),
  run({ doc, details }) {
    if (Meteor.isServer) {
      const journalId = getNextSeq({
        _id: `journal${doc.branchId}`,
        opts: {
          prefix: doc.branchId,
        },
      })

      try {
        doc._id = journalId
        Journals.insert(doc, (error, result) => {
          if (!error) {
            details.forEach((item, index) => {
              // Check account type
              let { dr, cr, amount } = calAmountOfJournalDetail({
                chartAccountId: item.chartAccountId,
                dr: item.dr,
                cr: item.cr,
              })

              item._id = `${journalId}-${index}`
              item.dr = dr
              item.cr = cr
              item.amount = amount
              item.journalId = journalId

              JournalDetails.insert(item)
            })
          }
        })

        return journalId
      } catch (error) {
        getNextSeq({
          _id: `journal${doc.branchId}`,
          opts: { seq: -1 },
        })
        JournalDetails.remove({ journalId: journalId })
        Journals.remove({ _id: journalId })

        throwError(error)
      }
    }
  },
})

// Update
export const updateJournal = new ValidatedMethod({
  name: 'app.updateJournal',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    doc: _.clone(Journals.schema).extend({
      _id: String,
    }),
    details: {
      type: Array,
    },
    'details.$': _.clone(JournalDetails.schema).omit('journalId'),
  }).validator(),
  run({ doc, details }) {
    if (Meteor.isServer) {
      const journalId = doc._id

      try {
        Journals.update({ _id: journalId }, { $set: doc }, (error, result) => {
          if (!error) {
            // Remove all before insert
            JournalDetails.remove({ journalId: journalId }, error => {
              if (!error) {
                details.forEach((item, index) => {
                  // Check account type
                  let { dr, cr, amount } = calAmountOfJournalDetail({
                    chartAccountId: item.chartAccountId,
                    dr: item.dr,
                    cr: item.cr,
                  })

                  item._id = `${journalId}-${index}`
                  item.dr = dr
                  item.cr = cr
                  item.amount = amount
                  item.journalId = journalId

                  JournalDetails.insert(item)
                })
              }
            })
          }
        })

        return journalId
      } catch (error) {
        throwError(error)
      }
    }
  },
})

// Soft remove
export const softRemoveJournal = new ValidatedMethod({
  name: 'app.softRemoveJournal',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return Journals.softRemove({ _id })
    }
  },
})

// Restore
export const restoreJournals = new ValidatedMethod({
  name: 'app.restoreJournals',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return Journals.restore({ _id })
    }
  },
})

// Remove
export const removeJournal = new ValidatedMethod({
  name: 'app.removeJournal',
  mixins: [CallPromiseMixin],
  // validate: null,
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return Journals.remove({ _id }, error => {
        if (!error) {
          JournalDetails.remove({ journalId: _id })
        }
      })
    }
  },
})

rateLimit({
  methods: [
    findJournals,
    findOneJournal,
    insertJournal,
    updateJournal,
    softRemoveJournal,
    restoreJournals,
    removeJournal,
  ],
})
