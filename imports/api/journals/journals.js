import { Mongo } from 'meteor/mongo'
import SimpleSchema from 'simpl-schema'

const Journals = new Mongo.Collection('app_journals')

Journals.schema = new SimpleSchema({
  refNum: {
    type: String,
  },
  tranDate: {
    type: Date,
  },
  journalType: {
    type: String,
  },
  currency: {
    type: String,
  },
  amount: {
    type: Number,
  },
  memo: {
    type: String,
    optional: true,
  },
  branchId: {
    type: String,
  },
})

Journals.attachSchema(Journals.schema)
Journals.timestamp()

export default Journals
