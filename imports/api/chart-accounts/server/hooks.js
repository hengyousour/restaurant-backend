// Collection
import ChartAccount from '../chart-accounts'

ChartAccount.before.insert((userId, doc) => {
  // Check sub account
  if (doc.parent) {
    let parentAccount = ChartAccount.findOne({ _id: doc.parent })
    let ancestors = parentAccount.ancestors || []
    ancestors.push(doc.parent)

    doc.ancestors = ancestors
    doc.order = `${parentAccount.order}${doc.code}`
  } else {
    doc.order = doc.code
  }
})

ChartAccount.before.update((userId, doc, fieldNames, modifier, options) => {
  modifier.$set = modifier.$set || {}

  // Check sub account
  if (modifier.$set.parent) {
    let parentAccount = ChartAccount.findOne({ _id: modifier.$set.parent })
    let ancestors = parentAccount.ancestors || []
    ancestors.push(modifier.$set.parent)

    modifier.$set.ancestors = ancestors
    modifier.$set.order = `${parentAccount.order}${modifier.$set.code}`
  } else {
    modifier.$set.order = modifier.$set.code
  }
})
