import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'

import rateLimit from '/imports/utils/rate-limit'
import markDataTablesQuery from '/imports/utils/data-tables-query'

import ChartAccounts from './chart-accounts'

// Data table
export const dataTableChartAccounts = new ValidatedMethod({
  name: 'app.dataTableChartAccounts',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    query: {
      type: Object,
      blackbox: true,
      optional: true,
    },
    options: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ query, options }) {
    if (Meteor.isServer) {
      query = query || {}
      options = options || {}

      // Mark Query
      const fieldType = {
        code: String,
        name: String,
        status: String,
      }

      const dataTableQuery = markDataTablesQuery(query, fieldType)
      // console.log('data table query', dataTableQuery);

      // let data = ChartAccounts.find(dataTableQuery.selector, dataTableQuery.options).fetch();
      let doc = ChartAccounts.aggregate([
        {
          $match: dataTableQuery.selector,
        },
        {
          $lookup: {
            from: 'chartAccounts',
            localField: 'parent',
            foreignField: '_id',
            as: 'parentDoc',
          },
        },
        {
          $unwind: {
            path: '$parentDoc',
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $lookup: {
            from: 'accountTypes',
            localField: 'accountTypeId',
            foreignField: '_id',
            as: 'accountTypeDoc',
          },
        },
        {
          $unwind: '$accountTypeDoc',
        },
        {
          $sort: dataTableQuery.options.sort,
        },
        {
          $skip: dataTableQuery.options.skip,
        },
        {
          $limit: dataTableQuery.options.limit,
        },
      ])

      return { data: doc, total: doc.length }
    }
  },
})

// Find
export const findChartAccounts = new ValidatedMethod({
  name: 'app.findChartAccounts',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector }) {
    if (Meteor.isServer) {
      selector = selector || {}
      let sort = { order: 1 }

      let data = ChartAccounts.aggregate([
        {
          $match: selector,
        },
        {
          $lookup: {
            from: 'chartAccounts',
            localField: 'parent',
            foreignField: '_id',
            as: 'parentDoc',
          },
        },
        {
          $unwind: {
            path: '$parentDoc',
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $lookup: {
            from: 'accountTypes',
            localField: 'accountTypeId',
            foreignField: '_id',
            as: 'accountTypeDoc',
          },
        },
        {
          $unwind: '$accountTypeDoc',
        },
        {
          $sort: sort,
        },
      ])

      return data
    }
  },
})

// Find One
export const findOneChartAccount = new ValidatedMethod({
  name: 'app.findOneChartAccount',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
    },
    options: {
      type: Object,
      blackbox: true,
    },
  }).validator(),
  run({ selector, options }) {
    if (Meteor.isServer) {
      selector = selector || {}
      options = options || {}

      return ChartAccounts.findOne(selector, options)
    }
  },
})

// Insert
export const insertChartAccount = new ValidatedMethod({
  name: 'app.insertChartAccount',
  mixins: [CallPromiseMixin],
  validate: ChartAccounts.schema.validator(),
  run(doc) {
    if (Meteor.isServer) {
      return ChartAccounts.insert(doc)
    }
  },
})

// Update
export const updateChartAccount = new ValidatedMethod({
  name: 'app.updateChartAccount',
  mixins: [CallPromiseMixin],
  validate: _
    .clone(ChartAccounts.schema)
    .extend({
      _id: String,
    })
    .validator(),
  run(doc) {
    if (Meteor.isServer) {
      return ChartAccounts.update({ _id: doc._id }, { $set: doc })
    }
  },
})

// Soft remove
export const softRemoveChartAccount = new ValidatedMethod({
  name: 'app.softRemoveChartAccount',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: { type: String },
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return ChartAccounts.softRemove({ _id })
    }
  },
})

// Restore
export const restoreChartAccount = new ValidatedMethod({
  name: 'app.restoreChartAccount',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: { type: String },
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return ChartAccounts.restore({ _id })
    }
  },
})

// Remove
export const removeChartAccount = new ValidatedMethod({
  name: 'app.removeChartAccount',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: { type: String },
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return ChartAccounts.remove({ _id })
    }
  },
})

rateLimit({
  methods: [
    dataTableChartAccounts,
    findChartAccounts,
    findOneChartAccount,
    insertChartAccount,
    updateChartAccount,
    softRemoveChartAccount,
    restoreChartAccount,
    removeChartAccount,
  ],
})
