import { Mongo } from 'meteor/mongo'
import SimpleSchema from 'simpl-schema'

const ChartAccounts = new Mongo.Collection('chartAccounts')

ChartAccounts.schema = new SimpleSchema({
  accountTypeId: {
    type: String,
    index: 1,
  },
  parent: {
    type: String,
    optional: true,
  },
  code: {
    type: String,
    unique: true,
    min: 4,
    max: 10,
  },
  name: {
    type: String,
    unique: true,
    max: 200,
  },
  status: {
    type: String,
  },
  memo: {
    type: String,
    optional: true,
  },
  // Use in collection hook
  ancestors: {
    type: Array,
    optional: true,
  },
  'ancestors.$': {
    type: String,
  },
  order: {
    type: String,
    optional: true,
  },
})

ChartAccounts.attachSchema(ChartAccounts.schema)
ChartAccounts.timestamp()

export default ChartAccounts
