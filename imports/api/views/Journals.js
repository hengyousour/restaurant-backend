import { Meteor } from 'meteor/meteor'
import { Mongo } from 'meteor/mongo'

const JournalsView = new Mongo.Collection('app_journalsView')
export default JournalsView

if (Meteor.isServer) {
  const db = JournalsView.rawDatabase()

  // Drop before create
  db.dropCollection('app_journalsView', (err, res) => {
    db.createCollection('app_journalsView', {
      viewOn: 'app_journals',
      pipeline: [
        {
          $lookup: {
            from: 'app_journalDetails',
            localField: '_id',
            foreignField: 'journalId',
            as: 'details',
          },
        },
        { $unwind: { path: '$details', preserveNullAndEmptyArrays: true } },
        {
          $lookup: {
            from: 'chartAccounts',
            localField: 'details.chartAccountId',
            foreignField: '_id',
            as: 'chartAccountDoc',
          },
        },
        {
          $unwind: {
            path: '$chartAccountDoc',
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $lookup: {
            from: 'accountTypes',
            localField: 'chartAccountDoc.accountTypeId',
            foreignField: '_id',
            as: 'accountTypeDoc',
          },
        },
        {
          $unwind: {
            path: '$accountTypeDoc',
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $group: {
            _id: '$_id',
            refNum: { $last: '$refNum' },
            tranDate: { $last: '$tranDate' },
            journalType: { $last: '$journalType' },
            currency: { $last: '$currency' },
            total: {
              $sum: '$details.amount',
            },
            memo: { $last: '$memo' },
            branchId: { $last: '$branchId' },
            details: {
              $push: {
                _id: '$details._id',
                chartAccountId: '$details.chartAccountId',
                dr: '$details.dr',
                cr: '$details.cr',
                amount: '$details.amount',
                journalId: '$details.journalId',
                chartAccountDoc: '$chartAccountDoc',
                accountTypeDoc: '$accountTypeDoc',
              },
            },
          },
        },
      ],
    })
  })
}
