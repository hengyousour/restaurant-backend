import { Meteor } from 'meteor/meteor'
import { Mongo } from 'meteor/mongo'

const ChartAccountView = new Mongo.Collection('chartAccountsView')
export default ChartAccountView

if (Meteor.isServer) {
  const db = ChartAccountView.rawDatabase()

  // Drop before create
  db.dropCollection('chartAccountsView', (err, res) => {
    db.createCollection('chartAccountsView', {
      viewOn: 'chartAccounts',
      pipeline: [
        {
          $lookup: {
            from: 'accountTypes',
            localField: 'accountTypeId',
            foreignField: '_id',
            as: 'accountTypeDoc',
          },
        },
        { $unwind: '$accountTypeDoc' },
      ],
    })
  })
}
