import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import _ from 'lodash'

import rateLimit from '/imports/utils/rate-limit'

import Files from './files'

export const findImageData = new ValidatedMethod({
  name: 'app.findImageData',
  mixins: [CallPromiseMixin],
  validate: null,
  // new SimpleSchema({
  // selector: {
  //     type: Object,
  //     blackbox: true,
  //     optional: true,
  // },
  // options: {
  //     type: Object,
  //     blackbox: true,
  //     optional: true,
  // },
  // }
  // ).validator(),
  run() {
    if (Meteor.isServer) {
      let images = Files.find(
        {},
        {
          sort: { date: -1 },
        }
      ).fetch()

      _.forEach(images, o => {
        o.url = Files.findOne({ _id: o._id }).url()
      })

      return images
    }
  },
})

rateLimit({
  methods: [findImageData],
})
