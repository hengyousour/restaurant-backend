const Files = new FS.Collection('files', {
  stores: [
    new FS.Store.FileSystem('filesStore', { path: '/data/file_uploads' }),
  ],
})

Files.allow({
  insert: function() {
    return true
  },
  update: function() {
    return true
  },
  remove: function() {
    return true
  },
  download: function() {
    return true
  },
})

export default Files
