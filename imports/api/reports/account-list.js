import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'

import rateLimit from '/imports/utils/rate-limit'

import ChartAccounts from '../chart-accounts/chart-accounts'

export const reportAccountList = new ValidatedMethod({
  name: 'app.reportAccountList',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    accountType: {
      type: Array,
    },
    'accountType.$': {
      type: String,
    },
  }).validator(),
  run({ accountType }) {
    if (Meteor.isServer) {
      Meteor._sleepForMs(500)

      let selector = {}
      let sort = { order: 1 }

      // Check filter
      if (accountType.length) {
        selector.accountTypeId = { $in: accountType }
      }

      let getChartAccounts = ChartAccounts.aggregate([
        {
          $match: selector,
        },
        {
          $lookup: {
            from: 'chartAccounts',
            localField: 'parent',
            foreignField: '_id',
            as: 'parentDoc',
          },
        },
        {
          $unwind: {
            path: '$parentDoc',
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $lookup: {
            from: 'accountTypes',
            localField: 'accountTypeId',
            foreignField: '_id',
            as: 'accountTypeDoc',
          },
        },
        {
          $unwind: '$accountTypeDoc',
        },
        {
          $sort: sort,
        },
      ])

      // Set indent
      let data = []
      getChartAccounts.forEach(doc => {
        const count = (doc.ancestors && doc.ancestors.length) || 0
        doc.indent = _.repeat('&nbsp;', count * 5)
        data.push(doc)
      })

      return data
    }
  },
})

rateLimit({
  methods: [reportAccountList],
})
