import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'
import moment from 'moment'

import rateLimit from '/imports/utils/rate-limit'

import JournalsView from '../views/Journals'

const reportGeneralLedger = new ValidatedMethod({
  name: 'app.reportGeneralLedger',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    branchId: {
      type: String,
      optional: true,
    },
    currency: Array,
    'currency.$': String,
    accountType: Array,
    'accountType.$': String,
    reportDate: Array,
    'reportDate.$': Date,
  }).validator(),
  async run(params) {
    if (Meteor.isServer) {
      Meteor._sleepForMs(300)

      const dateF = moment(params.reportDate[0])
        .startOf('day')
        .toDate()
      const dateT = moment(params.reportDate[1])
        .endOf('day')
        .toDate()

      // Pick selector
      let selector = {
        tranDate: {
          $gte: dateF,
          $lte: dateT,
        },
      }
      if (params.branchId) {
        selector.branchId = params.branchId
      }
      if (params.currency.length) {
        selector.currency = { $in: params.currency }
      }
      if (params.accountType.length) {
        selector.accountTypeId = { $in: params.accountType }
      }

      // Aggregate
      const data = await JournalsView.rawCollection()
        .aggregate([
          {
            $match: selector,
          },
          {
            $sort: { tranDate: 1, refNum: 1 },
          },
        ])
        .toArray()

      return data
    }
  },
})

rateLimit({
  methods: [reportGeneralLedger],
})

export default reportGeneralLedger
