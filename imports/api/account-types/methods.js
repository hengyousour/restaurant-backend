import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'

import rateLimit from '/imports/utils/rate-limit'

import AccountTypes from './account-types'

// Find
export const findAccountTypes = new ValidatedMethod({
  name: 'app.findAccountTypes',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
    options: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector, options }) {
    if (Meteor.isServer) {
      selector = selector || {}
      options = options || {}

      return AccountTypes.find(selector, options).fetch()
    }
  },
})

// Find One
export const findOneAccountType = new ValidatedMethod({
  name: 'app.findOneAccountType',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
    options: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector, options }) {
    if (Meteor.isServer) {
      selector = selector || {}
      options = options || {}

      return AccountTypes.findOne(selector, options)
    }
  },
})

// Insert
export const insertAccountType = new ValidatedMethod({
  name: 'app.insertAccountType',
  mixins: [CallPromiseMixin],
  validate: AccountTypes.schema.validator(),
  run(doc) {
    if (Meteor.isServer) {
      return AccountTypes.insert(doc)
    }
  },
})

// Update
export const updateAccountType = new ValidatedMethod({
  name: 'app.updateAccountType',
  mixins: [CallPromiseMixin],
  validate: _
    .clone(AccountTypes.schema)
    .extend({
      _id: String,
    })
    .validator(),
  run(doc) {
    if (Meteor.isServer) {
      return AccountTypes.update({ _id: doc._id }, { $set: doc })
    }
  },
})

export const softRemoveAccountType = new ValidatedMethod({
  name: 'app.softRemoveAccountType',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return AccountTypes.softRemove({ _id })
    }
  },
})

export const removeAccountType = new ValidatedMethod({
  name: 'app.removeAccountType',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return AccountTypes.remove({ _id })
    }
  },
})

export const restoreAccountType = new ValidatedMethod({
  name: 'app.restoreAccountType',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return AccountTypes.restore({ _id })
    }
  },
})

rateLimit({
  methods: [
    findAccountTypes,
    findOneAccountType,
    insertAccountType,
    updateAccountType,
    removeAccountType,
  ],
})
