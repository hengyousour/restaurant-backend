import { Mongo } from 'meteor/mongo'
import SimpleSchema from 'simpl-schema'

const AccountTypes = new Mongo.Collection('accountTypes')

AccountTypes.schema = new SimpleSchema({
  code: {
    type: String,
    unique: true,
  },
  name: {
    type: String,
    unique: true,
  },
  nature: {
    type: String,
    allowedValues: ['Asset', 'Liability', 'Equity', 'Revenue', 'Expense'],
  },
  status: {
    type: String,
  },
  memo: {
    type: String,
    optional: true,
  },
})

AccountTypes.attachSchema(AccountTypes.schema)
AccountTypes.timestamp()

export default AccountTypes
