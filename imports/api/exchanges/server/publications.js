import { Meteor } from 'meteor/meteor'

import Exchanges from '../exchanges'

Meteor.publish('app.exchanges', function(selector = {}, options = {}) {
  Meteor._sleepForMs(100)

  return Exchanges.find(selector, options)
})
