import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'
import moment from 'moment'

import rateLimit from '/imports/utils/rate-limit'

import AppLogs from './app-logs'

// Insert
export const appLog = new ValidatedMethod({
  name: 'app.appLog',
  mixins: [CallPromiseMixin],
  validate: null,
  run(doc) {
    if (Meteor.isServer) {
      _.defaults(doc, {
        userId: Meteor.userId(),
        date: moment().toDate(),
      })

      return AppLogs.insert(doc)
    }
  },
})

rateLimit({
  methods: [appLog],
})
