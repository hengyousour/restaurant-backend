import { Mongo } from 'meteor/mongo'
import SimpleSchema from 'simpl-schema'

const Classifications = new Mongo.Collection('classifications')

Classifications.schema = new SimpleSchema({
  name: {
    type: String,
    unique: true,
  },
  status: {
    type: String,
  },
  memo: {
    type: String,
    optional: true,
  },
})

Classifications.attachSchema(Classifications.schema)
Classifications.timestamp()

export default Classifications
