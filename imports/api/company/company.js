import { Mongo } from 'meteor/mongo'
import SimpleSchema from 'simpl-schema'

const Company = new Mongo.Collection('company')

Company.general = new SimpleSchema({
  name: {
    type: String,
    max: 500,
  },
  address: {
    type: String,
  },
  telephone: {
    type: String,
    max: 100,
  },
  email: {
    type: String,
    regEx: SimpleSchema.RegEx.Email,
    optional: true,
  },
  website: {
    type: String,
    regEx: SimpleSchema.RegEx.Url,
    optional: true,
  },
  // Type of company
  industry: {
    type: String,
  },
})

Company.accounting = new SimpleSchema({
  baseCurrency: {
    type: String,
  },
  decimalNumber: {
    type: Number,
  },
  accountingIntegration: {
    type: Boolean,
  },
})

Company.other = new SimpleSchema({
  dateFormat: {
    type: String,
  },
  lang: {
    type: String,
  },
  footerAddress: {
    type: String,
    optional: true,
  },
  wifiPass: {
    type: String,
    optional: true,
  },
  facebookPage: {
    type: String,
    optional: true,
  },
  thankText: {
    type: String,
    optional: true,
  },
})

Company.schema = new SimpleSchema({
  general: Company.general,
  accounting: Company.accounting,
  other: Company.other,
})

Company.attachSchema(Company.schema)

export default Company
