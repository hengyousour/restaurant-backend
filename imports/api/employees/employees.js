import { Mongo } from 'meteor/mongo'
import SimpleSchema from 'simpl-schema'

const Employees = new Mongo.Collection('employees')

// Address
const EmployeeAddress = new SimpleSchema({
  phone: {
    type: String,
    optional: true,
  },
  email: {
    type: String,
    optional: true,
  },
  address: {
    type: String,
    optional: true,
  },
})

Employees.schema = new SimpleSchema({
  _id: {
    type: String,
    optional: true,
  },
  code: {
    type: String,
  },
  name: {
    type: String,
    max: 200,
  },
  gender: {
    type: String,
  },
  role: {
    type: String,
  },
  linkUser: {
    type: String,
    optional: true,
  },
  status: {
    type: String,
  },
  branchId: {
    type: String,
  },
  employeeAddress: {
    type: EmployeeAddress,
    optional: true,
  },
})

Employees.attachSchema(Employees.schema)

export default Employees
