import { Meteor } from 'meteor/meteor'

import Employee from '../employees'

Meteor.publish('app.employee', function(selector = {}, options = {}) {
  Meteor._sleepForMs(100)

  return Employee.find(selector, options)
})
