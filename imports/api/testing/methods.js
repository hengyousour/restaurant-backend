import { Meteor } from 'meteor/meteor'
import { MongoInternals } from 'meteor/mongo'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import { SimpleSchema } from 'simpl-schema'
import moment from 'moment'
import _ from 'lodash'
import { Promise } from 'meteor/promise'

// Collections
import Branches from '../branches/branches'

// WrapAsync
export const wrapAsyncTest = new ValidatedMethod({
  name: 'app.wrapAsyncTest',
  mixins: [CallPromiseMixin],
  validate: null,
  run() {
    if (Meteor.isServer) {
      console.log('method called')
      let getSync = sync('first', 'second')
      console.log('after async called')

      return getSync
    }
  },
})

const async = function(first, second, callback) {
  console.log('async all, please waiting 2000ms')
  const err = null
  setTimeout(function() {
    callback(err, { first, second })
  }, 2000)
}

const sync = Meteor.wrapAsync(async)

// Dynamic Collection
export const dynamicCollectionTest = new ValidatedMethod({
  name: 'app.dynamicCollectionTest',
  mixins: [CallPromiseMixin],
  validate: null,
  run(collectionName) {
    if (Meteor.isServer) {
      Meteor._sleepForMs(500)
      const dyCollection = MongoInternals.defaultRemoteCollectionDriver().open(
        collectionName
      )

      return dyCollection.findOne()
    }
  },
})

// Raw Collection
export const rawCollectionTest = new ValidatedMethod({
  name: 'app.rawCollectionTest',
  mixins: [CallPromiseMixin],
  validate: null,
  async run() {
    if (Meteor.isServer) {
      Meteor._sleepForMs(500)
      const selector = {}

      const data = await Branches.rawCollection()
        .aggregate([
          {
            $match: selector,
          },
          {
            $project: {
              _id: 1,
              name: 1,
              address: 1,
            },
          },
        ])
        .toArray()

      return data
    }
  },
})

// Timezone Test
export const timezoneTest = new ValidatedMethod({
  name: 'app.timezoneTest',
  mixins: [CallPromiseMixin],
  validate: null,
  run() {
    if (Meteor.isServer) {
      Meteor._sleepForMs(100)
      const selector = {}

      const data = Promise.await(
        Branches.rawCollection()
          .aggregate([
            {
              $match: selector,
            },
            {
              $project: {
                _id: 1,
                name: 1,
                createdAt: 1,
                createdAtStr: {
                  $dateToString: { format: '%Y-%m-%d', date: '$createdAt' },
                },
              },
            },
          ])
          .toArray()
      )

      let tmpData = []
      _.forEach(data, val => {
        val.nextDate = moment(val.createdAt)
          .add(1, 'day')
          .toDate()
        tmpData.push(val)
      })

      return tmpData
    }
  },
})

// Get Current Timezone
export const getCurrentTimezone = new ValidatedMethod({
  name: 'app.getCurrentTimezone',
  mixins: [CallPromiseMixin],
  validate: null,
  run() {
    if (Meteor.isServer) {
      return moment.tz.guess()
    }
  },
})
