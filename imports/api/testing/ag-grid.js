import { Meteor } from 'meteor/meteor'
import { Random } from 'meteor/random'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import { SimpleSchema } from 'simpl-schema'
import moment from 'moment'
import _ from 'lodash'
import faker from 'faker'

// WrapAsync
export const agGridData = new ValidatedMethod({
  name: 'app.agGridData',
  mixins: [CallPromiseMixin],
  validate: null,
  run(num) {
    if (Meteor.isServer) {
      Meteor._sleepForMs(500)
      num = num ? num : 20
      console.log(num)

      let data = []
      _.times(num, val => {
        data.push({
          name: faker.name.findName(),
          gender: faker.random.arrayElement(['M', 'F']),
          dob: faker.date.past(),
          address: faker.address.country(),
          telephone: faker.phone.phoneNumber(),
          salary: faker.finance.amount(),
          id: Random.id(),
        })
      })

      return data
    }
  },
})
