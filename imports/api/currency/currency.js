import { Mongo } from 'meteor/mongo'
import SimpleSchema from 'simpl-schema'

const Currency = new Mongo.Collection('currency')

Currency.schema = new SimpleSchema({
  name: {
    type: String,
    unique: true,
  },
  symbol: {
    type: String,
  },
})

Currency.attachSchema(Currency.schema)

export default Currency
