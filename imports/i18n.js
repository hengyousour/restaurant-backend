import Vue from 'vue'
import VueI18n from 'vue-i18n'
Vue.use(VueI18n)

// Element UI
import enLocale from 'element-ui/lib/locale/lang/en'
import kmLocale from 'element-ui/lib/locale/lang/km'

// Lang
import enRestaurant from '/imports/modules/rest/lang/en'
import kmRestaurant from '/imports/modules/rest/lang/km'

const messages = {
  en: {
    message: 'hello',
    ...enLocale,
    rest: enRestaurant,
  },
  km: {
    message: 'សួស្តី',
    ...kmLocale,
    rest: kmRestaurant,
  },
}

// Create VueI18n instance with options
const i18n = new VueI18n({
  locale: 'en', // set locale
  messages, // set locale messages
})

export default i18n
