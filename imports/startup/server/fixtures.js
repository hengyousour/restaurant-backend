import { Meteor } from 'meteor/meteor'
import { EJSON } from 'meteor/ejson'
import { Roles } from 'meteor/alanning:roles'
import { Accounts } from 'meteor/accounts-base'

import Company from '../../api/company/company'
import Branches from '../../api/branches/branches'
import Currency from '../../api/currency/currency'
import AccountTypes from '../../api/account-types/account-types'
import ChartAccounts from '../../api/chart-accounts/chart-accounts'

Meteor.startup(function() {
  /**
   * Development
   */
  // if (Meteor.isDevelopment) {
  // }

  /**
   * Production
   */
  // Roles
  if (Roles.getAllRoles().count() === 0) {
    const data = EJSON.parse(Assets.getText('roles.json'))
    data.forEach(role => {
      Roles.createRole(role)
    })
  }

  // User
  if (!Meteor.users.findOne({ name: 'super' })) {
    const data = EJSON.parse(Assets.getText('user-account.json'))
    data.forEach(({ username, email, password, profile, roles }) => {
      const userExists = Accounts.findUserByUsername(username)

      if (!userExists) {
        const userId = Accounts.createUser({
          username,
          email,
          password,
          profile,
        })
        Roles.addUsersToRoles(userId, roles)
      }
    })
  }

  // Company
  if (Company.find().count() === 0) {
    const data = EJSON.parse(Assets.getText('company.json'))
    Company.insert(data)
  }

  // Branch
  if (Branches.find().count() === 0) {
    const data = EJSON.parse(Assets.getText('branch.json'))
    data.forEach(doc => {
      Branches.insert(doc)
    })
  }

  // Currency
  if (Currency.find().count() === 0) {
    const data = EJSON.parse(Assets.getText('currency.json'))
    data.forEach(doc => {
      Currency.insert(doc)
    })
  }

  // Account Type
  if (AccountTypes.find().count() === 0) {
    const data = EJSON.parse(Assets.getText('account-type.json'))
    data.forEach(doc => {
      AccountTypes.insert(doc)
    })
  }

  // Chart of Account
  if (ChartAccounts.find().count() === 0) {
    const data = EJSON.parse(Assets.getText('chart-account.json'))
    data.forEach(doc => {
      ChartAccounts.insert(doc)
    })
  }
})
