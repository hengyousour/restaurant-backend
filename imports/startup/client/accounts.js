import { Accounts } from 'meteor/accounts-base'

Accounts.onLogout(() => {
  console.log('Auto logout if no activity (15m)')
  window.location.pathname = '/'
})
