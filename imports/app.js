import './plugins'

import { Meteor } from 'meteor/meteor'
import Vue from 'vue'
import VueRouter from 'vue-router'

// Router Sync
import { sync } from 'vuex-router-sync'

// NProgress
import NProgress from 'nprogress'

//-------------------------------
// App layout
import AppLayout from '/imports/ui/layouts/AppLayout.vue'
// Store
import store from '/imports/ui/store/index'

// Create router
import routes from './routes'
const router = new VueRouter({
  mode: 'history',
  routes: routes,
})

import i18n from './i18n'
// Sync the router with the vuex store
sync(store, router)

/******************
 * Meteor Startup
 *****************/
Meteor.startup(() => {
  // Before each
  router.beforeEach((to, from, next) => {
    NProgress.start()

    if (to.meta.notRequiresAuth) {
      next()
    } else {
      // Check user logged in
      // if (Meteor.loggingIn() || Meteor.userId()) {
      if (Meteor.userId()) {
        next()
      } else {
        next({ path: '/login' })
      }
    }
  })

  // // After each
  router.afterEach((to, from) => {
    NProgress.done()
  })

  // Start the vue app
  new Vue({
    router,
    store,
    i18n,
    ...AppLayout,
  }).$mount('app')
})
