import '/imports/startup/server'
import '/imports/modules/blog/startup/server'
import '/imports/modules/rest/startup/server'
import '/imports/modules/restaurant-api/startup/server'