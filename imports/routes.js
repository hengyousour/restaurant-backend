import _ from 'lodash'

//------ Module -----
import app from './ui/routes'
import blog from '/imports/modules/blog/ui/routes'
import rest from '/imports/modules/rest/ui/routes'

const routes = _.concat(app, blog, rest)
export default routes
