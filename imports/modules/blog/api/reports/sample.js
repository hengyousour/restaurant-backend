import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'
import faker from 'faker'

import rateLimit from '/imports/utils/rate-limit'

export const reportSample = new ValidatedMethod({
  name: 'blog.reportSample',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Array,
    },
    'selector.$': {
      type: String,
    },
  }).validator(),
  run({ selector }) {
    if (Meteor.isServer) {
      Meteor._sleepForMs(500)

      let data = []
      _.times(100, t => {
        t += 1
        data.push({
          _id: t,
          code: t,
          name: faker.name.findName(),
          gender: faker.random.arrayElement(['Male', 'Female']),
          department: faker.random.arrayElement(['Admin', 'Finance', 'IT']),
          telephone: faker.phone.phoneNumberFormat('000-000-000'),
          salary: faker.finance.amount(),
        })
      })

      let tmpData = _.filter(data, o => {
        return selector.length > 0 ? _.includes(selector, o.department) : true
      })

      return tmpData
    }
  },
})

rateLimit({
  methods: [reportSample],
})
