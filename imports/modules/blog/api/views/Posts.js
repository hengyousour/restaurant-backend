import { Meteor } from 'meteor/meteor'
import { Mongo } from 'meteor/mongo'

const PostsView = new Mongo.Collection('bg_postsView')
export default PostsView

if (Meteor.isServer) {
  const db = PostsView.rawDatabase()

  // Always drop existing before create new
  db.dropCollection('bg_postsView', (err, res) => {
    // Don't check err, it will auto update
    db.createCollection('bg_postsView', {
      viewOn: 'bg_posts',
      pipeline: [
        {
          $lookup: {
            from: 'bg_categories',
            localField: 'categoryId',
            foreignField: '_id',
            as: 'categoryDoc',
          },
        },
        { $unwind: '$categoryDoc' },
        // {
        //   $project: {
        //     posDate: 1,
        //     title: 1,
        //     body: 1,
        //   },
        // },
      ],
    })
  })
}
