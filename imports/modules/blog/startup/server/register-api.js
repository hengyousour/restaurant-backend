/**
 * View & Report
 */
import '../../api/views'
import '../../api/reports'

/**
 * API
 */
// Category
import '../../api/categories/methods'
import '../../api/categories/server/hooks'
import '../../api/categories/server/publications'

// Post
import '../../api/posts/methods'
import '../../api/posts/server/hooks'
import '../../api/posts/server/publications'
