import Setting from './pages/Setting.vue'
import Category from './pages/Category.vue'
import Post from './pages/Post.vue'
import Map from './pages/Map.vue'

import IndexReport from './reports/Index.vue'
import PostReport from './reports/Post.vue'
import SampleReport from './reports/Sample.vue'

const routes = [
  {
    path: '/blog',
    component: {
      render(h) {
        return h('router-view')
      },
    },
    children: [
      /**************
       * Setting
       *************/
      {
        path: 'setting',
        name: 'blog.setting',
        component: Setting,
        meta: {
          pageTitle: 'Setting',
          breadcrumb: {
            title: 'Setting',
            parent: 'home',
          },
        },
      },
      // Category
      {
        path: 'category',
        name: 'blog.category',
        component: Category,
        meta: {
          pageTitle: 'Category',
          linkActiveClass: 'blog.setting',
          breadcrumb: {
            title: 'Category',
            parent: 'blog.setting',
          },
        },
      },
      /****************
       * Data
       ***************/
      // Post
      {
        path: 'post',
        name: 'blog.post',
        component: Post,
        meta: {
          pageTitle: 'Post',
          breadcrumb: {
            title: 'Post',
            parent: 'home',
          },
        },
      },
      // Map
      {
        path: 'map',
        name: 'blog.map',
        component: Map,
        meta: {
          pageTitle: 'Map',
          breadcrumb: {
            title: 'Map',
            parent: 'home',
          },
        },
      },
      /*****************
       * Report
       ****************/
      {
        path: 'report',
        name: 'blog.report',
        component: IndexReport,
        meta: {
          pageTitle: 'Report',
          breadcrumb: {
            title: 'Report',
            parent: 'home',
          },
        },
      },
      // Sample
      {
        path: 'report-sample',
        name: 'blog.sampleReport',
        component: SampleReport,
        meta: {
          pageTitle: 'Sample Report',
          linkActiveClass: 'blog.report',
          breadcrumb: {
            title: 'Sample Report',
            parent: 'blog.report',
          },
        },
      },
      // Post
      {
        path: 'report-post',
        name: 'blog.postReport',
        component: PostReport,
        meta: {
          pageTitle: 'Post Report',
          linkActiveClass: 'blog.report',
          breadcrumb: {
            title: 'Post Report',
            parent: 'blog.report',
          },
        },
      },
    ],
  },
]

export default routes
