const LookupValue = {
  status: [
    { label: 'Public', value: 'Public' },
    { label: 'Private', value: 'Private' },
  ],
}

export default LookupValue
