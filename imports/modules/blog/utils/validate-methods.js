import { Meteor } from 'meteor/meteor'
import { Accounts } from 'meteor/accounts-base'
import { check } from 'meteor/check'
import { Roles } from 'meteor/alanning:roles'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'

import rateLimit from '/imports/utils/rate-limit'
import { userIsInRole, throwError } from '/imports/utils/security'

// Collection
import Categories from '../api/categories/Categories'

// Category exist
export const validateCategoryExist = new ValidatedMethod({
  name: 'blog.validateCategoryExist',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector }) {
    if (Meteor.isServer) {
      return Categories.findOne(selector)
    }
  },
})
