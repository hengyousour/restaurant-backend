import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'

// Collection
import Groups from '../api/groups/collection'
import Categories from '../api/categories/collection'
import Stations from '../api/stations/collection'
import Units from '../api/units/collection'
import Products from '../api/products/collection'
import Suppliers from '../api/suppliers/suppliers'
import Guests from '../api/guests/guests'
import Floors from '../api/floors/floors'
import Tables from '../api/tables/tables'

// Table exist
export const validateTableExist = new ValidatedMethod({
  name: 'rest.validateTableExist',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector }) {
    if (Meteor.isServer) {
      return Tables.findOne(selector)
    }
  },
})
// Floor exist
export const validateFloorExist = new ValidatedMethod({
  name: 'rest.validateFloorExist',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector }) {
    if (Meteor.isServer) {
      return Floors.findOne(selector)
    }
  },
})
// Supplier exist
export const validateSupplierExist = new ValidatedMethod({
  name: 'rest.validateSupplierExist',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector }) {
    if (Meteor.isServer) {
      return Suppliers.findOne(selector)
    }
  },
})
// Guest exist
export const validateGuestExist = new ValidatedMethod({
  name: 'rest.validateGuestExist',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector }) {
    if (Meteor.isServer) {
      return Guests.findOne(selector)
    }
  },
})
// Group exist
export const validateGroupExist = new ValidatedMethod({
  name: 'rest.validateGroupExist',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector }) {
    if (Meteor.isServer) {
      return Groups.findOne(selector)
    }
  },
})

// Category exist
export const validateCategoryExist = new ValidatedMethod({
  name: 'rest.validateCategoryExist',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector }) {
    if (Meteor.isServer) {
      return Categories.findOne(selector)
    }
  },
})

// Station exist
export const validateStationExist = new ValidatedMethod({
  name: 'rest.validateStationExist',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector }) {
    if (Meteor.isServer) {
      return Stations.findOne(selector)
    }
  },
})

// Unit exist
export const validateUnitExist = new ValidatedMethod({
  name: 'rest.validateStationsExist',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector }) {
    if (Meteor.isServer) {
      return Units.findOne(selector)
    }
  },
})

// Product exist
export const validateProductExist = new ValidatedMethod({
  name: 'rest.validateProductExist',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector }) {
    if (Meteor.isServer) {
      return Products.findOne(selector)
    }
  },
})
