import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
// Collections
import Products from '../api/products/collection'
import Exchange from '/imports/api/exchanges/exchanges'

// Find One
export const saleRequire = new ValidatedMethod({
  name: 'rest.saleRequire',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
    options: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector, options }) {
    if (Meteor.isServer) {
      selector = selector || {}
      options = options || {}

      if (Meteor.isServer) {
        let exchange = Exchange.find().count()
        let product = Products.find(selector, options).count()
        return { exchange, product }
      }
    }
  },
})
