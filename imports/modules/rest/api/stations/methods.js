import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'

import { throwError } from '../../../../utils/security'
import rateLimit from '/imports/utils/rate-limit'
import getNextSeq from '/imports/utils/get-next-seq'

import StationSchema from './schema'
import Stations from './collection'
import Products from '../products/collection'
// Find
export const findStations = new ValidatedMethod({
  name: 'rest.findStations',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
    options: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector, options }) {
    if (Meteor.isServer) {
      selector = selector || {}
      options = options || {}

      return Stations.find(selector, options).fetch()
    }
  },
})

// Find One
export const findOneStation = new ValidatedMethod({
  name: 'rest.findOneStation',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return Stations.findOne({ _id })
    }
  },
})

// Upsert Station
export const upsertStation = new ValidatedMethod({
  name: 'rest.upsertStation',
  mixins: [CallPromiseMixin],
  validate: _.clone(StationSchema)
    .extend({
      _id: {
        type: String,
        optional: true,
      },
    })
    .validator(),
  run(doc) {
    if (Meteor.isServer) {
      try {
        return Stations.upsert({ _id: doc._id }, { $set: doc })
      } catch (error) {
        throwError(error)
      }
    }
  },
})

// Soft remove
export const softRemoveStation = new ValidatedMethod({
  name: 'rest.softRemoveStation',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return Stations.softRemove(_id)
    }
  },
})

// Restore
export const restoreStation = new ValidatedMethod({
  name: 'rest.restoreStation',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return Stations.restore(_id)
    }
  },
})

// Remove
export const removeStation = new ValidatedMethod({
  name: 'rest.removeStation',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return Stations.remove(_id)
    }
  },
})

// usedStation
export const usedStation = new ValidatedMethod({
  name: 'rest.usedStation',
  mixins: [CallPromiseMixin],
  validate: null,
  run(_id) {
    if (Meteor.isServer) {
      let used = false
      let productDoc = Products.findOne({ stationId: _id })
      if (productDoc) {
        used = true
      } else {
        used = false
      }

      return { used }
    }
  },
})
