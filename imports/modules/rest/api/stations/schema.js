import SimpleSchema from 'simpl-schema'

export default new SimpleSchema({
  name: {
    type: String,
  },
  printer: {
    type: String,
    optional: true,
  },
})
