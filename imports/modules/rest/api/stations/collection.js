import { Mongo } from 'meteor/mongo'
import Schema from './schema'

const Stations = new Mongo.Collection('rest_stations')
Stations.attachSchema(Schema)
Stations.timestamp()

export default Stations
