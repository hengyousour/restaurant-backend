import { Mongo } from 'meteor/mongo'
import SimpleSchema from 'simpl-schema'

const Sales = new Mongo.Collection('rest_sales')

Sales.schema = new SimpleSchema({
  _id: {
    type: String,
  },
  date: {
    type: Date,
  },
  outDate: {
    type: Date,
    optional: true,
  },
  type: {
    type: String,
  },
  status: {
    type: String,
  },
  statusDate: {
    type: Object,
  },
  'statusDate.open': {
    type: Date,
    optional: true,
  },
  'statusDate.partial': {
    type: Date,
    optional: true,
  },
  'statusDate.close': {
    type: Date,
    optional: true,
  },
  'statusDate.cancel': {
    type: Date,
    optional: true,
  },
  discountRate: {
    type: Number,
  },
  discountValue: {
    type: Number,
  },
  total: {
    type: Number,
  },
  totalReceived: {
    type: Number,
  },
  discountDetail: {
    type: Object,
    optional: true,
  },
  'discountDetail.type': {
    type: String,
  },
  'discountDetail.discountType': {
    type: String,
  },
  'discountDetail.discountValue': {
    type: Number,
  },
  guestId: {
    type: String,
  },
  numOfGuest: {
    type: Number,
  },
  employeeId: {
    type: String,
  },
  billed: {
    type: Number,
  },
  tableId: {
    type: String,
  },
  refId: {
    type: String,
    optional: true,
  },
})

Sales.attachSchema(Sales.schema)

export default Sales
