import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'
import numeral from 'numeral'
import moment from 'moment'
import math from 'mathjs'
import BigNumber from 'bignumber.js'

import { throwError } from '../../../../utils/security'
import getNextSeq from '/imports/utils/get-next-seq'

import Sales from './sales'
import SaleDetails from './saleDetails'
// Table
import Tables from '../tables/tables'
// Floor
import Floors from '../floors/floors'
// discount
import Discounts from '../discounts/collection'

/*===========
  ----findSales-----
=============*/
export const findSales = new ValidatedMethod({
  name: 'rest.findSales',
  mixins: [CallPromiseMixin],
  validate: null,
  run({ selector, options }) {
    if (Meteor.isServer) {
      selector = selector || {}
      options = options || {}

      let orders = Sales.find(selector, options).fetch()
      _.forEach(orders, o => {
        o.details = SaleDetails.aggregate([
          { $match: { invoiceId: o._id } },
          {
            $lookup: {
              from: 'rest_products',
              localField: 'itemId',
              foreignField: '_id',
              as: 'productDoc',
            },
          },
          {
            $unwind: {
              path: '$productDoc',
              preserveNullAndEmptyArrays: true,
            },
          },
          {
            $addFields: {
              itemName: '$productDoc.name',
              itemType: '$productDoc.type',
            },
          },
          { $project: { productDoc: 0 } },
        ])
        o.details = _.reverse(o.details)
      })
      orders.sort()
      return orders
    }
  },
})

/*===========
  ----findCurrentTable&Floor-----
=============*/
export const findCurrentTableAndFloor = new ValidatedMethod({
  name: 'rest.findCurrentTableAndFloor',
  mixins: [CallPromiseMixin],
  validate: null,
  run(tableId) {
    if (Meteor.isServer) {
      let doc = Tables.aggregate([
        { $match: { _id: tableId } },
        {
          $lookup: {
            from: 'rest_floors',
            localField: 'floorId',
            foreignField: '_id',
            as: 'floorDoc',
          },
        },
        {
          $unwind: {
            path: '$floorDoc',
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $addFields: {
            floorName: '$floorDoc.name',
            tableName: '$name',
          },
        },
        { $project: { floorName: 1, tableName: 1 } },
      ])

      return doc[0]
    }
  },
})

/*===========
  ----findTableSaleGroup-----
=============*/
export const findTableSaleGroup = new ValidatedMethod({
  name: 'rest.findTableSaleGroup',
  mixins: [CallPromiseMixin],
  validate: null,
  run({ selector, options }) {
    if (Meteor.isServer) {
      selector = selector || {}
      options = options || {}
      let tables = Tables.find().fetch()
      _.forEach(tables, o => {
        let floor = Floors.findOne({ _id: o.floorId })
        o.name = `${floor.name} - ${o.name}`
        selector.tableId = o._id
        o.invoiceDoc = Sales.find(selector, options).fetch()
      })

      return tables
    }
  },
})

/*===========
  ----findSaleAggregate-----
=============*/
export const findSaleAggregate = new ValidatedMethod({
  name: 'rest.findSaleAggregate',
  mixins: [CallPromiseMixin],
  validate: null,
  run({ tableQuery, params }) {
    if (Meteor.isServer) {
      if (params !== undefined && tableQuery !== undefined) {
        tableQuery = tableQuery || {}
        params = params || {}

        const selector = {}
        if (!_.isEmpty(params.status)) {
          selector.status = params.status
        }
        if (params.billed != undefined) {
          selector.billed = {
            $gte: params.billed,
          }
          selector.status = 'Open'
        }

        let filterSelector = {}
        if (!_.isEmpty(params.filters)) {
          filterSelector = {
            $or: [
              {
                _id: new RegExp(params.filters.value, 'i'), //Can I put a limit here?
              },
              {
                tableName: new RegExp(params.filters.value, 'i'), //Can I put a limit here?
              },
              {
                guestName: new RegExp(params.filters.value, 'i'), //Can I put a limit here?
              },
            ],
          }
        }
        // Options
        const sort = {
          [tableQuery.sort.prop]:
            tableQuery.sort.order === 'ascending' ? 1 : -1,
        }
        const skip = (tableQuery.page - 1) * tableQuery.pageSize
        const limit = tableQuery.pageSize
        const options = { sort, skip, limit }

        const data = Sales.aggregate([
          {
            $match: selector,
          },
          {
            $lookup: {
              from: 'rest_guests',
              localField: 'guestId',
              foreignField: '_id',
              as: 'guestDocs',
            },
          },
          {
            $unwind: {
              path: '$guestDocs',
              preserveNullAndEmptyArrays: true,
            },
          },
          {
            $lookup: {
              from: 'rest_tables',
              localField: 'tableId',
              foreignField: '_id',
              as: 'tableDoc',
            },
          },
          {
            $unwind: {
              path: '$tableDoc',
              preserveNullAndEmptyArrays: true,
            },
          },
          {
            $addFields: {
              tableName: '$tableDoc.name',
              guestName: '$guestDoc.name',
            },
          },
          {
            $match: filterSelector,
          },
          {
            $sort: options.sort,
          },
          {
            $skip: options.skip,
          },
          {
            $limit: options.limit,
          },
          { $project: { tableDoc: 0, guestDoc: 0 } },
        ])

        _.forEach(data, o => {
          o.details = SaleDetails.aggregate([
            { $match: { invoiceId: o._id } },
            {
              $lookup: {
                from: 'rest_products',
                localField: 'itemId',
                foreignField: '_id',
                as: 'productDoc',
              },
            },
            {
              $unwind: {
                path: '$productDoc',
                preserveNullAndEmptyArrays: true,
              },
            },
            {
              $addFields: {
                itemName: '$productDoc.name',
              },
            },
            { $project: { productDoc: 0 } },
          ])
        })

        let total = Sales.find(selector).count()

        return { data: data, total: total }
      }
    }
  },
})

/*===========
  ----findSaleDetailAggregate-----
=============*/
export const findSaleDetailAggregate = new ValidatedMethod({
  name: 'rest.findSaleDetailAggregate',
  mixins: [CallPromiseMixin],
  validate: null,
  run({ params }) {
    if (Meteor.isServer) {
      let doc = SaleDetails.aggregate([
        { $match: { invoiceId: o._id } },
        {
          $lookup: {
            from: 'rest_products',
            localField: 'itemId',
            foreignField: '_id',
            as: 'productDoc',
          },
        },
        {
          $unwind: {
            path: '$productDoc',
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $addFields: {
            itemName: '$productDoc.name',
          },
        },
        { $project: { productDoc: 0 } },
      ])

      return doc
    }
  },
})

/*===========
  ----findFilterSale-----
=============*/
export const findFilterSale = new ValidatedMethod({
  name: 'rest.findFilterSale',
  mixins: [CallPromiseMixin],
  validate: null,
  run({ params }) {
    if (Meteor.isServer) {
      let selector = {}
      if (!_.isEmpty(params.status)) {
        selector.status = params.status
      }
      if (!_.isEmpty(params.billed)) {
        selector.billed = params.billed
      }

      let orders = Sales.aggregate([
        {
          $match: selector,
        },
        {
          $lookup: {
            from: 'rest_tables',
            localField: 'tableId',
            foreignField: '_id',
            as: 'tableDoc',
          },
        },
        {
          $unwind: {
            path: '$tableDoc',
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $lookup: {
            from: 'rest_floors',
            localField: 'tableDoc.floorId',
            foreignField: '_id',
            as: 'floorDoc',
          },
        },
        {
          $unwind: {
            path: '$floorDoc',
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $lookup: {
            from: 'rest_guests',
            localField: 'guestId',
            foreignField: '_id',
            as: 'guestDoc',
          },
        },
        {
          $unwind: {
            path: '$guestDoc',
            preserveNullAndEmptyArrays: true,
          },
        },
      ])

      _.forEach(orders, o => {
        o.details = SaleDetails.aggregate([
          { $match: { invoiceId: o._id } },
          {
            $lookup: {
              from: 'rest_products',
              localField: 'itemId',
              foreignField: '_id',
              as: 'productDoc',
            },
          },
          {
            $unwind: {
              path: '$productDoc',
              preserveNullAndEmptyArrays: true,
            },
          },
          {
            $addFields: {
              itemName: '$productDoc.name',
              itemType: '$productDoc.type',
            },
          },
          { $project: { productDoc: 0 } },
        ])
      })

      return orders
    }
  },
})

/*===========
  ----findOneSale-----
=============*/
export const findOneSale = new ValidatedMethod({
  name: 'rest.findOneSale',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      let data = Sales.findOne({ _id: _id })
      let details = SaleDetails.find({ invoiceId: _id }).fetch()
      return { data, details }
    }
  },
})

/*===========
  ----insertSale-----
=============*/
export const insertSale = new ValidatedMethod({
  name: 'rest.insertSale',
  mixins: [CallPromiseMixin],
  validate: null,
  run({ doc, details }) {
    if (Meteor.isServer) {
      const invoiceId = getNextSeq({
        // Mandatory
        filter: {
          _id: 'rest_sales',
        },
        // Optional
        opts: {
          seq: 1,
          paddingType: 'start',
          paddingLength: 4,
          paddingChar: '0',
          prefix: moment(doc.date).format('YYMMDD'),
        },
      })
      try {
        doc._id = invoiceId.toString()
        const newDate = new Date()
        const checkDate = moment(newDate).toDate()
        let selector = {
          startDate: { $lte: checkDate },
          endDate: { $gte: checkDate },
          type: 'Invoice',
        }

        let discount = Discounts.aggregate([
          {
            $match: selector,
          },
          {
            $group: {
              _id: '$_id',
              eventName: { $last: '$eventName' },
              startDate: { $last: '$startDate' },
              endDate: { $last: '$endDate' },
              discountValue: { $last: '$discountValue' },
              discountType: { $last: '$discountType' },
              type: { $last: '$type' },
            },
          },
        ])

        let discountDoc = null
        let checkTime = moment(newDate).format('HH:mm:ss')

        _.forEach(discount, o => {
          let startDate = moment(o.startDate).format('HH:mm:ss')
          let endDate = moment(o.endDate).format('HH:mm:ss')

          if (
            Date.parse('01 Jan 2019 ' + checkTime) >=
              Date.parse('01 Jan 2019 ' + startDate) &&
            Date.parse('01 Jan 2019 ' + checkTime) <=
              Date.parse('01 Jan 2019 ' + endDate)
          ) {
            discountDoc = o
          }
        })

        // Add discount Doc
        if (!_.isEmpty(discountDoc)) {
          if (discountDoc.discountType == 'Amount') {
            doc.discountValue = discountDoc.discountValue
            doc.total = -discountDoc.discountValue
          } else {
            doc.discountRate = discountDoc.discountValue
          }
          doc.discountDetail = {
            _id: discountDoc._id,
            discountValue: discountDoc.discountValue,
            discountType: discountDoc.discountType,
            type: discountDoc.type,
          }
        }

        Sales.insert(doc, error => {
          if (!error) {
            if (details && details.length) {
              _.forEach(details, o => {
                o.invoiceId = doc._id
                SaleDetails.insert(o)
              })
            }
          }
        })

        return doc._id
      } catch (error) {
        // Decrement seq
        getNextSeq({
          filter: { _id: 'rest_sales' },
          opts: { seq: -1 },
        })
        SaleDetails.remove({ invoiceId: invoiceId })
        Sales.remove({ _id: invoiceId })
        throwError(error)
      }
    }
  },
})

/*===========
  ----insertFastSale-----
=============*/
export const insertFastSale = new ValidatedMethod({
  name: 'rest.insertFastSale',
  mixins: [CallPromiseMixin],
  validate: null,
  run({ doc }) {
    if (Meteor.isServer) {
      const invoiceId = getNextSeq({
        // Mandatory
        filter: {
          _id: 'rest_sales',
          // type: '001' // BranchId
        },
        // Optional
        opts: {
          seq: 1,
          paddingType: 'start',
          paddingLength: 4,
          paddingChar: '0',
          prefix: moment(doc.date).format('YYMMDD'),
        },
      })
      try {
        doc._id = invoiceId.toString()
        doc.tableId = Tables.findOne()._id

        const newDate = new Date()
        const checkDate = moment(newDate).toDate()
        let selector = {
          startDate: { $lte: checkDate },
          endDate: { $gte: checkDate },
          type: 'Invoice',
        }

        let discount = Discounts.aggregate([
          {
            $match: selector,
          },
          {
            $group: {
              _id: '$_id',
              eventName: { $last: '$eventName' },
              startDate: { $last: '$startDate' },
              endDate: { $last: '$endDate' },
              discountValue: { $last: '$discountValue' },
              discountType: { $last: '$discountType' },
              type: { $last: '$type' },
            },
          },
        ])

        let discountDoc = null
        let checkTime = moment(newDate).format('HH:mm:ss')

        _.forEach(discount, o => {
          let startDate = moment(o.startDate).format('HH:mm:ss')
          let endDate = moment(o.endDate).format('HH:mm:ss')

          if (
            Date.parse('01 Jan 2019 ' + checkTime) >=
              Date.parse('01 Jan 2019 ' + startDate) &&
            Date.parse('01 Jan 2019 ' + checkTime) <=
              Date.parse('01 Jan 2019 ' + endDate)
          ) {
            discountDoc = o
          }
        })

        // Add discount Doc
        if (!_.isEmpty(discountDoc)) {
          if (discountDoc.discountType == 'Amount') {
            doc.discountValue = discountDoc.discountValue
          } else {
            doc.discountRate = discountDoc.discountValue
          }
          doc.discountDetail = {
            _id: discountDoc._id,
            discountValue: discountDoc.discountValue,
            discountType: discountDoc.discountType,
            type: discountDoc.type,
          }
        }

        // Insert sale
        Sales.insert(doc)
        return doc._id
      } catch (error) {
        // Decrement seq
        getNextSeq({
          filter: { _id: 'rest_sales' },
          opts: { seq: -1 },
        })
        Sales.remove({ _id: invoiceId })
        throwError(error)
      }
    }
  },
})

/*===========
  ----insertSaleDetail-----
=============*/
export const insertSaleDetail = new ValidatedMethod({
  name: 'rest.insertSaleDetail',
  mixins: [CallPromiseMixin],
  validate: null,
  run({ doc }) {
    if (Meteor.isServer) {
      const invoiceId = doc.invoiceId
      try {
        const newDate = new Date()
        const checkDate = moment(newDate).toDate()
        let matchDate = {
          startDate: { $lte: checkDate },
          endDate: { $gte: checkDate },
          type: 'Item',
        }
        let matchItem = {
          'products.productId': doc.itemId,
        }

        let discount = Discounts.aggregate([
          {
            $match: matchDate,
          },
          {
            $unwind: { path: '$products', preserveNullAndEmptyArrays: true },
          },
          {
            $match: matchItem,
          },
          {
            $group: {
              _id: '$_id',
              eventName: { $last: '$eventName' },
              startDate: { $last: '$startDate' },
              endDate: { $last: '$endDate' },
              type: { $last: '$type' },
              products: { $last: '$products' },
            },
          },
        ])

        let discountDoc = null
        let checkTime = moment(newDate).format('HH:mm:ss')

        _.forEach(discount, o => {
          let startDate = moment(o.startDate).format('HH:mm:ss')
          let endDate = moment(o.endDate).format('HH:mm:ss')

          if (
            Date.parse('01 Jan 2019 ' + checkTime) >=
              Date.parse('01 Jan 2019 ' + startDate) &&
            Date.parse('01 Jan 2019 ' + checkTime) <=
              Date.parse('01 Jan 2019 ' + endDate)
          ) {
            discountDoc = o
          }
        })

        // Selector
        let selector = {
          invoiceId: doc.invoiceId,
          itemId: doc.itemId,
          price: doc.price,
          discount: doc.discount,
          checkPrint: doc.checkPrint,
          extraItemDoc: { $exists: false },
          child: { $exists: false },
        }

        // discount doc exist
        if (!_.isEmpty(discountDoc)) {
          // Selector
          selector.discount = discountDoc.products.discount
        }
        // Find exist doc
        let checkExist = SaleDetails.findOne(selector)

        // check exist doc
        if (checkExist) {
          doc.discount = checkExist.discount
          doc.price = checkExist.price
          let discountEachPrice = math
            .chain(math.bignumber(checkExist.price))
            .multiply(math.bignumber(checkExist.discount))
            .divide(math.bignumber(100))
            .done()
            .toNumber()
          let priceAfterDiscount = math
            .subtract(
              math.bignumber(checkExist.price),
              math.bignumber(discountEachPrice)
            )
            .toNumber()

          SaleDetails.update(
            { _id: checkExist._id },
            {
              $inc: {
                qty: doc.qty,
                amount: _.round(priceAfterDiscount, 2),
              },
            },
            error => {
              if (!error) {
                Sales.update(
                  { _id: invoiceId },
                  { $inc: { total: _.round(priceAfterDiscount, 2) } }
                )
              }
            }
          )
        } else {
          // if discount
          if (discountDoc) {
            // Calculate discount
            let discountEachPrice = math
              .chain(math.bignumber(doc.price))
              .multiply(math.bignumber(discountDoc.products.discount))
              .divide(math.bignumber(100))
              .done()
              .toNumber()

            // Assign value to amount
            let amount = math
              .subtract(
                math.bignumber(doc.price),
                math.bignumber(discountEachPrice)
              )
              .toNumber()
            doc.discount = discountDoc.products.discount
            doc.amount = amount
          } else {
            // Amount
            doc.amount = math
              .multiply(math.bignumber(doc.qty), math.bignumber(doc.price))
              .toNumber()
          }
          // Insert
          doc.amount = _.round(doc.amount, 2)
          SaleDetails.insert(doc, error => {
            if (!error) {
              Sales.update({ _id: invoiceId }, { $inc: { total: doc.amount } })
            }
          })
        }

        return 'success'
      } catch (error) {
        // Decrement seq
        throwError(error)
      }
    }
  },
})

/*===========
  ----insertSaleDetailExta-----
=============*/

export const insertSaleDetailExtra = new ValidatedMethod({
  name: 'rest.insertSaleDetailExtra',
  mixins: [CallPromiseMixin],
  validate: null,
  run({ doc, itemList, baseCurrency }) {
    if (Meteor.isServer) {
      const invoiceId = doc.invoiceId
      try {
        const newDate = new Date()
        const checkDate = moment(newDate).toDate()
        let matchDate = {
          startDate: { $lte: checkDate },
          endDate: { $gte: checkDate },
          type: 'Item',
        }
        let matchItem = {
          'products.productId': doc.itemId,
        }

        let discount = Discounts.aggregate([
          {
            $match: matchDate,
          },
          {
            $unwind: { path: '$products', preserveNullAndEmptyArrays: true },
          },
          {
            $match: matchItem,
          },
          {
            $group: {
              _id: '$_id',
              eventName: { $last: '$eventName' },
              startDate: { $last: '$startDate' },
              endDate: { $last: '$endDate' },
              type: { $last: '$type' },
              products: { $last: '$products' },
            },
          },
        ])

        let discountDoc = null
        let checkTime = moment(newDate).format('HH:mm:ss')

        _.forEach(discount, o => {
          let startDate = moment(o.startDate).format('HH:mm:ss')
          let endDate = moment(o.endDate).format('HH:mm:ss')

          if (
            Date.parse('01 Jan 2019 ' + checkTime) >=
              Date.parse('01 Jan 2019 ' + startDate) &&
            Date.parse('01 Jan 2019 ' + checkTime) <=
              Date.parse('01 Jan 2019 ' + endDate)
          ) {
            discountDoc = o
          }
        })

        itemList.forEach(item => {
          if (item.itemType !== 'ExtraFood') {
            // Clone Deep newItemDoc
            let newItemDoc = _.cloneDeep(item)
            let extraDoc = _.cloneDeep(doc)
            //  If Discount doc exist
            if (discountDoc) {
              // Check discount type
              let discountVal = math
                .chain(math.bignumber(extraDoc.price))
                .multiply(math.bignumber(discountDoc.products.discount))
                .divide(math.bignumber(100))
                .done()
                .toNumber()
              // Amount
              let amount = extraDoc.price - discountVal

              extraDoc.discount = discountDoc.products.discount
              extraDoc.amount = _.round(amount, 2)
            } else {
              // If discount doc not exist
              let amount = math
                .multiply(
                  math.bignumber(extraDoc.qty),
                  math.bignumber(extraDoc.price)
                )
                .toNumber()

              extraDoc.amount = _.round(amount, 2)
            }

            // Check if extraItemDetail already exist in array
            if (item.extraItemDoc && item.extraItemDoc.length) {
              // Find exist item in extraItemDetail
              let extraItemDetail = _.find(item.extraItemDoc, function(obj) {
                return obj.itemId === extraDoc.itemId
              })
              // Check if this item is exist in extraItemDetail
              if (extraItemDetail) {
                // Selector
                let selector = {
                  invoiceId: invoiceId,
                  itemId: extraDoc.itemId,
                  price: extraDoc.price,
                  discount: extraDoc.discount,
                  checkPrint: extraDoc.checkPrint,
                  extraItemDoc: { $exists: false },
                  child: { $exists: false },
                }

                // Check exists
                let checkExist = SaleDetails.findOne(selector)

                if (checkExist) {
                  extraDoc.discount = checkExist.discount
                  extraDoc.price = checkExist.price
                  // discountEachPrice
                  let discountEachPrice = math
                    .chain(math.bignumber(checkExist.price))
                    .multiply(math.bignumber(checkExist.discount))
                    .divide(math.bignumber(100))
                    .done()
                    .toNumber()
                  // priceAfterDiscount
                  let priceAfterDiscount = _.round(
                    checkExist.price - discountEachPrice,
                    2
                  )

                  // Update sale detail
                  SaleDetails.update(
                    { _id: checkExist._id },
                    {
                      $inc: {
                        qty: extraDoc.qty,
                        amount: priceAfterDiscount,
                      },
                    },
                    error => {
                      if (!error) {
                        Sales.update(
                          { _id: invoiceId },
                          { $inc: { total: priceAfterDiscount } }
                        )
                      }
                    }
                  )
                } else {
                  insertDetailAndExtraDetailSyn(extraDoc, invoiceId)
                }
              } else {
                // Check if item qty > 1
                if (item.qty > 1) {
                  // Break item
                  item.qty = item.qty - 1
                  // discountEachPrice
                  let discountEachPrice = math
                    .chain(math.bignumber(item.price))
                    .multiply(math.bignumber(item.discount))
                    .divide(math.bignumber(100))
                    .done()
                    .toNumber()
                  // priceAfterDiscount
                  let priceAfterDiscount = _.round(
                    item.price - discountEachPrice,
                    2
                  )

                  item.amount = _.round(item.amount - priceAfterDiscount, 2)
                  // Assign value
                  delete newItemDoc._id
                  newItemDoc.qty = 1
                  newItemDoc.amount = priceAfterDiscount

                  // New item extra doc
                  if (
                    newItemDoc.extraItemDoc &&
                    newItemDoc.extraItemDoc.length
                  ) {
                    newItemDoc.extraItemDoc.forEach(e => {
                      // extraDiscountEachPrice
                      let extraDiscountEachPrice = math
                        .chain(math.bignumber(e.price))
                        .multiply(math.bignumber(e.discount))
                        .divide(math.bignumber(100))
                        .done()
                        .toNumber()
                      // extraDiscountEachPrice
                      let extraPriceAfterDiscount =
                        e.price - extraDiscountEachPrice

                      e.amount = _.round(
                        extraPriceAfterDiscount * newItemDoc.qty,
                        2
                      )
                      // Delete extra item _id

                      // Find extra doc to compare
                      let newExtraDoc = SaleDetails.findOne({ _id: e._id })
                      if (newExtraDoc) {
                        newExtraDoc.qty = newItemDoc.qty
                        newExtraDoc.amount = e.amount
                        delete newExtraDoc._id
                        // Update extra detail
                        SaleDetails.insert(newExtraDoc, (err, res) => {
                          if (res) {
                            e._id = res
                          }
                        })
                      }
                    })
                  }
                  // Item extra doc
                  if (item.extraItemDoc && item.extraItemDoc.length) {
                    item.extraItemDoc.forEach(e => {
                      let extraDiscountEachPrice = (e.price * e.discount) / 100
                      let extraPriceAfterDiscount =
                        e.price - extraDiscountEachPrice

                      e.amount = _.round(e.amount - extraPriceAfterDiscount, 2)
                      // Update extra detail
                      SaleDetails.update(
                        { _id: e._id },
                        { $set: { qty: newItemDoc.qty, amount: e.amount } }
                      )
                    })
                  }
                  // Update item
                  SaleDetails.update({ _id: item._id }, { $set: item })
                }

                // Asign child = true for compare detail
                extraDoc.child = true
                // Check if this item not yet exist in extraItemDetail
                SaleDetails.insert(extraDoc, (error, res) => {
                  if (!error) {
                    // Update newItemDoc.extraItemDoc
                    newItemDoc.extraItemDoc.push({
                      _id: res,
                      itemId: doc.itemId,
                      itemName: doc.itemName,
                      price: extraDoc.price,
                      discount: extraDoc.discount,
                      amount: extraDoc.amount,
                    })

                    if (newItemDoc._id) {
                      // Update detail extraItemDoc
                      SaleDetails.update(
                        { _id: newItemDoc._id },
                        { $set: { extraItemDoc: newItemDoc.extraItemDoc } }
                      )
                    } else {
                      // Insert new detail
                      SaleDetails.insert(newItemDoc)
                    }

                    // Update invoice amount
                    Sales.update(
                      { _id: invoiceId },
                      { $inc: { total: extraDoc.amount } }
                    )
                  }
                })
              }
            } else {
              // Check if item qty > 1
              if (item.qty > 1) {
                // Break item
                item.qty = item.qty - 1
                let discountEachPrice = (item.price * item.discount) / 100
                let priceAfterDiscount = parseFloat(
                  item.price - discountEachPrice
                ).toFixed(2)
                priceAfterDiscount = parseFloat(priceAfterDiscount)
                item.amount = item.amount - priceAfterDiscount
                // Assign value
                delete newItemDoc._id
                newItemDoc.qty = 1
                newItemDoc.amount = priceAfterDiscount

                // Update item
                SaleDetails.update({ _id: item._id }, { $set: item })
              }

              // Asign child = true for compare detail
              extraDoc.child = true
              // Insert detail
              SaleDetails.insert(extraDoc, (error, res) => {
                if (!error) {
                  // Update newItemDoc.extraItemDoc
                  newItemDoc.extraItemDoc = [
                    {
                      _id: res,
                      itemId: doc.itemId,
                      itemName: doc.itemName,
                      price: extraDoc.price,
                      discount: extraDoc.discount,
                      amount: extraDoc.amount,
                    },
                  ]

                  if (newItemDoc._id) {
                    // update detail
                    SaleDetails.update(
                      { _id: newItemDoc._id },
                      { $set: newItemDoc }
                    )
                  } else {
                    // Insert new detail
                    SaleDetails.insert(newItemDoc)
                  }
                  // Update Amount
                  Sales.update(
                    { _id: invoiceId },
                    { $inc: { total: extraDoc.amount } }
                  )
                }
              })
            }
          }
        })

        return 'success'
      } catch (error) {
        // Decrement seq
        throwError(error)
      }
    }
  },
})
// Insert extra detail fuction helper
// Insert sale detail
let insertDetailAndExtraDetail = function(extraDoc, invoiceId, callback) {
  SaleDetails.insert(extraDoc, error => {
    if (!error) {
      Sales.update({ _id: invoiceId }, { $inc: { total: extraDoc.amount } })
      // Callback
      callback(error)
    }
  })
}
const insertDetailAndExtraDetailSyn = Meteor.wrapAsync(
  insertDetailAndExtraDetail
)

/*===========
  ----insertSaleDetailList-----
=============*/
export const insertSaleDetailList = new ValidatedMethod({
  name: 'rest.insertSaleDetailList',
  mixins: [CallPromiseMixin],
  validate: null,
  run({ doc }) {
    if (Meteor.isServer) {
      try {
        if (doc.products.length) {
          const invoiceId = doc.invoiceId

          _.forEach(doc.products, o => {
            let detail = {
              itemId: o.productId,
              qty: o.qty,
              price: o.amount,
              invoiceId: invoiceId,
              discount: 0,
              amount: 0,
              checkPrint: false,
            }

            //
            const newDate = new Date()
            const checkDate = moment(newDate).toDate()
            let matchDate = {
              startDate: { $lte: checkDate },
              endDate: { $gte: checkDate },
              type: 'Item',
            }
            let matchItem = {
              'products.productId': detail.itemId,
            }

            let discount = Discounts.aggregate([
              {
                $match: matchDate,
              },
              {
                $unwind: {
                  path: '$products',
                  preserveNullAndEmptyArrays: true,
                },
              },
              {
                $match: matchItem,
              },
              {
                $group: {
                  _id: '$_id',
                  eventName: { $last: '$eventName' },
                  startDate: { $last: '$startDate' },
                  endDate: { $last: '$endDate' },
                  discountValue: { $last: '$discountValue' },
                  discountType: { $last: '$discountType' },
                  type: { $last: '$type' },
                  products: { $last: '$products' },
                },
              },
            ])

            let discountDoc = null
            let checkTime = moment(newDate).format('HH:mm:ss')

            _.forEach(discount, o => {
              let startDate = moment(o.startDate).format('HH:mm:ss')
              let endDate = moment(o.endDate).format('HH:mm:ss')

              if (
                Date.parse('01 Jan 2019 ' + checkTime) >=
                  Date.parse('01 Jan 2019 ' + startDate) &&
                Date.parse('01 Jan 2019 ' + checkTime) <=
                  Date.parse('01 Jan 2019 ' + endDate)
              ) {
                discountDoc = o
              }
            })

            // Check item, price and discount exist
            let selector = {
              invoiceId: detail.invoiceId,
              itemId: detail.itemId,
              price: detail.price,
              discount: detail.discount,
              checkPrint: detail.checkPrint,
              extraItemDoc: { $exists: false },
              child: { $exists: false },
            }
            if (!_.isEmpty(discountDoc)) {
              selector.discount = discountDoc.products.discount
            }
            // check exist
            let checkExist = SaleDetails.findOne(selector)
            // if exist
            if (checkExist) {
              let discountEachPrice =
                (checkExist.price * checkExist.discount) / 100
              let priceAfterDiscount = parseFloat(
                checkExist.price - discountEachPrice
              ).toFixed(2)
              priceAfterDiscount = parseFloat(priceAfterDiscount)
              let detailAmount = parseFloat(
                priceAfterDiscount * detail.qty
              ).toFixed(2)
              detailAmount = parseFloat(detailAmount)

              SaleDetails.update(
                { _id: checkExist._id },
                {
                  $inc: {
                    qty: detail.qty,
                    amount: detailAmount,
                  },
                },
                error => {
                  if (!error) {
                    Sales.update(
                      { _id: invoiceId },
                      { $inc: { total: detailAmount } }
                    )
                  }
                }
              )
            } else {
              if (discountDoc) {
                let discountVal =
                  (discountDoc.products.discount * detail.price) / 100
                let amount = parseFloat(
                  (detail.price - discountVal) * detail.qty
                ).toFixed(2)
                amount = parseFloat(amount)
                detail.discount = discountDoc.products.discount
                detail.amount = amount
              } else {
                // Amount
                detail.amount = (detail.qty * detail.price).toFixed(2)
                detail.amount = parseFloat(detail.amount)
              }

              // Insert
              SaleDetails.insert(detail, error => {
                if (!error) {
                  Sales.update(
                    { _id: invoiceId },
                    { $inc: { total: detail.amount } }
                  )
                }
              })
            }
          })
        }

        return 'success'
      } catch (error) {
        // Decrement seq
        throwError(error)
      }
    }
  },
})

/*===========
  ----updateSale-----
=============*/

export const updateSale = new ValidatedMethod({
  name: 'rest.updateSale',
  mixins: [CallPromiseMixin],
  validate: null,
  run({ doc, details }) {
    if (Meteor.isServer) {
      try {
        const invoiceId = doc._id

        Sales.update({ _id: invoiceId }, { $set: doc }, error => {
          if (!error) {
            if (details && details.length) {
              _.forEach(details, o => {
                SaleDetails.update({ _id: o._id }, { $set: o })
              })
            }
          }
        })
        return invoiceId
      } catch (error) {
        throwError(error)
      }
    }
  },
})

/*===========
  ----updateSaleDetail-----
=============*/
export const updateSaleDetail = new ValidatedMethod({
  name: 'rest.updateSaleDetail',
  mixins: [CallPromiseMixin],
  validate: null,
  run({ details, invoiceData }) {
    if (Meteor.isServer) {
      try {
        _.forEach(details, o => {
          // Update extra items
          if (o.extraItemDoc && o.extraItemDoc.length) {
            // Foreach extra doc
            o.extraItemDoc.forEach(e => {
              // Find extra detail
              let extraDoc = SaleDetails.findOne({ _id: e._id })
              if (extraDoc) {
                extraDoc.qty = o.qty
                let discountEachPrice =
                  (extraDoc.price * extraDoc.discount) / 100
                let priceAfterDiscount = parseFloat(
                  extraDoc.price - discountEachPrice
                ).toFixed(2)
                priceAfterDiscount = parseFloat(priceAfterDiscount)
                extraDoc.amount = extraDoc.qty * priceAfterDiscount

                // Update extra detail
                SaleDetails.update({ _id: extraDoc._id }, { $set: extraDoc })
              }
            })
            // Update detail
            SaleDetails.update({ _id: o._id }, { $set: o })
          } else {
            // Update detail
            SaleDetails.update({ _id: o._id }, { $set: o })
          }
        })
        // Update total
        Sales.update(
          { _id: invoiceData._id },
          { $set: { total: invoiceData.total } }
        )
        return invoiceData._id
      } catch (error) {
        throw new Meteor.Error('Remove Error', 'Invoice remove error', error)
      }
    }
  },
})

/*===========
  ----updateSaleOneClickPrint-----
=============*/

export const updateOnPrint = new ValidatedMethod({
  name: 'rest.updateOnPrint',
  mixins: [CallPromiseMixin],
  validate: null,
  run({ _id }) {
    if (Meteor.isServer) {
      try {
        // Update invoice incress bill +1
        Sales.update({ _id: _id }, { $inc: { billed: 1 } })

        // Find detail
        let details = SaleDetails.find({ invoiceId: _id }).fetch()
        _.forEach(details, o => {
          SaleDetails.update({ _id: o._id }, { $set: { checkPrint: true } })
        })

        // Return Id
        return _id
      } catch (error) {
        throwError(error)
      }
    }
  },
})

/*===========
  ----Merge Update-----
=============*/
export const updateOnMerge = new ValidatedMethod({
  name: 'rest.updateOnMerge',
  mixins: [CallPromiseMixin],
  validate: null,
  run({ currentInvoice, mergeInvoice }) {
    if (Meteor.isServer) {
      try {
        let doc = SaleDetails.find({ invoiceId: currentInvoice }).fetch()
        if (doc && doc.length) {
          _.forEach(doc, o => {
            if (o.checkPrint === false) {
              // Update id
              o.invoiceId = mergeInvoice
              // Selector
              let selector = {
                invoiceId: o.invoiceId,
                itemId: o.itemId,
                price: o.price,
                discount: o.discount,
                checkPrint: o.checkPrint,
                extraItemDoc: { $exists: false },
                child: { $exists: false },
              }

              let checkExist
              if (_.isEmpty(o.extraItemDoc) && o.child === undefined) {
                checkExist = SaleDetails.findOne(selector)
              }

              if (checkExist) {
                let qty = checkExist.qty + o.qty
                let amountBeforeDiscount = qty * checkExist.price
                let discount =
                  (amountBeforeDiscount * checkExist.discount) / 100
                let amount = amountBeforeDiscount - discount
                // Update detail
                SaleDetails.update(
                  { _id: checkExist._id },
                  {
                    $set: {
                      qty: qty,
                      amount: amount,
                    },
                  },
                  error => {
                    if (!error) {
                      // Update merge total
                      Sales.update(
                        { _id: checkExist.invoiceId },
                        { $inc: { total: o.amount } }
                      )
                      // Update current total
                      Sales.update(
                        { _id: currentInvoice },
                        { $inc: { total: -o.amount } }
                      )

                      // Remove Detail
                      SaleDetails.remove({ _id: o._id })
                    }
                  }
                )
              } else {
                // Update detail
                SaleDetails.update(
                  { _id: o._id },
                  {
                    $set: o,
                  },
                  error => {
                    if (!error) {
                      // Update merge total
                      Sales.update(
                        { _id: o.invoiceId },
                        { $inc: { total: o.amount } }
                      )
                      // Update current total
                      Sales.update(
                        { _id: currentInvoice },
                        { $inc: { total: -o.amount } }
                      )
                    }
                  }
                )
              }
            }
          })
        }

        // Find table
        let tableId = Sales.findOne({ _id: mergeInvoice }).tableId

        return { _id: mergeInvoice, tableId: tableId }
      } catch (error) {
        throwError(error)
      }
    }
  },
})

/*===========
  ----Transfer Item-----
=============*/
export const updateOnTransfer = new ValidatedMethod({
  name: 'rest.updateOnTransfer',
  mixins: [CallPromiseMixin],
  validate: null,
  run({ doc, currentDoc, currentInvoice, transferInvoice }) {
    if (Meteor.isServer) {
      try {
        let newAmount = 0
        // New Detail
        _.forEach(doc, o => {
          // new Amount
          newAmount += o.amount
          const selector = {
            invoiceId: o.invoiceId,
            itemId: o.itemId,
            price: o.price,
            discount: o.discount,
            checkPrint: o.checkPrint,
            extraItemDoc: { $exists: false },
            child: { $exists: false },
          }
          // Check for exist doc
          let checkExist
          if (_.isEmpty(o.extraItemDoc) && o.child === undefined) {
            checkExist = SaleDetails.findOne(selector)
          }

          if (checkExist) {
            // Update detail
            SaleDetails.update(
              { _id: checkExist._id },
              {
                $inc: {
                  qty: o.qty,
                  amount: o.amount,
                },
              }
            )
          } else {
            // Insert detail
            if (o.extraItemDoc && o.extraItemDoc.length) {
              o.extraItemDoc.forEach(e => {
                // new Amount
                newAmount += e.amount
                // Find extra doc
                let extraItemDoc = SaleDetails.findOne({ _id: e._id })
                if (extraItemDoc) {
                  // Insert extra doc
                  delete extraItemDoc._id
                  extraItemDoc.invoiceId = o.invoiceId
                  extraItemDoc.qty = o.qty
                  extraItemDoc.amount = e.amount
                  // Insert detail
                  e._id = extraItemInsertSyn(extraItemDoc)
                  // End asyn
                }
              })
            }

            // Insert detail
            SaleDetails.insert(o)
          }
        })

        //  Current Details
        _.forEach(currentDoc, o => {
          // If qty = 0
          if (o.qty === 0) {
            // remove detail
            SaleDetails.remove({ _id: o._id }, error => {
              if (!error) {
                if (o.extraItemDoc && o.extraItemDoc.length) {
                  o.extraItemDoc.forEach(e => {
                    SaleDetails.remove({ _id: e._id })
                  })
                }
              }
            })
          } else {
            // update detail
            SaleDetails.update({ _id: o._id }, { $set: o }, error => {
              if (!error) {
                if (o.extraItemDoc && o.extraItemDoc.length) {
                  o.extraItemDoc.forEach(e => {
                    SaleDetails.update(
                      { _id: e._id },
                      { $set: { qty: o.qty, amount: e.amount } }
                    )
                  })
                }
              }
            })
          }
        })
        // Update amount
        Sales.update({ _id: transferInvoice }, { $inc: { total: newAmount } })
        // update current invoice
        Sales.update({ _id: currentInvoice }, { $inc: { total: -newAmount } })
        // Transfer Table
        let tableId = Sales.findOne({ _id: transferInvoice }).tableId

        return { _id: transferInvoice, tableId: tableId }
      } catch (error) {
        throwError(error)
      }
    }
  },
})

// updateOnTransfer & split function helper

let findExtraItem = function(extraItemDoc, callback) {
  SaleDetails.insert(extraItemDoc, (err, res) => {
    if (res) {
      callback(err, res)
    }
  })
}
const extraItemInsertSyn = Meteor.wrapAsync(findExtraItem)

/*===========
  ----splitSale-----
=============*/
export const splitSale = new ValidatedMethod({
  name: 'rest.splitSale',
  mixins: [CallPromiseMixin],
  validate: null,
  run({ doc, details, currentDetails, currentInvoice }) {
    if (Meteor.isServer) {
      const invoiceId = getNextSeq({
        // Mandatory
        filter: {
          _id: 'rest_sales',
        },
        // Optional
        opts: {
          seq: 1,
          paddingType: 'start',
          paddingLength: 4,
          paddingChar: '0',
          prefix: moment(doc.date).format('YYMMDD'),
        },
      })
      try {
        // Get return to wait
        doc._id = spliteSaleSyn(
          doc,
          details,
          currentDetails,
          currentInvoice,
          invoiceId
        )

        // return new invoice id
        return doc._id
      } catch (error) {
        // Decrement seq
        getNextSeq({
          filter: { _id: 'rest_sales' },
          opts: { seq: -1 },
        })
        SaleDetails.remove({ invoiceId: invoiceId })
        Sales.remove({ _id: invoiceId })
        throwError(error)
      }
    }
  },
})

// updateOnTransfer & split function helper

let spliteSaleFunc = function(
  doc,
  details,
  currentDetails,
  currentInvoice,
  invoiceId,
  callback
) {
  doc._id = invoiceId.toString()

  // Insert sale
  Sales.insert(doc, (error, result) => {
    if (!error) {
      _.forEach(details, o => {
        // Update invoice
        o.invoiceId = doc._id
        //    delete o._id
        delete o._id
        // Insert detail
        if (o.extraItemDoc && o.extraItemDoc.length) {
          o.extraItemDoc.forEach(e => {
            // Find extra doc
            let extraItemDoc = SaleDetails.findOne({ _id: e._id })
            if (extraItemDoc) {
              // Insert extra doc
              delete extraItemDoc._id
              extraItemDoc.invoiceId = o.invoiceId
              extraItemDoc.qty = o.qty
              extraItemDoc.amount = e.amount
              // Insert detail
              e._id = extraItemInsertSyn(extraItemDoc)
              // End asyn
            }
          })
        }
        // Insert detail
        SaleDetails.insert(o)
      })

      //  Current Details
      _.forEach(currentDetails, o => {
        // If qty = 0
        if (o.qty === 0) {
          // remove detail
          SaleDetails.remove({ _id: o._id }, error => {
            if (!error) {
              if (o.extraItemDoc && o.extraItemDoc.length) {
                o.extraItemDoc.forEach(e => {
                  SaleDetails.remove({ _id: e._id })
                })
              }
            }
          })
        } else {
          // update detail
          SaleDetails.update({ _id: o._id }, { $set: o }, error => {
            if (!error) {
              if (o.extraItemDoc && o.extraItemDoc.length) {
                o.extraItemDoc.forEach(e => {
                  SaleDetails.update(
                    { _id: e._id },
                    { $set: { qty: o.qty, amount: e.amount } }
                  )
                })
              }
            }
          })
        }
      })

      // Update old invoice amount
      Sales.update({ _id: currentInvoice }, { $inc: { total: -doc.total } })
    }
    callback(error, doc._id)
  })
}
const spliteSaleSyn = Meteor.wrapAsync(spliteSaleFunc)

/*===========
  ----cancelCopySale-----
=============*/
export const cancelCopySale = new ValidatedMethod({
  name: 'rest.cancelCopySale',
  mixins: [CallPromiseMixin],
  validate: null,
  run({ doc, details }) {
    if (Meteor.isServer) {
      const invoiceId = getNextSeq({
        // Mandatory
        filter: {
          _id: 'rest_sales',
        },
        // Optional
        opts: {
          seq: 1,
          paddingType: 'start',
          paddingLength: 4,
          paddingChar: '0',
          prefix: moment(doc.date).format('YYMMDD'),
        },
      })
      try {
        const currentInvoice = doc.refId
        // Update invoice status
        Sales.update(
          { _id: currentInvoice },
          { $set: { status: 'Cancel', 'statusDate.close': doc.date } }
        )
        // Insert a copy invoice

        doc._id = invoiceId.toString()
        Sales.insert(doc, error => {
          if (!error) {
            if (details && details.length) {
              _.forEach(details, o => {
                o.invoiceId = doc._id
                delete o._id
                SaleDetails.insert(o)
              })
            }
          }
        })
        // return
        return doc._id
      } catch (error) {
        // Decrement seq
        getNextSeq({
          filter: { _id: 'rest_sales' },
          opts: { seq: -1 },
        })
        throwError(error)
      }
    }
  },
})

/*===========
  ----removeSale-----
=============*/
export const removeSale = new ValidatedMethod({
  name: 'rest.removeSale',
  mixins: [CallPromiseMixin],
  validate: null,
  run({ _id }) {
    if (Meteor.isServer) {
      try {
        Sales.remove({ _id: _id }, error => {
          if (!error) {
            SaleDetails.remove({ invoiceId: _id })
          }
        })
        return _id
      } catch (error) {
        throw new Meteor.Error('Remove Error', 'Invoice remove error', error)
      }
    }
  },
})

/*===========
  ----removeSaleDetail-----
=============*/
export const removeSaleDetail = new ValidatedMethod({
  name: 'rest.removeSaleDetail',
  mixins: [CallPromiseMixin],
  validate: null,
  run({ detail, invoiceData }) {
    if (Meteor.isServer) {
      try {
        // Foreach detail
        detail.forEach(o => {
          SaleDetails.remove({ _id: o._id }, error => {
            if (!error) {
              if (o.extraItemDoc && o.extraItemDoc.length) {
                o.extraItemDoc.forEach(e => {
                  SaleDetails.remove({ _id: e._id })
                })
              }
            }
          })
        })
        // Update total
        Sales.update(
          { _id: invoiceData._id },
          { $set: { total: invoiceData.total } }
        )
        return invoiceData._id
      } catch (error) {
        throw new Meteor.Error('Remove Error', 'Invoice remove error', error)
      }
    }
  },
})

/*===========
  ----removeEmptySale-----
=============*/
export const removeEmptySale = new ValidatedMethod({
  name: 'rest.removeEmptySale',
  mixins: [CallPromiseMixin],
  validate: null,
  run({ selector }) {
    if (Meteor.isServer) {
      selector = selector || {}
      try {
        let saleDoc = Sales.find(selector).fetch()
        _.forEach(saleDoc, o => {
          if (
            SaleDetails.find({ invoiceId: o._id }).count() === 0 &&
            o.numOfGuest === 0
          ) {
            Sales.remove({ _id: o._id })
          }
        })

        return 'success'
      } catch (error) {
        throw new Meteor.Error('Remove Error', 'Invoice remove error', error)
      }
    }
  },
})
