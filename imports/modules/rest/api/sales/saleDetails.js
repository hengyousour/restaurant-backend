import { Mongo } from 'meteor/mongo'
import SimpleSchema from 'simpl-schema'
import { Optional } from 'ag-grid'

const SaleDetails = new Mongo.Collection('rest_saleDetails')

SaleDetails.schema = new SimpleSchema({
  itemId: {
    type: String,
  },
  qty: {
    type: Number,
  },
  price: {
    type: Number,
  },
  discount: {
    type: Number,
  },
  amount: {
    type: Number,
  },
  invoiceId: {
    type: String,
  },
  checkPrint: {
    type: Boolean,
  },
  child: {
    type: Boolean,
    optional: true,
  },
  extraItemDoc: {
    type: Array,
    optional: true,
  },
  'extraItemDoc.$': {
    type: Object,
    optional: true,
  },
  'extraItemDoc.$._id': {
    type: String,
  },
  'extraItemDoc.$.itemId': {
    type: String,
  },
  'extraItemDoc.$.itemName': {
    type: String,
  },
  'extraItemDoc.$.price': {
    type: Number,
  },
  'extraItemDoc.$.discount': {
    type: Number,
  },
  'extraItemDoc.$.amount': {
    type: Number,
  },
})

SaleDetails.attachSchema(SaleDetails.schema)

export default SaleDetails
