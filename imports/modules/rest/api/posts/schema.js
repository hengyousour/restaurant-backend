import SimpleSchema from 'simpl-schema'

export default new SimpleSchema({
  postDate: {
    type: Date,
  },
  categoryIds: {
    type: Array,
  },
  'categoryIds.$': {
    type: String,
  },
  title: {
    type: String,
  },
  body: {
    type: String,
  },
  status: {
    type: String,
  },
  memo: {
    type: String,
    optional: true,
  },
  branchId: {
    type: String,
  },
})
