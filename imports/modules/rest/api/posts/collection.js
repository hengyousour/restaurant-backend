import { Mongo } from 'meteor/mongo'
import Schema from './schema'

const Posts = new Mongo.Collection('rest_posts')
Posts.attachSchema(Schema)
Posts.timestamp()

export default Posts
