import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'
import moment from 'moment'

import { throwError } from '../../../../utils/security'
import rateLimit from '/imports/utils/rate-limit'
import getNextSeq from '/imports/utils/get-next-seq'

import PostSchema from './schema'
import Posts from './collection'

// Data table
export const dataTablePosts = new ValidatedMethod({
  name: 'rest.dataTablePosts',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    tableQuery: {
      type: Object,
      blackbox: true,
    },
    params: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ tableQuery, params }) {
    if (Meteor.isServer) {
      Meteor._sleepForMs(500)
      tableQuery = tableQuery || {}
      params = params || {}

      // Selector
      const selector = {}
      if (params.date) {
        selector.postDate = {
          $gte: moment(params.date[0])
            .startOf('day')
            .toDate(),
          $lte: moment(params.date[1])
            .endOf('day')
            .toDate(),
        }
      }

      // Options
      const skip = (tableQuery.page - 1) * tableQuery.pageSize
      const limit = tableQuery.pageSize
      const sort = {
        [tableQuery.sortInfo.prop]:
          tableQuery.sortInfo.order === 'ascending' ? 1 : -1,
      }
      const options = { sort, skip, limit }

      let data = Posts.find(selector, options).fetch()
      let total = Posts.find(selector).count()

      return { data, total }
    }
  },
})

// Find
export const findPosts = new ValidatedMethod({
  name: 'rest.findPosts',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
    options: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector, options }) {
    if (Meteor.isServer) {
      selector = selector || {}
      options = options || {}

      return Posts.find(selector, options).fetch()
    }
  },
})

// Find One
export const findOnePost = new ValidatedMethod({
  name: 'rest.findOnePost',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return Posts.findOne({ _id })
    }
  },
})

// Insert
export const insertPost = new ValidatedMethod({
  name: 'rest.insertPost',
  mixins: [CallPromiseMixin],
  validate: PostSchema.validator(),
  run(doc) {
    if (Meteor.isServer) {
      const _id = getNextSeq({
        // Mandatory
        filter: {
          _id: 'rest_post',
          type: doc.branchId, // BranchId
        },
        // Optional
        opts: {
          seq: 1,
          // paddingType: 'start',
          // paddingLength: 6,
          // paddingChar: '0',
          prefix: `${doc.branchId}-`,
        },
      })

      try {
        doc._id = _id
        return Posts.insert(doc)
      } catch (error) {
        // Decrement seq
        getNextSeq({
          filter: { _id: 'rest_post', type: doc.branchId },
          opts: { seq: -1 },
        })
        throwError(error)
      }
    }
  },
})

// Update
export const updatePost = new ValidatedMethod({
  name: 'rest.updatePost',
  mixins: [CallPromiseMixin],
  validate: _.clone(PostSchema)
    .extend({
      _id: String,
    })
    .validator(),
  run(doc) {
    if (Meteor.isServer) {
      return Posts.update({ _id: doc._id }, { $set: doc })
    }
  },
})

// Upsert Category
export const upsertPost = new ValidatedMethod({
  name: 'rest.upsertPost',
  mixins: [CallPromiseMixin],
  validate: _.clone(PostSchema)
    .extend({
      _id: {
        type: String,
        optional: true,
      },
    })
    .validator(),
  run(doc) {
    if (Meteor.isServer) {
      let _id
      if (!doc._id) {
        _id = getNextSeq({
          filter: {
            _id: 'rest_post',
          },
          opts: { seq: 1 },
        })
        doc._id = _id.toString()
      }
      try {
        return Posts.upsert({ _id: doc._id }, { $set: doc })
      } catch (error) {
        if (_id) {
          _id = getNextSeq({
            filter: { _id: 'rest_post' },
            seq: { seq: -1 },
          })
        }
        throwError(error)
      }
    }
  },
})

// Soft remove
export const softRemovePost = new ValidatedMethod({
  name: 'rest.softRemovePost',
  mixins: [CallPromiseMixin],
  // validate: null,
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return Posts.softRemove(_id)
    }
  },
})

// Restore
export const restorePost = new ValidatedMethod({
  name: 'rest.restorePost',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return Posts.restore(_id)
    }
  },
})

// Remove
export const removePost = new ValidatedMethod({
  name: 'rest.removePost',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return Posts.remove(_id)
    }
  },
})

rateLimit({
  methods: [
    findPosts,
    findOnePost,
    insertPost,
    updatePost,
    softRemovePost,
    restorePost,
    removePost,
  ],
})
