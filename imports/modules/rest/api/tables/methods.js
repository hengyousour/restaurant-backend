import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'

import { throwError } from '../../../../utils/security'
import getNextSeq from '/imports/utils/get-next-seq'
import TableSchema from './schema'
import Tables from './tables'
import Floors from '../floors/floors'
import Sales from '../sales/sales'

//find
export const findTables = new ValidatedMethod({
    name: 'rest.findTables',
    mixins: [CallPromiseMixin],
    validate: new SimpleSchema({
        selector: {
            type: Object,
            blackbox: true,
            optional: true,
        },
        options: {
            type: Object,
            blackbox: true,
            optional: true,
        },
    }).validator(),
    run({ selector, options }) {
        if (Meteor.isServer) {
            selector = selector || {}
            options = options || {}

            return Tables.find(selector, options).fetch()
        }
    },
})

// Find One
export const findOneTable = new ValidatedMethod({
    name: 'rest.findOneTable',
    mixins: [CallPromiseMixin],
    validate: new SimpleSchema({
        _id: String,
    }).validator(),
    run({ _id }) {
        if (Meteor.isServer) {
            return Tables.findOne({ _id })
        }
    },
})

// Find  Table Aggregate
export const findTableAggregate = new ValidatedMethod({
    name: 'rest.findTableAggregate',
    mixins: [CallPromiseMixin],
    validate: null,
    run() {
        if (Meteor.isServer) {
            let doc = Tables.aggregate([{
                    $lookup: {
                        from: 'rest_floors',
                        localField: 'floorId',
                        foreignField: '_id',
                        as: 'floorDoc',
                    },
                },
                {
                    $unwind: { path: '$floorDoc', preserveNullAndEmptyArrays: true },
                },
                {
                    $addFields: {
                        floorName: '$floorDoc.name',
                    },
                },
                {
                    $project: {
                        floorDoc: 0,
                    },
                },
            ])
            return doc
        }
    },
})

// Find Aggregate
export const findTableAndFloor = new ValidatedMethod({
    name: 'rest.findTableAndFloor',
    mixins: [CallPromiseMixin],
    validate: null,
    run() {
        if (Meteor.isServer) {
            let doc = Floors.aggregate([{
                    $lookup: {
                        from: 'rest_tables',
                        localField: '_id',
                        foreignField: 'floorId',
                        as: 'tableDoc',
                    },
                },
                {
                    $unwind: {
                        path: '$tableDoc',
                        preserveNullAndEmptyArrays: true,
                    },
                },
                {
                    $group: {
                        _id: { tableId: '$tableDoc._id', floorId: '$_id' },
                        floorId: { $last: '$_id' },
                        name: { $last: '$name' },
                        tableDoc: { $last: '$tableDoc' },
                    },
                },
                {
                    $group: {
                        _id: '$_id.floorId',
                        name: { $last: '$name' },
                        tableDocs: {
                            $push: {
                                _id: '$tableDoc._id',
                                floorId: '$tableDoc._floorId',
                                name: '$tableDoc.name',
                                numOfGuest: '$tableDoc.numOfGuest',
                                floorName: '$name',
                            },
                        },
                    },
                },
            ])

            let filterFloors = [],
                filterTables = []

            //  Sort floors
            doc.sort(function(a, b) {
                return a.name.localeCompare(b.name, undefined, {
                    numeric: true,
                    sensitivity: 'base',
                })
            })

            // Finish aggregate
            _.forEach(doc, o => {
                // Remove undefined
                _.remove(o.tableDocs, function(tableDoc) {
                    return tableDoc._id === undefined
                })

                // Foreach to get sale doc
                _.forEach(o.tableDocs, tableProp => {
                    if (!_.isEmpty(tableProp._id)) {
                        let saleDoc = Sales.find({
                            tableId: tableProp._id,
                            status: 'Open',
                        }).fetch()

                        if (saleDoc.length > 0) {
                            tableProp.total = _.sumBy(saleDoc, function(prop) {
                                return prop.total
                            })
                        } else if (saleDoc.length == 0) {
                            tableProp.total = 0
                        }
                        tableProp.guestCount = _.sumBy(saleDoc, function(prop) {
                            return prop.numOfGuest
                        })
                    }
                })

                // Push doc
                filterTables.push(o.tableDocs)

                // Sort tables
                o.tableDocs.sort(function(a, b) {
                    return a.name.localeCompare(b.name, undefined, {
                        numeric: true,
                        sensitivity: 'base',
                    })
                })
            })

            // Assign
            filterTables = _.flatten(filterTables)
            filterFloors = doc
                // RETURn
            return { filterFloors, filterTables }
        }
    },
})

// Update Table and Insert
export const upsertTable = new ValidatedMethod({
    name: 'rest.upsertTable',
    mixins: [CallPromiseMixin],
    validate: _.clone(TableSchema)
        .extend({
            _id: {
                type: String,
                optional: true,
            },
        })
        .validator(),
    run(doc) {
        if (Meteor.isServer) {
            let _id
            if (!doc._id) {
                _id = getNextSeq({
                    filter: {
                        _id: 'rest_Table',
                    },
                    opts: { seq: 1 },
                })
                doc._id = _id.toString()
            }
            try {
                return Tables.upsert({ _id: doc._id }, { $set: doc })
            } catch (error) {
                if (_id) {
                    _id = getNextSeq({
                        filter: { _id: 'rest_Table' },
                        seq: { seq: -1 },
                    })
                }
                throwError(error)
            }
        }
    },
})

// Update //

export const updateTable = new ValidatedMethod({
    name: 'rest.updateTable',
    mixins: [CallPromiseMixin],
    validate: null,
    run({ doc }) {
        if (Meteor.isServer) {
            return Tables.update({ _id: doc._id }, { $set: doc })
        }
    },
})

// Remove
export const removeTable = new ValidatedMethod({
    name: 'rest.removeTable',
    mixins: [CallPromiseMixin],
    validate: new SimpleSchema({
        _id: String,
    }).validator(),
    run({ _id }) {
        if (Meteor.isServer) {
            return Tables.remove(_id)
        }
    },
})

// usedGroup
export const usedTable = new ValidatedMethod({
    name: 'rest.usedTable',
    mixins: [CallPromiseMixin],
    validate: null,
    run(_id) {
        if (Meteor.isServer) {
            let used = false
            let saleDoc = Sales.findOne({ tableId: _id })
            if (saleDoc) {
                used = true
            } else {
                used = false
            }

            return { used }
        }
    },
})