import SimpleSchema from 'simpl-schema'

export default new SimpleSchema({
  name: {
    type: String,
  },
  numOfGuest: {
    type: Number,
  },
  floorId: {
    type: String,
  },
  status: {
    type: String,
    optional: true,
    allowedValues: ['Open', 'Busy'],
  },
})
