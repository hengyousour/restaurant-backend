import SimpleSchema from 'simpl-schema'
import { Mongo } from 'meteor/mongo'
const Tables = new Mongo.Collection('rest_tables')

Tables.schema = new SimpleSchema({
  name: {
    type: String,
  },
  numOfGuest: {
    type: Number,
  },
  floorId: {
    type: String,
  },
})
Tables.attachSchema(Tables.schema)
export default Tables
