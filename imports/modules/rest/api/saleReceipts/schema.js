import SimpleSchema from 'simpl-schema'

export default new SimpleSchema({
  date: {
    type: Date,
  },
  openAmount: {
    type: Number,
  },
  receiveAmount: {
    type: Number,
  },
  status: {
    type: String,
  },
  memo: {
    type: String,
    optional: true,
  },
  invoiceId: {
    type: String,
  },
  guestId: {
    type: String,
  },
  employeeId: {
    type: String,
  },
})
