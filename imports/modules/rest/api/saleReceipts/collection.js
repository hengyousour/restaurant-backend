import { Mongo } from 'meteor/mongo'
import Schema from './schema'

const SaleReceipts = new Mongo.Collection('rest_saleReceipts')
SaleReceipts.attachSchema(Schema)
SaleReceipts.timestamp()

export default SaleReceipts
