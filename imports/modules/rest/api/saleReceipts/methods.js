import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'
import moment from 'moment'

import { throwError } from '../../../../utils/security'
import rateLimit from '/imports/utils/rate-limit'
import getNextSeq from '/imports/utils/get-next-seq'

// SaleReceipt
// import SaleReceiptSchema from './schema'
import SaleReceipts from './collection'
// Invoice
import Sales from '../sales/sales'
import SaleDetails from '../sales/saleDetails'
// Product
import Products from '../products/collection'
// Inventories
import { insertInventories } from '../inventories/methods'

// Table Status
import Tables from '../tables/tables'

// Find
export const findSaleReceipts = new ValidatedMethod({
  name: 'rest.findSaleReceipts',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
    options: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector, options }) {
    if (Meteor.isServer) {
      selector = selector || {}
      options = options || {}

      return SaleReceipts.find(selector, options).fetch()
    }
  },
})

// Find One
export const findOneSaleReceipt = new ValidatedMethod({
  name: 'rest.findOneSaleReceipt',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector }) {
    if (Meteor.isServer) {
      selector = selector || {}

      return SaleReceipts.findOne(selector)
    }
  },
})

// Insert
export const insertSaleReceipt = new ValidatedMethod({
  name: 'rest.insertSaleReceipt',
  mixins: [CallPromiseMixin],
  validate: null,
  run({
    doc,
    saleDoc,
    customerId,
    checkInventoryInsert,
    baseCurrency,
    updateTotal,
  }) {
    if (Meteor.isServer) {
      try {
        let inventoryData = saleDoc.details
        doc.guestId = customerId

        if (doc.memo !== '') {
          doc.status = 'Waive'
        } else if (doc.receiveAmount >= doc.openAmount) {
          doc.status = 'Closed'
        } else if (doc.receiveAmount === 0 || doc.receiveAmount > 0) {
          doc.status = 'Partial'
        }
        // Update Sales
        if (doc.status === 'Closed') {
          Sales.update(
            { _id: doc.invoiceId },
            {
              $set: {
                status: doc.status,
                'statusDate.close': doc.date,
                guestId: customerId,
              },
              $inc: { totalReceived: doc.receiveAmount },
            }
          )
        } else if (doc.status === 'Partial') {
          Sales.update(
            { _id: doc.invoiceId },
            {
              $set: {
                status: doc.status,
                'statusDate.partial': doc.date,
                guestId: customerId,
              },
              $inc: { totalReceived: doc.receiveAmount },
            }
          )
        }

        // Check Inventories Insert Only From Sale Form
        _.forEach(inventoryData, o => {
          // Insert Inventory
          if (checkInventoryInsert == true) {
            if (o.itemType == 'Product') {
              let product = Products.findOne(o.itemId)
              let total = -o.qty * product.cost
              insertInventories({
                refId: o._id,
                refType: 'Sale',
                tranDate: doc.date,
                productId: o.itemId,
                qty: -o.qty,
                amount: product.cost,
                total: total,
              })
            } else if (o.itemType == 'Dish') {
              let dish = Products.findOne(o.itemId)
              _.forEach(dish.ingredients, element => {
                let qty = o.qty * element.qty
                let total = -qty * element.cost
                insertInventories({
                  refId: o._id,
                  refType: 'Sale',
                  tranDate: doc.date,
                  productId: element.ingredientId,
                  qty: -qty,
                  amount: element.cost,
                  total: total,
                })
              })
            } else if (o.itemType == 'Catalog') {
              let catalog = Products.findOne(o.itemId)
              _.forEach(catalog.products, element => {
                let qty = o.qty * element.qty
                let total = -qty * element.cost
                insertInventories({
                  refId: o._id,
                  refType: 'Sale',
                  tranDate: doc.date,
                  productId: element.productId,
                  qty: -qty,
                  amount: element.cost,
                  total: total,
                })
              })
            }
          }
        })

        // Insert Sale receipt
        return SaleReceipts.insert(doc)
      } catch (error) {
        // Decrement seq
        getNextSeq({
          filter: { _id: 'rest_purchasePayments' },
          opts: { seq: -1 },
        })

        throwError(error)
      }
    }
  },
})
