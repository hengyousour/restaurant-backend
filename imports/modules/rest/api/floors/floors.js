import SimpleSchema from 'simpl-schema'
import { Mongo } from 'meteor/mongo'
const Floors = new Mongo.Collection('rest_floors')

Floors.schema = new SimpleSchema({
  name: {
    type: String,
  },
})
Floors.attachSchema(Floors.schema)
export default Floors
