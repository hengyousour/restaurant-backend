import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'

import { throwError } from '../../../../utils/security'
import getNextSeq from '/imports/utils/get-next-seq'
import FloorSchema from './schema'
import Floors from './floors'
import Tables from '../tables/tables'

// Find
export const findFloors = new ValidatedMethod({
  name: 'rest.findFloors',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
    options: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector, options }) {
    if (Meteor.isServer) {
      selector = selector || {}
      options = options || {}

      return Floors.find(selector, options).fetch()
    }
  },
})

// Find One
export const findOneFloor = new ValidatedMethod({
  name: 'rest.findOneFloor',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return Floors.findOne({ _id })
    }
  },
})

// Upsert
export const upsertFloor = new ValidatedMethod({
  name: 'rest.upsertFloor',
  mixins: [CallPromiseMixin],
  validate: _.clone(FloorSchema)
    .extend({
      _id: {
        type: String,
        optional: true,
      },
    })
    .validator(),
  run(doc) {
    if (Meteor.isServer) {
      try {
        return Floors.upsert({ _id: doc._id }, { $set: doc })
      } catch (error) {
        throwError(error)
      }
    }
  },
})

export const removeFloor = new ValidatedMethod({
  name: 'rest.removeFloor',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return Floors.remove(_id)
    }
  },
})

// usedGroup
export const usedFloor = new ValidatedMethod({
  name: 'rest.usedFloor',
  mixins: [CallPromiseMixin],
  validate: null,
  run(_id) {
    if (Meteor.isServer) {
      let used = false
      let tableDoc = Tables.findOne({ floorId: _id })
      if (tableDoc) {
        used = true
      } else {
        used = false
      }

      return { used }
    }
  },
})
