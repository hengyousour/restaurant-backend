import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'
import moment from 'moment'

import { throwError } from '../../../../utils/security'
import rateLimit from '/imports/utils/rate-limit'
import getNextSeq from '/imports/utils/get-next-seq'

import Purchases from './purchases'
import PurchaseDetails from './purchaseDetails'
// Inventory
import PurchasePayments from '../purchasePayments/collection'
// Inventory
import Inventories from '../inventories/collection'

import { insertInventories, removeInventories } from '../inventories/methods'
import Products from '../products/collection'
// Find
export const findPurchases = new ValidatedMethod({
  name: 'rest.findPurchases',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
    options: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector, options }) {
    if (Meteor.isServer) {
      selector = selector || {}
      options = options || {}

      let purchaseDocs = Purchases.find(selector, options).fetch()
      _.forEach(purchaseDocs, o => {
        o.details = PurchaseDetails.find({ refId: o._id }).fetch()
      })

      return purchaseDocs
    }
  },
})

// Find findPurchaseAggregate
export const findPurchaseAggregate = new ValidatedMethod({
  name: 'rest.findPurchaseAggregate',
  mixins: [CallPromiseMixin],
  validate: null,
  run() {
    if (Meteor.isServer) {
      let purchaseDocs = Purchases.aggregate([
        {
          $lookup: {
            from: 'rest_suppliers',
            localField: 'supplierId',
            foreignField: '_id',
            as: 'supplierDoc',
          },
        },
        {
          $unwind: { path: '$supplierDoc', preserveNullAndEmptyArrays: true },
        },
        { $addFields: { supplierName: '$supplierDoc.name' } },
        { $project: { supplierDoc: 0 } },
      ])
      _.forEach(purchaseDocs, o => {
        o.details = PurchaseDetails.find({ refId: o._id }).fetch()
      })

      return purchaseDocs
    }
  },
})

// Find One
export const findOnePurchase = new ValidatedMethod({
  name: 'rest.findOnePurchase',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      let purchase = Purchases.findOne({ _id: _id })
      let detail = PurchaseDetails.find({ refId: _id }).fetch()
      return { purchase, detail }
    }
  },
})

// Insert
export const insertPurchase = new ValidatedMethod({
  name: 'rest.insertPurchase',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    doc: Purchases.schema,
    details: {
      type: Array,
    },
    'details.$': _.clone(PurchaseDetails.schema).omit('refId'),
  }).validator(),
  run({ doc, details }) {
    if (Meteor.isServer) {
      const purchaseId = getNextSeq({
        // Mandatory
        filter: {
          _id: 'rest_purchases',
          // type: '001' // BranchId
        },
        // Optional
        opts: {
          seq: 1,
          paddingType: 'start',
          paddingLength: 4,
          paddingChar: '0',
          prefix: moment(doc.date).format('YYMMDD'),
        },
      })
      try {
        let products = details
        doc._id = purchaseId.toString()

        _.remove(products, o => {
          return o.productId == ''
        })

        Purchases.insert(doc)
        // Call insert detail function
        insertPurchaseDetails({
          doc: products,
          tranDate: doc.date,
          purchaseId: doc._id,
        })

        // Insert payment if doc.type == 'Cash'
        if (doc.type == 'Cash') {
          let payment = {
            date: doc.date,
            openAmount: doc.total,
            payAmount: doc.totalPaid,
            status: doc.status,
            memo: '',
            purchaseId: doc._id,
            supplierId: doc.supplierId,
            employeeId: doc.employeeId,
          }
          PurchasePayments.insert(payment)
        }

        return doc._id
      } catch (error) {
        // Decrement seq
        getNextSeq({
          filter: { _id: 'rest_purchases' },
          opts: { seq: -1 },
        })
        Inventories.remove({ refId: purchaseId })
        Purchases.remove({ _id: purchaseId })
        throwError(error)
      }
    }
  },
})

// Update
export const updatePurchase = new ValidatedMethod({
  name: 'rest.updatePurchase',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    doc: _.clone(Purchases.schema).extend({
      _id: {
        type: String,
        optional: true,
      },
    }),
    details: {
      type: Array,
    },
    'details.$': _.clone(PurchaseDetails.schema).extend({
      _id: {
        type: String,
        optional: true,
      },
      refId: {
        type: String,
        optional: true,
      },
    }),
  }).validator(),
  run({ doc, details }) {
    if (Meteor.isServer) {
      const purchaseId = doc._id
      try {
        let products = details

        _.remove(products, o => {
          return o.productId == ''
        })
        let selector = {
          _id: purchaseId,
        }

        const prevDoc = aggregatePrevDoc(selector)

        Purchases.update({ _id: purchaseId }, { $set: doc })

        // Remove inventories
        removeInventories({
          prevDoc,
        })

        // Remove purchase details
        removePurchaseDetails({ refId: purchaseId })

        // Remove insert details and inventories
        insertPurchaseDetails({
          doc: products,
          tranDate: doc.date,
          purchaseId: purchaseId,
        })

        return purchaseId
      } catch (error) {
        throwError(error)
      }
    }
  },
})

// Remove
export const removePurchase = new ValidatedMethod({
  name: 'rest.removePurchase',
  mixins: [CallPromiseMixin],
  validate: null,
  run(purchaseId) {
    if (Meteor.isServer) {
      try {
        let selector = {
          _id: purchaseId,
        }
        const prevDoc = aggregatePrevDoc(selector)
        Purchases.remove({ _id: purchaseId })

        //remove detail purchase
        removePurchaseDetails({ refId: purchaseId })

        //remove inventory
        removeInventories({
          prevDoc,
        })

        return 'success'
      } catch (error) {
        throwError(error)
      }
    }
  },
})

// Insert detail function
const insertPurchaseDetails = ({ doc, tranDate, purchaseId }) => {
  new SimpleSchema({
    doc: {
      type: Array,
    },
    'doc.$': Object,
    tranDate: Date,
    purchaseId: String,
  }).validator({
    doc,
    tranDate,
    purchaseId,
  })
  try {
    _.forEach(doc, item => {
      let data = {
        productId: item.productId,
        qty: item.qty,
        cost: item.cost,
        amount: item.amount,
        refId: purchaseId,
      }
      // check inventories

      // Add stock items
      let purchaseDetailsId = PurchaseDetails.insert(data)

      // Find product type
      let checkType = Products.findOne({ _id: item.productId })
      // call inventory insert function
      if (checkType.type == 'Product' || 'Ingredient') {
        insertInventories({
          refId: purchaseDetailsId,
          refType: 'Purchase',
          tranDate: tranDate,
          productId: item.productId,
          qty: item.qty,
          amount: item.cost,
          total: item.amount,
        })
      }
    })
    return 'success'
  } catch (error) {
    throwError(error)
  }
}

// Remove detail function
const removePurchaseDetails = ({ refId }) => {
  new SimpleSchema({
    refId: String,
  }).validator({
    refId,
  })
  try {
    PurchaseDetails.remove({ refId: refId })
  } catch (error) {
    throwError(error)
  }
}

const aggregatePrevDoc = selector => {
  let data = Purchases.aggregate([
    {
      $match: selector,
    },
    {
      $lookup: {
        from: 'rest_purchaseDetails',
        localField: '_id',
        foreignField: 'refId',
        as: 'prevDetailsDoc',
      },
    },
  ])
  return data[0]
}
