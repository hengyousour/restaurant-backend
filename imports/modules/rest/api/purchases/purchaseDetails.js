import { Mongo } from 'meteor/mongo'
import SimpleSchema from 'simpl-schema'
import { Optional } from 'ag-grid'

const PurchaseDetails = new Mongo.Collection('rest_purchaseDetails')

PurchaseDetails.schema = new SimpleSchema({
  productId: {
    type: String,
  },
  qty: {
    type: Number,
  },
  cost: {
    type: Number,
  },
  amount: {
    type: Number,
  },
  refId: {
    type: String,
  },
})

PurchaseDetails.attachSchema(PurchaseDetails.schema)

export default PurchaseDetails
