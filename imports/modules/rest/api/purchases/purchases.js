import { Mongo } from 'meteor/mongo'
import SimpleSchema from 'simpl-schema'

const Purchases = new Mongo.Collection('rest_purchases')

Purchases.schema = new SimpleSchema({
  date: {
    type: Date,
  },
  type: {
    type: String,
  },
  status: {
    type: String,
  },
  statusDate: {
    type: Object,
    optional: true,
  },
  'statusDate.open': {
    type: Date,
    optional: true,
  },
  'statusDate.partial': {
    type: Date,
    optional: true,
  },
  'statusDate.close': {
    type: Date,
    optional: true,
  },
  total: {
    type: Number,
  },
  totalPaid: {
    type: Number,
  },
  supplierId: {
    type: String,
  },
  employeeId: {
    type: String,
  },
})

Purchases.attachSchema(Purchases.schema)

export default Purchases
