import { Mongo } from 'meteor/mongo'
import Schema from './schema'

const Employees = new Mongo.Collection('rest_employees')
Employees.attachSchema(Schema)
Employees.timestamp()

export default Employees
