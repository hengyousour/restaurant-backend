import SimpleSchema from 'simpl-schema'

export default new SimpleSchema({
  name: {
    type: String,
  },
  telephone: {
    type: String,
  },
  address: {
    type: String,
  },
  role: {
    type: String,
  },
  userMaping: {
    type: String,
  },
})
