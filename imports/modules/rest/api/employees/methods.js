import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'

import { throwError } from '../../../../utils/security'
import rateLimit from '/imports/utils/rate-limit'
import getNextSeq from '/imports/utils/get-next-seq'

import EmployeeSchema from './schema'
import Employees from './collection'

// Find
export const findEmployees = new ValidatedMethod({
  name: 'rest.findEmployees',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
    options: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector, options }) {
    if (Meteor.isServer) {
      selector = selector || {}
      options = options || {}

      return Employees.find(selector, options).fetch()
    }
  },
})

// Find One
export const findOneEmployee = new ValidatedMethod({
  name: 'rest.findOneEmployee',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return Employees.findOne({ _id })
    }
  },
})

// Insert
export const insertEmployee = new ValidatedMethod({
  name: 'rest.insertEmployee',
  mixins: [CallPromiseMixin],
  validate: EmployeeSchema.validator(),
  run(doc) {
    if (Meteor.isServer) {
      const _id = getNextSeq({
        // Mandatory
        filter: {
          _id: 'rest_employees',
          // type: '001' // BranchId
        },
        // Optional
        opts: {
          seq: 1,
          // paddingType: 'start',
          // paddingLength: 6,
          // paddingChar: '0',
          // prefix: ''
        },
      })

      try {
        doc._id = _id.toString()
        return Employees.insert(doc)
      } catch (error) {
        // Decrement seq
        getNextSeq({
          filter: { _id: 'rest_employees' },
          opts: { seq: -1 },
        })
        throwError(error)
      }
    }
  },
})

// Update
export const updateEmployee = new ValidatedMethod({
  name: 'rest.updateEmployee',
  mixins: [CallPromiseMixin],
  validate: _.clone(EmployeeSchema)
    .extend({
      _id: String,
    })
    .validator(),
  run(doc) {
    if (Meteor.isServer) {
      return Employees.update({ _id: doc._id }, { $set: doc })
    }
  },
})

// Upsert Employee
export const upsertEmployee = new ValidatedMethod({
  name: 'rest.upsertEmployee',
  mixins: [CallPromiseMixin],
  validate: _.clone(EmployeeSchema)
    .extend({
      _id: {
        type: String,
        optional: true,
      },
    })
    .validator(),
  run(doc) {
    if (Meteor.isServer) {
      let _id
      if (!doc._id) {
        _id = getNextSeq({
          filter: {
            _id: 'rest_employees',
          },
          opts: { seq: 1 },
        })
        doc._id = _id.toString()
      }
      try {
        return Employees.upsert({ _id: doc._id }, { $set: doc })
      } catch (error) {
        if (_id) {
          _id = getNextSeq({
            filter: { _id: 'rest_employees' },
            seq: { seq: -1 },
          })
        }
        throwError(error)
      }
    }
  },
})

// Soft remove
export const softRemoveEmployee = new ValidatedMethod({
  name: 'rest.softRemoveEmployee',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return Employees.softRemove(_id)
    }
  },
})

// Restore
export const restoreEmployee = new ValidatedMethod({
  name: 'rest.restoreEmployee',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return Employees.restore(_id)
    }
  },
})

// Remove
export const removeEmployee = new ValidatedMethod({
  name: 'rest.removeEmployee',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return Employees.remove(_id)
    }
  },
})

rateLimit({
  methods: [
    findEmployees,
    findOneEmployee,
    insertEmployee,
    updateEmployee,
    softRemoveEmployee,
    restoreEmployee,
    removeEmployee,
  ],
})
