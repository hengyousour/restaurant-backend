import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'

import { throwError } from '../../../../utils/security'
import rateLimit from '/imports/utils/rate-limit'
import getNextSeq from '/imports/utils/get-next-seq'

import CategorySchema from './schema'
import Categories from './collection'
import Products from '../products/collection'

// Find
export const findCategories = new ValidatedMethod({
  name: 'rest.findCategories',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
    options: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector, options }) {
    if (Meteor.isServer) {
      selector = selector || {}
      options = options || {}

      return Categories.find(selector, options).fetch()
    }
  },
})

// Find
export const findCategoriesForSale = new ValidatedMethod({
  name: 'rest.findCategoriesForSale',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
    options: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector, options }) {
    if (Meteor.isServer) {
      selector = selector || {}
      options = options || {}
      let data = Categories.find(selector, options).fetch()

      _.remove(data, o => {
        let existProduct = Products.findOne({ categoryId: o._id })
        if (!existProduct) {
          return o._id == o._id
        }
      })

      return data
    }
  },
})

// Find One
export const findOneCategory = new ValidatedMethod({
  name: 'rest.findOneCategory',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return Categories.findOne({ _id })
    }
  },
})

// Upsert Category
export const upsertCategory = new ValidatedMethod({
  name: 'rest.upsertCategory',
  mixins: [CallPromiseMixin],
  validate: _.clone(CategorySchema)
    .extend({
      _id: {
        type: String,
        optional: true,
      },
    })
    .validator(),
  run(doc) {
    if (Meteor.isServer) {
      try {
        return Categories.upsert({ _id: doc._id }, { $set: doc })
      } catch (error) {
        throwError(error)
      }
    }
  },
})

// Soft remove
export const softRemoveCategory = new ValidatedMethod({
  name: 'rest.softRemoveCategory',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return Categories.softRemove(_id)
    }
  },
})

// Restore
export const restoreCategory = new ValidatedMethod({
  name: 'rest.restoreCategory',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return Categories.restore(_id)
    }
  },
})

// Remove
export const removeCategory = new ValidatedMethod({
  name: 'rest.removeCategory',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return Categories.remove(_id)
    }
  },
})

// usedCategory
export const usedCategory = new ValidatedMethod({
  name: 'rest.usedCategory',
  mixins: [CallPromiseMixin],
  validate: null,
  run(_id) {
    if (Meteor.isServer) {
      let used = false
      let productDoc = Products.findOne({ categoryId: _id })
      if (productDoc) {
        used = true
      } else {
        used = false
      }

      return { used }
    }
  },
})

rateLimit({
  methods: [
    findCategories,
    findOneCategory,
    softRemoveCategory,
    restoreCategory,
    removeCategory,
    usedCategory,
  ],
})
