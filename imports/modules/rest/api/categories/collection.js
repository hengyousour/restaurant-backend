import { Mongo } from 'meteor/mongo'
import Schema from './schema'

const Categories = new Mongo.Collection('rest_categories')
Categories.attachSchema(Schema)
Categories.timestamp()

export default Categories
