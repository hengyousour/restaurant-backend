import SimpleSchema from 'simpl-schema'

export default new SimpleSchema({
  name: {
    type: String,
  },
  productType: {
    type: String,
    allowedValues: ['Product', 'Dish', 'Service', 'Ingredient', 'ExtraFood'],
  },
  color: {
    type: String,
    optional: true,
  },
})
