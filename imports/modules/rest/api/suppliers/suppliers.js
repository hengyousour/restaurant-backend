import SimpleSchema from 'simpl-schema'
import { Mongo } from 'meteor/mongo'
const Suppliers = new Mongo.Collection('rest_suppliers')

Suppliers.schema = new SimpleSchema({
  name: {
    type: String,
  },
  telephone: {
    type: String,
  },
  address: {
    type: String,
    optional: true,
  },
})
Suppliers.attachSchema(Suppliers.schema)
export default Suppliers
