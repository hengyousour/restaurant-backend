import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'

import { throwError } from '../../../../utils/security'

import getNextSeq from '/imports/utils/get-next-seq'

import SupplierSchema from './schema'
import Supplier from './suppliers'
import purchases from '../purchases/purchases'

//find
export const findSuppliers = new ValidatedMethod({
  name: 'rest.findSuppliers',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
    options: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector, options }) {
    if (Meteor.isServer) {
      selector = selector || {}
      options = options || {}

      return Supplier.find(selector, options).fetch()
    }
  },
})
// FindOne supplier
export const findOneSupplier = new ValidatedMethod({
  name: 'rest.findOneSupplier',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return Supplier.findOne({ _id })
    }
  },
})

// Upsert Supplier
export const upsertSupplier = new ValidatedMethod({
  name: 'rest.upsertSupplier',
  mixins: [CallPromiseMixin],
  validate: _.clone(SupplierSchema)
    .extend({
      _id: {
        type: String,
        optional: true,
      },
    })
    .validator(),
  run(doc) {
    if (Meteor.isServer) {
      try {
        return Supplier.upsert({ _id: doc._id }, { $set: doc })
      } catch (error) {
        throwError(error)
      }
    }
  },
})

export const removeSupplier = new ValidatedMethod({
  name: 'rest.removeSupplier',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return Supplier.remove(_id)
    }
  },
})

// usedProduct
export const usedSupplier = new ValidatedMethod({
  name: 'rest.usedSupplier',
  mixins: [CallPromiseMixin],
  validate: null,
  run(_id) {
    if (Meteor.isServer) {
      let used = false

      let purchaseDoc = purchases.findOne({ supplierId: _id })

      if (purchaseDoc) {
        used = true
      } else {
        used = false
      }

      return { used }
    }
  },
})
