import { Mongo } from 'meteor/mongo'
import Schema from './schema'

const Discounts = new Mongo.Collection('rest_discounts')
Discounts.attachSchema(Schema)
Discounts.timestamp()

export default Discounts
