import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'
import moment from 'moment'

import { throwError } from '../../../../utils/security'
import rateLimit from '/imports/utils/rate-limit'
import getNextSeq from '/imports/utils/get-next-seq'

import Discounts from './collection'

export const findDiscounts = new ValidatedMethod({
  name: 'rest.findDiscounts',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
    options: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector, options }) {
    if (Meteor.isServer) {
      selector = selector || {}
      options = options || {}

      return Discounts.find(selector, options).fetch()
    }
  },
})

export const findDiscountAggregate = new ValidatedMethod({
  name: 'rest.findDiscountAggregate',
  mixins: [CallPromiseMixin],
  validate: null,
  run() {
    if (Meteor.isServer) {
      let data = Discounts.aggregate([
        {
          $lookup: {
            from: 'rest_products',
            localField: 'productId',
            foreignField: '_id',
            as: 'productDoc',
          },
        },
        {
          $unwind: { path: '$productDoc', preserveNullAndEmptyArrays: true },
        },
        {
          $addFields: {
            productName: '$productDoc.name',
          },
        },
        {
          $project: {
            productDoc: 0,
          },
        },
      ])
      return data
    }
  },
})

// Insert
export const insertDiscounts = new ValidatedMethod({
  name: 'rest.insertDiscounts',
  mixins: [CallPromiseMixin],
  validate: null,
  run(doc) {
    if (Meteor.isServer) {
      try {
        const startDate = moment(doc.eventPeriod[0]).toDate()
        const endDate = moment(doc.eventPeriod[1]).toDate()

        let insertDoc = {
          eventName: doc.eventName,
          startDate: startDate,
          endDate: endDate,
          discountValue: doc.discountValue,
          discountType: doc.discountType,
          type: doc.type,
        }

        if (doc.details) {
          insertDoc.products = doc.details
        }

        // Insert
        Discounts.insert(insertDoc)

        return 'success'
      } catch (error) {
        throwError(error)
      }
    }
  },
})

// // Update
// export const updateCategory = new ValidatedMethod({
//   name: 'rest.updateCategory',
//   mixins: [CallPromiseMixin],
//   validate: _
//     .clone(CategorySchema)
//     .extend({
//       _id: String,
//     })
//     .validator(),
//   run(doc) {
//     if (Meteor.isServer) {
//       return Categories.update({ _id: doc._id }, { $set: doc })
//     }
//   },
// })

// Remove
export const removeDiscount = new ValidatedMethod({
  name: 'rest.removeDiscount',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return Discounts.remove(_id)
    }
  },
})

// rateLimit({
//   methods: [
//     findCategories,
//     findOneCategory,
//     insertCategory,
//     updateCategory,
//     softRemoveCategory,
//     restoreCategory,
//     removeCategory,
//   ],
// })
