import SimpleSchema from 'simpl-schema'

export default new SimpleSchema({
  eventName: {
    type: String,
  },
  startDate: {
    type: Date,
  },
  endDate: {
    type: Date,
  },
  type: {
    type: String,
  },
  discountType: {
    type: String,
    optional: true,
  },
  discountValue: {
    type: Number,
    optional: true,
  },

  // Product details
  products: {
    type: Array,
    optional: true,
  },
  'products.$': {
    type: Object,
  },
  'products.$.productId': {
    type: String,
  },
  'products.$.price': {
    type: Number,
  },
  'products.$.discount': {
    type: Number,
  },
})
