import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'
import moment from 'moment'

import { throwError } from '../../../../utils/security'
import rateLimit from '/imports/utils/rate-limit'
import getNextSeq from '/imports/utils/get-next-seq'

// PurchasePayment
import PurchasePaymentSchema from './schema'
import PurchasePayments from './collection'
// Purchase
import Purchases from '../purchases/purchases'

// Find
export const findPurchasePayments = new ValidatedMethod({
  name: 'rest.findPurchasePayments',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
    options: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector, options }) {
    if (Meteor.isServer) {
      selector = selector || {}
      options = options || {}

      return PurchasePayments.find(selector, options).fetch()
    }
  },
})

// Find One
export const findOnePurchasePayment = new ValidatedMethod({
  name: 'rest.findOnePurchasePayment',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return PurchasePayments.findOne({ _id })
    }
  },
})

// Insert
export const insertPurchasePayment = new ValidatedMethod({
  name: 'rest.insertPurchasePayment',
  mixins: [CallPromiseMixin],
  validate: PurchasePaymentSchema.validator(),
  run(doc) {
    if (Meteor.isServer) {
      try {
        PurchasePayments.insert(doc, error => {
          if (!error) {
            let status
            if (doc.payAmount >= doc.openAmount) {
              status = 'Closed'
            } else {
              status = 'Partial'
            }
            Purchases.update(
              { _id: doc.purchaseId },
              {
                $set: { status: status },
                $inc: { totalPaid: doc.payAmount },
              }
            )
          }
        })

        return 'success'
      } catch (error) {
        // Decrement seq
        getNextSeq({
          filter: { _id: 'rest_purchasePayments' },
          opts: { seq: -1 },
        })
        throwError(error)
      }
    }
  },
})

// Inserts
export const insertPurchasePayments = new ValidatedMethod({
  name: 'rest.insertPurchasePayments',
  mixins: [CallPromiseMixin],
  validate: null,
  run({ doc, purchase }) {
    if (Meteor.isServer) {
      try {
        let returnVal = []
        _.forEach(purchase, o => {
          if (o.payAmount != 0) {
            // doc._id = _id
            doc.purchaseId = o._id
            doc.memo = o.memo
            doc.openAmount = o.openAmount
            doc.payAmount = o.payAmount

            if (o.memo != '') {
              doc.status = 'Waive'
            } else if (o.payAmount >= o.openAmount) {
              doc.status = 'Closed'
            } else if (o.payAmount == '0' || o.payAmount > 0) {
              doc.status = 'Partial'
            } // Insert

            PurchasePayments.insert(doc)

            // Update
            if (o.payAmount >= o.openAmount) {
              o.status = 'Closed'
              Purchases.update(
                { _id: o._id },
                {
                  $set: { status: o.status, 'statusDate.close': doc.date },
                  $inc: { totalPaid: o.payAmount },
                }
              )
            } else if (o.payAmount >= 0) {
              o.status = 'Partial'
              Purchases.update(
                { _id: o._id },
                {
                  $set: { status: o.status, 'statusDate.partial': doc.date },
                  $inc: { totalPaid: o.payAmount },
                }
              )
            }
            returnVal.push(o._id)
          }
        })

        return returnVal
      } catch (error) {
        // Decrement seq
        getNextSeq({
          filter: { _id: 'rest_purchasePayments' },
          opts: { seq: -1 },
        })

        throwError(error)
      }
    }
  },
})

// Soft remove
export const softRemovePurchasePayment = new ValidatedMethod({
  name: 'rest.softRemovePurchasePayment',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return PurchasePayments.softRemove(_id)
    }
  },
})

// Restore
export const restorePurchasePayment = new ValidatedMethod({
  name: 'rest.restorePurchasePayment',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return PurchasePayments.restore(_id)
    }
  },
})

// Remove
export const removePurchasePayment = new ValidatedMethod({
  name: 'rest.removePurchasePayment',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return PurchasePayments.remove(_id)
    }
  },
})

rateLimit({
  methods: [
    findPurchasePayments,
    findOnePurchasePayment,
    insertPurchasePayment,
    insertPurchasePayments,
    softRemovePurchasePayment,
    restorePurchasePayment,
    removePurchasePayment,
  ],
})
