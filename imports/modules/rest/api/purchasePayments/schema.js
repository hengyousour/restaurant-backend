import SimpleSchema from 'simpl-schema'

export default new SimpleSchema({
  date: {
    type: Date,
  },
  openAmount: {
    type: Number,
  },
  payAmount: {
    type: Number,
  },
  status: {
    type: String,
  },
  memo: {
    type: String,
    optional: true,
  },
  purchaseId: {
    type: String,
  },
  supplierId: {
    type: String,
  },
  employeeId: {
    type: String,
  },
})
