import { Mongo } from 'meteor/mongo'
import Schema from './schema'

const PurchasePayments = new Mongo.Collection('rest_purchasePayments')
PurchasePayments.attachSchema(Schema)
PurchasePayments.timestamp()

export default PurchasePayments
