import { Meteor } from 'meteor/meteor'
import excelToJson from 'convert-excel-to-json'

import { throwError } from '../../../../utils/security'

import _ from 'lodash'

// Products
import Products from '../products/collection'
import Categories from '../categories/collection'

// Product
export const importProducts = sourceFile => {
  try {
    const products = excelToJson({
      sourceFile,
      header: {
        rows: 1,
      },
      sheets: [
        {
          name: 'Product',
          columnToKey: {
            A: 'categoryId',
            B: 'name',
            C: 'status',
            D: 'cost',
            E: 'markupPer',
            F: 'markupVal',
            G: 'price',
            H: 'barcode',
            I: 'type',
          },
        },
      ],
    })

    products.Product.forEach(o => {
      let categoryDoc = Categories.findOne({ name: o.categoryId })

      if (!_.isEmpty(categoryDoc)) {
        // If category already exist
        o.categoryId = categoryDoc._id
      } else {
        // If category not yet exist
        let categoryInsertDoc = {
          name: o.categoryId,
          productType: 'Product',
        }

        // Wrap sync
        let categoryId = sync(categoryInsertDoc)
        o.categoryId = categoryId
      }

      // Insert product
      Products.insert(o)
    })

    return products.Product.length
  } catch (error) {
    throwError(error)
  }
}

// Insert category function
const importCategory = (categoryInsertDoc, callback) => {
  try {
    let id = Categories.insert(categoryInsertDoc)

    // return category Id
    callback(null, id)
  } catch (error) {
    throwError(error)
  }
}

const sync = Meteor.wrapAsync(importCategory)
