import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'

import { throwError } from '../../../../utils/security'

import FileData from '/imports/api/files/files'
import { importCategory } from './category'
import { importProducts } from './product'

export const findProductImages = new ValidatedMethod({
  name: 'rest.findProductImages',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
    options: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector, options }) {
    if (Meteor.isServer) {
      selector = selector || {}
      options = options || {}

      let images = FileData.find(selector, options).fetch()

      images.forEach(o => {
        o.url = FileData.findOne({ _id: o._id }).url()
      })

      return images
    }
  },
})

export const importData = new ValidatedMethod({
  name: 'rest.importData',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    type: String,
    fileName: String,
  }).validator(),

  run({ type, fileName }) {
    if (Meteor.isServer) {
      try {
        let result
        const sourceFile = `/data/file_uploads/files-${fileName}-Restuarent_Import.xlsx`
        result = importProducts(sourceFile)

        // Remove file update
        FileData.remove({ _id: fileName })

        return result
      } catch (error) {
        // Remove file update
        FileData.remove({ _id: fileName })
        throwError(error)
      }
    }
  },
})
