import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import excelToJson from 'convert-excel-to-json'

import { throwError } from '../../../../utils/security'

// Category
import Categories from '../categories/collection'

// Category
export const importCategory = sourceFile => {
  try {
    const categories = excelToJson({
      sourceFile,
      header: {
        rows: 1,
      },
      sheets: [
        {
          name: 'Category',
          columnToKey: {
            A: 'name',
            B: 'productType',
            C: 'color',
          },
        },
      ],
    })

    categories.Category.forEach(o => {
      Categories.insert(o)
    })

    return categories.Category.length
  } catch (error) {
    throwError(error)
  }
}
