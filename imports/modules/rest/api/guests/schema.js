import SimpleSchema from 'simpl-schema'

export default new SimpleSchema({
  name: {
    type: String,
  },
  telephone: {
    type: String,
    optional: true,
  },
  address: {
    type: String,
    optional: true,
  },
})
