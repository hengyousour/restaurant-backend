import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'

import { throwError } from '../../../../utils/security'
import getNextSeq from '/imports/utils/get-next-seq'
import Schema from './schema'
import Guests from './guests'
import Sales from '../sales/sales'

// Find
export const findGuests = new ValidatedMethod({
  name: 'rest.findGuests',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
    options: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector, options }) {
    if (Meteor.isServer) {
      selector = selector || {}
      options = options || {}

      return Guests.find(selector, options).fetch()
    }
  },
})

// Find One Guest
export const findOneGuest = new ValidatedMethod({
  name: 'rest.findOneGuest',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector }) {
    if (Meteor.isServer) {
      selector = selector || {}
      return Guests.findOne(selector)
    }
  },
})

// Insert Guest
export const insertGuest = new ValidatedMethod({
  name: 'rest.insertGuest',
  mixins: [CallPromiseMixin],
  validate: null,
  run({ doc }) {
    if (Meteor.isServer) {
      let _id
      _id = getNextSeq({
        filter: { _id: 'rest_Guest' },
        opts: {
          seq: 1,
          paddingType: 'start',
          paddingLength: 4,
          paddingChar: '0',
        },
      })
      doc._id = _id.toString()

      try {
        Guests.insert(doc)
        return doc._id
      } catch (error) {
        // Decrement seq
        getNextSeq({
          filter: { _id: 'rest_Guest' },
          opts: { seq: -1 },
        })
        throwError(error)
      }
    }
  },
})

//update Guest and insert
export const upsertGuest = new ValidatedMethod({
  name: 'rest.upsertGuest',
  mixins: [CallPromiseMixin],
  validate: _.clone(Schema)
    .extend({
      _id: {
        type: String,
        optional: true,
      },
    })
    .validator(),
  run(doc) {
    if (Meteor.isServer) {
      let _id
      if (!doc._id) {
        _id = getNextSeq({
          filter: {
            _id: 'rest_Guest',
          },
          opts: {
            seq: 1,
            paddingType: 'start',
            paddingLength: 4,
            paddingChar: '0',
          },
        })

        doc._id = _id.toString()
      }
      try {
        return Guests.upsert({ _id: doc._id }, { $set: doc })
      } catch (error) {
        if (_id) {
          _id = getNextSeq({
            filter: { _id: 'rest_Guest' },
            seq: { seq: -1 },
          })
        }
        throwError(error)
      }
    }
  },
})

export const removeGuest = new ValidatedMethod({
  name: 'rest.removeGuest',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return Guests.remove(_id)
    }
  },
})

// usedGroup
export const usedGuest = new ValidatedMethod({
  name: 'rest.usedGuest',
  mixins: [CallPromiseMixin],
  validate: null,
  run(_id) {
    if (Meteor.isServer) {
      let used = false
      let saleDoc = Sales.findOne({ guestId: _id })
      if (saleDoc) {
        used = true
      } else {
        used = false
      }

      return { used }
    }
  },
})
