import { Mongo } from 'meteor/mongo'
import Schema from './schema'

const Guests = new Mongo.Collection('rest_guests')
Guests.attachSchema(Schema)
Guests.timestamp()

export default Guests
