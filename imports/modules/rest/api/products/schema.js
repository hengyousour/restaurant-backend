import SimpleSchema from 'simpl-schema'
import { Optional } from 'ag-grid'

export default new SimpleSchema({
  _id: {
    type: String,
    optional: true,
  },
  name: {
    type: String,
  },
  categoryId: {
    type: String,
  },
  groupId: {
    type: String,
    optional: true,
  },
  stationId: {
    type: String,
    optional: true,
  },
  status: {
    type: String,
    allowedValues: ['Active', 'Inactive'],
  },
  unitId: {
    type: String,
    optional: true,
  },
  // Ingredient details
  ingredients: {
    type: Array,
    optional: true,
  },
  'ingredients.$': {
    type: Object,
  },
  'ingredients.$.ingredientId': {
    type: String,
  },
  'ingredients.$.qty': {
    type: Number,
  },
  'ingredients.$.cost': {
    type: Number,
  },
  'ingredients.$.markupVal': {
    type: Number,
  },
  'ingredients.$.originalMarkupVal': {
    type: Number,
  },
  'ingredients.$.amount': {
    type: Number,
  },
  // Product details
  products: {
    type: Array,
    optional: true,
  },
  'products.$': {
    type: Object,
  },
  'products.$.productId': {
    type: String,
  },
  'products.$.qty': {
    type: Number,
  },
  'products.$.cost': {
    type: Number,
  },
  'products.$.markupVal': {
    type: Number,
  },
  'products.$.originalMarkupVal': {
    type: Number,
  },
  'products.$.amount': {
    type: Number,
  },
  cost: {
    type: Number,
  },
  markupPer: {
    type: Number,
  },
  markupVal: {
    type: Number,
  },
  price: {
    type: Number,
  },
  cookingTime: {
    type: Number,
    optional: true,
  },
  catalogType: {
    type: String,
    optional: true,
  },
  photo: {
    type: String,
    optional: true,
  },
  color: {
    type: String,
    optional: true,
  },
  barcode: {
    type: String,
    optional: true,
  },
  type: {
    type: String,
  },
})
