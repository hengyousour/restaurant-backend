import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'
import moment from 'moment'

import { throwError } from '../../../../utils/security'
import rateLimit from '/imports/utils/rate-limit'
import getNextSeq from '/imports/utils/get-next-seq'

// Product
import ProductSchema from './schema'
import Products from './collection'
// Dish details
import Images from '/imports/api/files/files'
import SaleDetails from '../sales/saleDetails'
import PurchaseDetails from '../purchases/purchaseDetails'
import Discounts from '../discounts/collection'
import Exchanges from '/imports/api/exchanges/exchanges'
import Company from '/imports/api/company/company'

// Find
export const findProducts = new ValidatedMethod({
    name: 'rest.findProducts',
    mixins: [CallPromiseMixin],
    validate: new SimpleSchema({
        selector: {
            type: Object,
            blackbox: true,
            optional: true,
        },
        options: {
            type: Object,
            blackbox: true,
            optional: true,
        },
        fromSale: {
            type: Object,
            blackbox: true,
            optional: true,
        },
    }).validator(),
    run({ selector, options, fromSale }) {
        if (Meteor.isServer) {
            selector = selector || {}
            options = options || {}
            fromSale = fromSale || {}

            let data = Products.find(selector, options).fetch()

            if (fromSale.findPhoto == true) {
                data = _.sortBy(data, [
                    function(o) {
                        if (!_.isEmpty(o.photo)) {
                            let photoUrl = Images.findOne({ _id: o.photo }).url()
                            if (!_.isEmpty(photoUrl)) {
                                o.photoUrl = photoUrl
                            }
                        }
                        return o.name
                    },
                ])
            }
            return data
        }
    },
})

// Find One
export const findOneProduct = new ValidatedMethod({
    name: 'rest.findOneProduct',
    mixins: [CallPromiseMixin],
    validate: null,
    run({ _id }) {
        if (Meteor.isServer) {
            Meteor._sleepForMs(400)
            return Products.findOne({ _id: _id })
        }
    },
})

// findProductForSale
export const findProductForSale = new ValidatedMethod({
    name: 'rest.findProductForSale',
    mixins: [CallPromiseMixin],
    validate: new SimpleSchema({
        selector: {
            type: Object,
            blackbox: true,
            optional: true,
        },
        options: {
            type: Object,
            blackbox: true,
            optional: true,
        },
        fromSale: {
            type: Object,
            blackbox: true,
            optional: true,
        },
    }).validator(),
    run({ selector, options, fromSale }) {
        if (Meteor.isServer) {
            selector = selector || {}
            options = options || {}
            fromSale = fromSale || {}

            // Product
            let data = Products.find(selector, options).fetch()
                // Exchange
            let exchangeDoc = Exchanges.findOne()
                // Company
            let companyDoc = Company.findOne()

            const newDate = new Date()
            const checkDate = moment(newDate).toDate()
            let matchDate = {
                startDate: { $lte: checkDate },
                endDate: { $gte: checkDate },
                type: 'Item',
            }

            if (fromSale.findPhoto == true) {
                data = _.sortBy(data, [
                    function(item) {
                        // Assign exchange
                        item.exchangeDoc = exchangeDoc
                            // Assign baseCurrency
                        item.baseCurrency = companyDoc.accounting.baseCurrency

                        let matchItem = {
                            'products.productId': item._id,
                        }

                        let discount = Discounts.aggregate([{
                                $match: matchDate,
                            },
                            {
                                $unwind: {
                                    path: '$products',
                                    preserveNullAndEmptyArrays: true,
                                },
                            },
                            {
                                $match: matchItem,
                            },
                            {
                                $group: {
                                    _id: '$_id',
                                    eventName: { $last: '$eventName' },
                                    startDate: { $last: '$startDate' },
                                    endDate: { $last: '$endDate' },
                                    type: { $last: '$type' },
                                    products: { $last: '$products' },
                                },
                            },
                        ])
                        let discountRevers = _.reverse(discount)
                        let discountDoc = null
                        let checkTime = moment(newDate).format('HH:mm:ss')
                        _.forEach(discountRevers, o => {
                            let startDate = moment(o.startDate).format('HH:mm:ss')
                            let endDate = moment(o.endDate).format('HH:mm:ss')

                            if (
                                Date.parse('01 Jan 2019 ' + checkTime) >=
                                Date.parse('01 Jan 2019 ' + startDate) &&
                                Date.parse('01 Jan 2019 ' + checkTime) <=
                                Date.parse('01 Jan 2019 ' + endDate)
                            ) {
                                discountDoc = o
                            }
                        })

                        if (!_.isEmpty(discountDoc)) {
                            item.discounts = discountDoc.products
                        }

                        if (!_.isEmpty(item.photo) && Images.findOne({ _id: item.photo })) {
                            Images.findOne({ _id: item.photo })
                            let photoUrl = Images.findOne({ _id: item.photo }).url()
                            if (!_.isEmpty(photoUrl)) {
                                item.photoUrl = photoUrl
                            }
                        }
                        return item.name
                    },
                ])
            }
            //  Sort floors
            data.sort(function(a, b) {
                return a.name.localeCompare(b.name, undefined, {
                    numeric: true,
                    sensitivity: 'base',
                })
            })
            let dataCount = Products.find(selector).count()

            return { data, dataCount }
        }
    },
})

// findProductAggregate
export const findProductAggregate = new ValidatedMethod({
    name: 'rest.findProductAggregate',
    mixins: [CallPromiseMixin],
    validate: null,
    run(params) {
        if (Meteor.isServer) {
            let selector = {}
            if (!_.isEmpty(params.type)) {
                selector.type = { $in: params.type }
            }

            let data = Products.aggregate([{
                    $match: selector,
                },
                {
                    $lookup: {
                        from: 'rest_categories',
                        localField: 'categoryId',
                        foreignField: '_id',
                        as: 'categoryDoc',
                    },
                },
                {
                    $unwind: { path: '$categoryDoc', preserveNullAndEmptyArrays: true },
                },
                {
                    $lookup: {
                        from: 'rest_groups',
                        localField: 'groupId',
                        foreignField: '_id',
                        as: 'groupDoc',
                    },
                },
                {
                    $unwind: { path: '$groupDoc', preserveNullAndEmptyArrays: true },
                },
                {
                    $lookup: {
                        from: 'rest_units',
                        localField: 'unitId',
                        foreignField: '_id',
                        as: 'unitDoc',
                    },
                },
                {
                    $unwind: { path: '$unitDoc', preserveNullAndEmptyArrays: true },
                },
                {
                    $addFields: {
                        categoryName: '$categoryDoc.name',
                        groupName: '$groupDoc.name',
                        unitName: '$unitDoc.name',
                    },
                },
                {
                    $project: {
                        categoryDoc: 0,
                        groupDoc: 0,
                        unitDoc: 0,
                    },
                },
            ])

            return data
        }
    },
})

// Insert
export const insertProduct = new ValidatedMethod({
    name: 'rest.insertProduct',
    mixins: [CallPromiseMixin],
    validate: new SimpleSchema({ doc: ProductSchema }).validator(),
    run({ doc }) {
        if (Meteor.isServer) {
            const _id = getNextSeq({
                // Mandatory
                filter: {
                    _id: 'rest_products',
                },
                // Optional
                opts: {
                    seq: 1,
                },
            })

            try {
                doc._id = _id.toString()

                if (doc.type == 'Dish') {
                    _.remove(doc.ingredients, o => {
                        return o.ingredientId == ''
                    })
                } else if (doc.type == 'Catalog') {
                    _.remove(doc.products, o => {
                        return o.productId == ''
                    })
                }

                return Products.insert(doc)
            } catch (error) {
                // Decrement seq
                getNextSeq({
                    filter: { _id: 'rest_products' },
                    opts: { seq: -1 },
                })
                throwError(error)
            }
        }
    },
})

// Update
export const updateProduct = new ValidatedMethod({
    name: 'rest.updateProduct',
    mixins: [CallPromiseMixin],
    validate: new SimpleSchema({ doc: ProductSchema }).validator(),
    run({ doc }) {
        if (Meteor.isServer) {
            return Products.update({ _id: doc._id }, { $set: doc })
        }
    },
})

// Update Status
export const updateProductStatus = new ValidatedMethod({
    name: 'rest.updateProductStatus',
    mixins: [CallPromiseMixin],
    validate: null,
    run({ doc }) {
        if (Meteor.isServer) {
            return Products.update({ _id: doc._id }, { $set: { status: doc.status } })
        }
    },
})

// Remove
export const removeProduct = new ValidatedMethod({
    name: 'rest.removeProduct',
    mixins: [CallPromiseMixin],
    validate: new SimpleSchema({
        _id: String,
        photo: {
            type: String,
            optional: true,
        },
    }).validator(),
    run(doc) {
        if (Meteor.isServer) {
            Products.remove({ _id: doc._id }, error => {
                if (!error) {
                    if (doc.photo) {
                        Images.remove({ _id: doc.photo })
                    }
                }
            })
            return 'success'
        }
    },
})

// usedProduct
export const usedProduct = new ValidatedMethod({
    name: 'rest.usedProduct',
    mixins: [CallPromiseMixin],
    validate: null,
    run(param) {
        if (Meteor.isServer) {
            let used = false

            // Type Ingredient
            if (param.type == 'Ingredient') {
                let productDoc = Products.findOne({
                    'ingredients.ingredientId': param._id,
                })
                let purchaseDetails = PurchaseDetails.findOne({ productId: param._id })
                if (productDoc || purchaseDetails) {
                    used = true
                } else {
                    used = false
                }
            } else {
                // Type Product
                let saleDetail = SaleDetails.findOne({ itemId: param._id })
                let purchaseDetail = PurchaseDetails.findOne({ productId: param._id })

                if (saleDetail || purchaseDetail) {
                    used = true
                } else {
                    used = false
                }
            }

            return { used }
        }
    },
})

rateLimit({
    methods: [
        findProducts,
        findOneProduct,
        insertProduct,
        updateProduct,
        removeProduct,
        usedProduct,
    ],
})