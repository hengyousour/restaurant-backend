import { Mongo } from 'meteor/mongo'
import Schema from './schema'

const Products = new Mongo.Collection('rest_products')
Products.attachSchema(Schema)
Products.timestamp()

export default Products
