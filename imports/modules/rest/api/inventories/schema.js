import SimpleSchema from 'simpl-schema'

export default new SimpleSchema({
  productId: {
    type: String,
  },
  tranDate: {
    type: Date,
  },
  qty: {
    type: Number,
  },
  amount: {
    type: Number,
  },
  total: {
    type: Number,
  },
  refType: {
    type: String,
    allowedValues: ['Purchase', 'Sale'],
  },
  refId: {
    type: String,
  },
  balance: {
    type: Number,
    optional: true,
  },
})
