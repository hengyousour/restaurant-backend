import { check } from 'meteor/check'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'
import numeral from 'numeral'
import { throwError } from '../../../../utils/security'

import Inventories from './collection'
// Defer update after insert
export const deferUpdateInventoryAfterInsert = ({
  productId,
  tranDate,
  balanceQty,
  balanceTotal,
}) => {
  new SimpleSchema({
    productId: String,
    tranDate: Date,
    balanceQty: Number,
    balanceTotal: Number,
  }).validate({
    productId,
    tranDate,
    balanceQty,
    balanceTotal,
  })
  let items = Inventories.find(
    {
      productId: productId,
      tranDate: {
        $gt: tranDate,
      },
    },
    {
      sort: {
        tranDate: 1,
        _id: 1,
      },
    }
  ).fetch()
  try {
    let tmpLastBalQty = balanceQty
    let tmpLastBalTotal = balanceTotal

    _.forEach(items, item => {
      // Get last stock item + items loop

      let balTotal = item.total + tmpLastBalTotal

      // update date bigger than
      Inventories.update(
        item._id,
        {
          $set: {
            total: balTotal,
          },
          $inc: {
            qty: tmpLastBalQty,
          },
        },
        error => {}
      )
    })

    return 'success'
  } catch (error) {
    throwError(error)
  }
}

// Defer Update after remove
export const deferUpdateInventoryAfterRemove = ({
  tranDate,
  productId,
  qty,
}) => {
  // Get stock item
  let items = Inventories.find(
    {
      productId: productId,
      tranDate: {
        $gte: tranDate,
      },
    },
    {
      sort: {
        tranDate: 1,
        _id: 1,
      },
    }
  ).fetch()
  try {
    let balQty = 0,
      balTotal = 0

    _.forEach(items, item => {
      balQty = item.qty - qty
      balTotal = balQty * item.amount
      // Update stock item
      Inventories.update(item._id, {
        $set: {
          total: balTotal,
        },
        $inc: {
          qty: -qty,
        },
      })
    })

    return 'success'
  } catch (error) {
    throwError(error)
  }
}

// Get last stock item
export const getLastInventory = ({ tranDate, productId, type }) => {
  let selector = {
    productId: productId,
  }

  if (type === 'insert') {
    selector.tranDate = {
      $lte: tranDate,
    }
  } else {
    // remove
    selector.tranDate = {
      $lt: tranDate,
    }
  }
  let options = {
    sort: {
      tranDate: -1,
      _id: -1,
    },
  }

  return Inventories.findOne(selector, options)
}
