import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'
import moment from 'moment'

import { throwError } from '../../../../utils/security'
import rateLimit from '/imports/utils/rate-limit'
import getNextSeq from '/imports/utils/get-next-seq'

import Inventories from './collection'

import {
  deferUpdateInventoryAfterInsert,
  deferUpdateInventoryAfterRemove,
  getLastInventory,
} from '../inventories/auto-update-inventory'

// Insert
export const insertInventories = ({
  refType,
  refId,
  tranDate,
  productId,
  amount,
  qty,
}) => {
  new SimpleSchema({
    refType: String,
    refId: String,
    tranDate: Date,
    productId: String,
    amount: {
      type: Number,
      optional: true,
    },
    qty: Number,
  }).validate({
    refType,
    refId,
    tranDate,
    productId,
    amount,
    qty,
  })

  try {
    //find last balance
    let lastStock = getLastInventory({
      tranDate: tranDate,
      productId: productId,
      type: 'insert',
    })
    let balanceQty = 0
    let total = 0

    if (lastStock) {
      balanceQty = lastStock.qty
      total = lastStock.total
    }
    balanceQty += qty
    total += qty * amount

    let doc = {
      refType: refType,
      refId: refId,
      tranDate: tranDate,
      productId: productId,
      amount: amount,
      qty: balanceQty,
      total: total,
    }

    Inventories.insert(doc)

    deferUpdateInventoryAfterInsert({
      tranDate: tranDate,
      productId: productId,
      balanceQty: qty,
      balanceTotal: qty * amount,
    })

    return 'Success'
  } catch (error) {
    // Decrement seq
    throwError(error)
  }
}

// remove inventories
export const removeInventories = ({ prevDoc }) => {
  new SimpleSchema({
    prevDoc: {
      type: Object,
      blackbox: true,
    },
  }).validate({
    prevDoc,
  })
  // Get items and then remove one by one
  _.forEach(prevDoc.prevDetailsDoc, obj => {
    try {
      Inventories.remove({ refId: obj._id })
      // if (obj.itemType === 'Inventory') {
      deferUpdateInventoryAfterRemove({
        tranDate: prevDoc.date,
        productId: obj.productId,
        qty: obj.qty,
      })
    } catch (error) {
      throwError(error)
    }
  })
}
