import { Mongo } from 'meteor/mongo'
import Schema from './schema'

const Inventories = new Mongo.Collection('rest_inventories')
Inventories.attachSchema(Schema)
Inventories.timestamp()

export default Inventories
