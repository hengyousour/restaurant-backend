import { Mongo } from 'meteor/mongo'
import Schema from './schema'

const Units = new Mongo.Collection('rest_units')
Units.attachSchema(Schema)
Units.timestamp()

export default Units
