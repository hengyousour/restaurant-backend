import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'
import { throwError } from '../../../../utils/security'
import rateLimit from '/imports/utils/rate-limit'
import getNextSeq from '/imports/utils/get-next-seq'

import UnitSchema from './schema'
import Units from './collection'
import Products from '../products/collection'

// Find
export const findUnits = new ValidatedMethod({
  name: 'rest.findUnits',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
    options: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector, options }) {
    if (Meteor.isServer) {
      selector = selector || {}
      options = options || {}

      return Units.find(selector, options).fetch()
    }
  },
})

// Find One
export const findOneUnit = new ValidatedMethod({
  name: 'rest.findOneUnit',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return Units.findOne({ _id })
    }
  },
})

// Upsert Unit
export const upsertUnit = new ValidatedMethod({
  name: 'rest.upsertUnit',
  mixins: [CallPromiseMixin],
  validate: _.clone(UnitSchema)
    .extend({
      _id: {
        type: String,
        optional: true,
      },
    })
    .validator(),
  run(doc) {
    if (Meteor.isServer) {
      try {
        return Units.upsert({ _id: doc._id }, { $set: doc })
      } catch (error) {
        throwError(error)
      }
    }
  },
})

// Soft remove
export const softRemoveUnit = new ValidatedMethod({
  name: 'rest.softRemoveUnit',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return Units.softRemove(_id)
    }
  },
})

// Restore
export const restoreUnit = new ValidatedMethod({
  name: 'rest.restoreUnit',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return Units.restore(_id)
    }
  },
})

// Remove
export const removeUnit = new ValidatedMethod({
  name: 'rest.removeUnit',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return Units.remove(_id)
    }
  },
})

// usedUnit
export const usedUnit = new ValidatedMethod({
  name: 'rest.usedUnit',
  mixins: [CallPromiseMixin],
  validate: null,
  run(_id) {
    if (Meteor.isServer) {
      let used = false
      let productDoc = Products.findOne({ unitId: _id })
      if (productDoc) {
        used = true
      } else {
        used = false
      }

      return { used }
    }
  },
})
