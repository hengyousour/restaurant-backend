import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'
import moment from 'moment'

import rateLimit from '/imports/utils/rate-limit'

import Purchases from '../purchases/purchases'

import PurchasePayments from '../purchasePayments/collection'

// Purchase Method
export const purchaseReport = new ValidatedMethod({
  name: 'rest.purchaseReport',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    reportPeriod: Array,
    'reportPeriod.$': Date,
    supplierId: {
      type: Array,
      optional: true,
    },
    'supplierId.$': {
      type: String,
    },
    employeeId: {
      type: Array,
      optional: true,
    },
    'employeeId.$': {
      type: String,
    },
  }).validator(),
  run(params) {
    if (Meteor.isServer) {
      Meteor._sleepForMs(500)

      const dateF = moment(params.reportPeriod[0])
        .startOf('day')
        .toDate()

      const dateT = moment(params.reportPeriod[1])
        .endOf('day')
        .toDate()

      // Pick query
      let selector = {
        date: {
          $gte: dateF,
          $lte: dateT,
        },
      }
      if (!_.isEmpty(params.supplierId)) {
        selector.supplierId = { $in: params.supplierId }
      }
      if (!_.isEmpty(params.employeeId)) {
        selector.employeeId = { $in: params.employeeId }
      }

      // Aggregate with sales
      let data = Purchases.aggregate([
        {
          $match: selector,
        },
        {
          $lookup: {
            from: 'rest_purchaseDetails',
            localField: '_id',
            foreignField: 'refId',
            as: 'detailDoc',
          },
        },
        {
          $unwind: { path: '$detailDoc', preserveNullAndEmptyArrays: true },
        },
        {
          $lookup: {
            from: 'rest_products',
            localField: 'detailDoc.productId',
            foreignField: '_id',
            as: 'productDoc',
          },
        },
        {
          $unwind: { path: '$productDoc', preserveNullAndEmptyArrays: true },
        },
        {
          $lookup: {
            from: 'rest_suppliers',
            localField: 'supplierId',
            foreignField: '_id',
            as: 'supplierDoc',
          },
        },
        {
          $unwind: { path: '$supplierDoc', preserveNullAndEmptyArrays: true },
        },
        {
          $lookup: {
            from: 'users',
            localField: 'employeeId',
            foreignField: '_id',
            as: 'employeeDoc',
          },
        },
        {
          $unwind: { path: '$employeeDoc', preserveNullAndEmptyArrays: true },
        },
        {
          $group: {
            _id: '$_id',
            date: { $last: '$date' },
            type: { $last: '$type' },
            statusDate: { $last: '$statusDate' },
            total: { $last: '$total' },
            totalPaid: { $last: '$totalPaid' },
            supplierId: { $last: '$supplierId' },
            supplierName: { $last: '$supplierDoc.name' },
            employeeId: { $last: '$employeeId' },
            employeeName: { $last: '$employeeDoc.profile.fullName' },
            status: { $last: '$status' },
            detailDoc: {
              $addToSet: {
                _id: '$detailDoc._id',
                productId: '$detailDoc.productId',
                date: '$date',
                supplierName: '$supplierDoc.name',
                employeeName: '$employeeDoc.profile.fullName',
                qty: '$detailDoc.qty',
                cost: '$detailDoc.cost',
                amount: '$detailDoc.amount',
                refId: '$detailDoc.refId',
                name: '$productDoc.name',
              },
            },
          },
        },
      ])

      // Merge data
      let mergeData = []
      _.forEach(data, o => {
        mergeData = mergeData.concat(o.detailDoc)
      })

      return _.sortBy(mergeData, ['date', 'name'])
    }
  },
})

// Purchase Payment methods
export const purchasePaymentReport = new ValidatedMethod({
  name: 'rest.purchasePaymentReport',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    reportPeriod: Array,
    'reportPeriod.$': Date,
    supplierId: {
      type: Array,
      optional: true,
    },
    'supplierId.$': {
      type: String,
    },
    status: {
      type: Array,
      optional: true,
    },
    'status.$': {
      type: String,
    },
    employeeId: {
      type: Array,
      optional: true,
    },
    'employeeId.$': {
      type: String,
    },
  }).validator(),
  run(params) {
    if (Meteor.isServer) {
      Meteor._sleepForMs(500)

      const dateF = moment(params.reportPeriod[0])
        .startOf('day')
        .toDate()

      const dateT = moment(params.reportPeriod[1])
        .endOf('day')
        .toDate()

      // Pick query
      let selector = {
        date: {
          $gte: dateF,
          $lte: dateT,
        },
      }
      if (!_.isEmpty(params.supplierId)) {
        selector.supplierId = { $in: params.supplierId }
      }
      if (!_.isEmpty(params.status)) {
        selector.status = { $in: params.status }
      }
      if (!_.isEmpty(params.employeeId)) {
        selector.employeeId = { $in: params.employeeId }
      }

      // Aggregate with sales
      let data = PurchasePayments.aggregate([
        { $match: selector },
        {
          $lookup: {
            from: 'rest_suppliers',
            localField: 'supplierId',
            foreignField: '_id',
            as: 'supplierDoc',
          },
        },
        { $unwind: { path: '$supplierDoc', preserveNullAndEmptyArrays: true } },
        {
          $lookup: {
            from: 'users',
            localField: 'employeeId',
            foreignField: '_id',
            as: 'employeeDoc',
          },
        },
        { $unwind: { path: '$employeeDoc', preserveNullAndEmptyArrays: true } },

        {
          $addFields: {
            supplierName: '$supplierDoc.name',
            employeeName: '$employeeDoc.profile.fullName',
          },
        },
        { $project: { supplierDoc: 0, employeeDoc: 0 } },
        {
          $sort: {
            date: -1,
          },
        },
      ])

      return data
    }
  },
})

rateLimit({
  methods: [],
})
