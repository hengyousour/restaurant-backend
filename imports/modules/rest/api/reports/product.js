import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'
import moment from 'moment'
import Products from '../products/collection'

import Inventories from '../inventories/collection'

import rateLimit from '/imports/utils/rate-limit'

export const productReport = new ValidatedMethod({
  name: 'rest.productReport',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    categoryId: {
      type: Array,
      optional: true,
    },
    'categoryId.$': {
      type: String,
    },
    status: {
      type: Array,
      optional: true,
    },
    'status.$': {
      type: String,
    },
  }).validator(),
  run(params) {
    if (Meteor.isServer) {
      Meteor._sleepForMs(500)

      let selector = {
        type: { $in: ['Product', 'Dish'] },
      }

      if (!_.isEmpty(params.categoryId)) {
        selector.categoryId = { $in: params.categoryId }
      }
      if (!_.isEmpty(params.status)) {
        selector.status = { $in: params.status }
      }

      let data = Products.aggregate([
        {
          $match: selector,
        },
        {
          $lookup: {
            from: 'rest_categories',
            localField: 'categoryId',
            foreignField: '_id',
            as: 'categoryDoc',
          },
        },
        {
          $unwind: {
            path: '$categoryDoc',
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $lookup: {
            from: 'rest_stations',
            localField: 'stationId',
            foreignField: '_id',
            as: 'stationDoc',
          },
        },
        {
          $unwind: {
            path: '$stationDoc',
            preserveNullAndEmptyArrays: true,
          },
        },

        {
          $addFields: {
            categoryName: '$categoryDoc.name',
            stationName: '$stationDoc.name',
          },
        },
        { $project: { categoryDoc: 0, stationDoc: 0 } },
        {
          $sort: {
            name: 1,
          },
        },
      ])
      return data
    }
  },
})

export const stockReport = new ValidatedMethod({
  name: 'rest.stockReport',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    date: {
      type: Date,
    },
    categoryId: {
      type: Array,
      optional: true,
    },
    'categoryId.$': {
      type: String,
    },
    status: {
      type: Array,
      optional: true,
    },
    'status.$': {
      type: String,
    },
  }).validator(),
  run(params) {
    if (Meteor.isServer) {
      Meteor._sleepForMs(500)

      const date = moment(params.date)
        .endOf('day')
        .toDate()

      // Pick query
      let dateSelector = {
        tranDate: {
          $lte: date,
        },
      }
      let categorySelector = {}
      if (!_.isEmpty(params.categoryId)) {
        categorySelector = {
          'productDoc.categoryId': { $in: params.categoryId },
        }
      }
      let statusSelector = {}
      if (!_.isEmpty(params.status)) {
        statusSelector = {
          'productDoc.status': { $in: params.status },
        }
      }

      let data = Inventories.aggregate([
        { $match: dateSelector },
        {
          $lookup: {
            from: 'rest_products',
            localField: 'productId',
            foreignField: '_id',
            as: 'productDoc',
          },
        },
        {
          $unwind: { path: '$productDoc', preserveNullAndEmptyArrays: true },
        },
        { $match: categorySelector },
        { $match: statusSelector },
        {
          $lookup: {
            from: 'rest_categories',
            localField: 'productDoc.categoryId',
            foreignField: '_id',
            as: 'categoryDoc',
          },
        },
        {
          $unwind: { path: '$categoryDoc', preserveNullAndEmptyArrays: true },
        },
        {
          $lookup: {
            from: 'rest_units',
            localField: 'productDoc.unitId',
            foreignField: '_id',
            as: 'unitDoc',
          },
        },
        {
          $unwind: { path: '$unitDoc', preserveNullAndEmptyArrays: true },
        },
        {
          $group: {
            _id: '$productId',
            productId: { $last: '$_id' },
            productName: { $last: '$productDoc.name' },
            status: { $last: '$productDoc.status' },
            price: { $last: '$productDoc.cost' },
            qty: { $last: '$qty' },
            category: { $last: '$categoryDoc.name' },
            unit: { $last: '$unitDoc.name' },
            refId: { $last: '$refId' },
            refType: { $last: '$refType' },
            total: { $last: '$total' },
            tranDate: { $last: '$tranDate' },
          },
        },
        // {
        //   $project: {
        //     _id: '$productId',
        //     productId: '$_id',
        //     productName: '$productName',
        //     price: '$price',
        //     qty: '$qty',
        //     refId: '$refId',
        //     refType: '$refType',
        //     category: '$category',
        //     unit: '$unit',
        //     total: '$total',
        //     tranDate: '$tranDate',
        //     status: '$status',
        //   },
        // },
        {
          $sort: {
            category: 1,
            productName: 1,
          },
        },
      ])
      return data
    }
  },
})

rateLimit({
  methods: [productReport],
})
