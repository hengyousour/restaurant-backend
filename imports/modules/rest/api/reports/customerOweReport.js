import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'
import moment from 'moment'

import rateLimit from '/imports/utils/rate-limit'

import Sales from '../sales/sales'

// Find owe Customer
export const findOweCustomer = new ValidatedMethod({
  name: 'rest.findOweCustomer',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    status: {
      type: Array,
      optional: true,
    },
    'status.$': {
      type: String,
    },
  }).validator(),
  run(params) {
    if (Meteor.isServer) {
      Meteor._sleepForMs(500)

      // Pick query
      let selector = {
        status: { $in: params.status },
      }
      // Aggregate with sales
      let data = Sales.aggregate([
        {
          $match: selector,
        },
        {
          $lookup: {
            from: 'rest_guests',
            localField: 'guestId',
            foreignField: '_id',
            as: 'guestDoc',
          },
        },
        { $unwind: { path: '$guestDoc', preserveNullAndEmptyArrays: true } },
        {
          $group: {
            _id: '$guestDoc._id',
            name: { $last: '$guestDoc.name' },
          },
        },
      ])

      return data
    }
  },
})

// Customer owe
export const customerOweReport = new ValidatedMethod({
  name: 'rest.customerOweReport',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    date: {
      type: Date,
    },
    guestId: {
      type: Array,
      optional: true,
    },
    'guestId.$': {
      type: String,
    },
  }).validator(),
  run(params) {
    if (Meteor.isServer) {
      Meteor._sleepForMs(500)
      const date = moment(params.date)
        .endOf('day')
        .toDate()

      // Pick query
      let selector = {
        date: {
          $lte: date,
        },
        status: 'Partial',
      }
      if (!_.isEmpty(params.guestId)) {
        selector.guestId = { $in: params.guestId }
      }

      let data = Sales.aggregate([
        {
          $match: selector,
        },
        {
          $lookup: {
            from: 'rest_guests',
            localField: 'guestId',
            foreignField: '_id',
            as: 'guestDoc',
          },
        },
        {
          $unwind: { path: '$guestDoc', preserveNullAndEmptyArrays: true },
        },
        {
          $group: {
            _id: '$_id',
            openAmount: { $last: '$total' },
            receiveAmount: { $last: '$totalReceived' },
            saleDate: { $last: '$date' },
            guestName: { $last: '$guestDoc.name' },
            guestTel: { $last: '$guestDoc.telephone' },
          },
        },
        {
          $project: {
            invoiceId: '$_id',
            openAmount: '$openAmount',
            receiveAmount: '$receiveAmount',
            saleDate: '$saleDate',
            guestName: '$guestName',
            guestTel: '$guestTel',
          },
        },
        {
          $sort: {
            invoiceId: 1,
          },
        },
      ])

      return data
    }
  },
})

rateLimit({
  methods: [],
})
