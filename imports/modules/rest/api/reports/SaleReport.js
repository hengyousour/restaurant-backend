import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'
import moment from 'moment'

import rateLimit from '/imports/utils/rate-limit'

import Sales from '../sales/sales'

import SaleReceipts from '../saleReceipts/collection'
import Employees from '../employees/collection'

// Sale method
export const saleReport = new ValidatedMethod({
  name: 'rest.saleReport',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    reportPeriod: Array,
    'reportPeriod.$': Date,
    guestId: {
      type: Array,
      optional: true,
    },
    'guestId.$': {
      type: String,
    },
    status: {
      type: Array,
      optional: true,
    },
    'status.$': {
      type: String,
    },
    employeeId: {
      type: Array,
      optional: true,
    },
    'employeeId.$': {
      type: String,
    },
    paperSize: {
      type: String,
      optional: true,
    },
  }).validator(),
  run(params) {
    if (Meteor.isServer) {
      Meteor._sleepForMs(100)

      const dateF = moment(params.reportPeriod[0]).toDate()

      const dateT = moment(params.reportPeriod[1]).toDate()

      // Pick query
      let selector = {
        date: {
          $gte: dateF,
          $lte: dateT,
        },
      }

      if (!_.isEmpty(params.guestId)) {
        selector.guestId = { $in: params.guestId }
      }
      if (!_.isEmpty(params.status)) {
        selector.status = { $in: params.status }
      }
      if (!_.isEmpty(params.employeeId)) {
        selector.employeeId = { $in: params.employeeId }
      }

      // Aggregate with sales
      let data = Sales.aggregate([
        {
          $match: selector,
        },
        {
          $lookup: {
            from: 'rest_saleDetails',
            localField: '_id',
            foreignField: 'invoiceId',
            as: 'detailDoc',
          },
        },
        {
          $unwind: { path: '$detailDoc', preserveNullAndEmptyArrays: true },
        },
        {
          $lookup: {
            from: 'rest_products',
            localField: 'detailDoc.itemId',
            foreignField: '_id',
            as: 'productDoc',
          },
        },
        {
          $unwind: { path: '$productDoc', preserveNullAndEmptyArrays: true },
        },
        {
          $lookup: {
            from: 'rest_guests',
            localField: 'guestId',
            foreignField: '_id',
            as: 'guestDoc',
          },
        },
        {
          $unwind: { path: '$guestDoc', preserveNullAndEmptyArrays: true },
        },
        {
          $lookup: {
            from: 'users',
            localField: 'employeeId',
            foreignField: '_id',
            as: 'employeeDoc',
          },
        },
        {
          $unwind: { path: '$employeeDoc', preserveNullAndEmptyArrays: true },
        },
        {
          $lookup: {
            from: 'rest_tables',
            localField: 'tableId',
            foreignField: '_id',
            as: 'tableDoc',
          },
        },
        {
          $unwind: { path: '$tableDoc', preserveNullAndEmptyArrays: true },
        },

        {
          $group: {
            _id: '$_id',
            date: { $last: '$date' },
            outDate: { $last: '$statusDate.close' },
            type: { $last: '$type' },
            status: { $last: '$status' },
            statusDate: { $last: '$statusDate' },
            discountRate: { $last: '$discountRate' },
            discountValue: { $last: '$discountValue' },
            subTotal: { $last: { $add: ['$total', '$discountValue'] } },
            total: { $last: '$total' },
            totalReceived: { $last: '$totalReceived' },
            tableId: { $last: '$tableId' },
            tableName: { $last: '$tableDoc.name' },
            employeeId: { $last: 'employeeId' },
            employeeName: { $last: '$employeeDoc.profile.fullName' },
            guestId: { $last: '$guestId' },
            guestName: { $last: '$guestDoc.name' },
            billed: { $last: '$billed' },
            refId: { $last: '$refId' },
            detailDoc: {
              $addToSet: {
                _id: '$detailDoc._id',
                date: '$date',
                itemType: '$productDoc.type',
                itemId: '$detailDoc.itemId',
                name: '$productDoc.name',
                qty: '$detailDoc.qty',
                price: '$detailDoc.price',
                discount: '$detailDoc.discount',
                amount: '$detailDoc.amount',
                invoiceId: '$detailDoc.invoiceId',
              },
            },
          },
        },
        {
          $sort: {
            _id: -1,
          },
        },
      ])

      return data
    }
  },
})

// Sale Detail By Item method
export const saleDetailByItem = new ValidatedMethod({
  name: 'rest.saleDetailByItem',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    reportPeriod: Array,
    'reportPeriod.$': Date,
    employeeId: {
      type: Array,
      optional: true,
    },
    'employeeId.$': {
      type: String,
    },
    categoryId: {
      type: Array,
      optional: true,
    },
    'categoryId.$': {
      type: String,
    },
    paperSize: {
      type: String,
      optional: true,
    },
  }).validator(),
  async run(params) {
    if (Meteor.isServer) {
      Meteor._sleepForMs(500)

      const dateF = moment(params.reportPeriod[0]).toDate()

      const dateT = moment(params.reportPeriod[1]).toDate()

      // Pick query
      let selector = {
        date: {
          $gte: dateF,
          $lte: dateT,
        },
        status: {
          $ne: 'Cancel',
        },
      }
      if (!_.isEmpty(params.employeeId)) {
        selector.employeeId = { $in: params.employeeId }
      }

      let categorySelector = {}
      if (!_.isEmpty(params.categoryId)) {
        categorySelector = {
          'productDoc.categoryId': { $in: params.categoryId },
        }
      }

      let itemsDoc

      if (params.paperSize == 'a4p') {
        // Aggregate with sales
        let data = await Sales.rawCollection()
          .aggregate([
            {
              $match: selector,
            },
            {
              $lookup: {
                from: 'rest_saleDetails',
                localField: '_id',
                foreignField: 'invoiceId',
                as: 'saleDetailDoc',
              },
            },
            {
              $unwind: {
                path: '$saleDetailDoc',
                preserveNullAndEmptyArrays: true,
              },
            },
            {
              $lookup: {
                from: 'rest_products',
                localField: 'saleDetailDoc.itemId',
                foreignField: '_id',
                as: 'productDoc',
              },
            },
            {
              $unwind: {
                path: '$productDoc',
                preserveNullAndEmptyArrays: true,
              },
            },
            {
              $match: categorySelector,
            },
            {
              $group: {
                _id: '$saleDetailDoc.itemId',
                date: { $last: '$date' },
                type: { $last: '$type' },
                status: { $last: '$status' },
                statusDate: { $last: '$statusDate' },
                discountRate: { $last: '$discountRate' },
                discountValue: { $last: '$discountValue' },
                total: { $last: '$total' },
                totalReceived: { $last: '$totalReceived' },
                tableId: { $last: '$tableId' },
                employeeId: { $last: '$employeeId' },
                guestId: { $last: '$guestId' },
                billed: { $last: '$billed' },
                totalByItem: { $sum: '$saleDetailDoc.amount' },
                qtyByItem: { $sum: '$saleDetailDoc.qty' },
                productName: { $last: '$productDoc.name' },

                saleDetail: {
                  $addToSet: {
                    _id: '$saleDetailDoc._id',
                    itemId: '$saleDetailDoc.itemId',
                    qty: '$saleDetailDoc.qty',
                    price: '$saleDetailDoc.price',
                    discount: '$saleDetailDoc.discount',
                    amount: '$saleDetailDoc.amount',
                    invoiceId: '$saleDetailDoc.invoiceId',
                    checkPrint: '$saleDetailDoc.checkPrint',
                    date: '$date',
                  },
                },
              },
            },
            {
              $sort: {
                productName: 1,
              },
            },
            {
              $group: {
                _id: null,
                grandTotal: { $sum: '$totalByItem' },
                data: { $push: '$$ROOT' },
              },
            },
          ])
          .toArray()

        if (data[0]) {
          data[0].data = data[0].data.filter(function(item) {
            return item._id !== null
          })

          itemsDoc = data[0]
        } else {
          itemsDoc = []
        }
      } else {
        let data = await Sales.aggregate([
          {
            $match: selector,
          },
          {
            $lookup: {
              from: 'rest_saleDetails',
              localField: '_id',
              foreignField: 'invoiceId',
              as: 'saleDetailDoc',
            },
          },
          {
            $unwind: {
              path: '$saleDetailDoc',
              preserveNullAndEmptyArrays: true,
            },
          },
          {
            $lookup: {
              from: 'rest_products',
              localField: 'saleDetailDoc.itemId',
              foreignField: '_id',
              as: 'productDoc',
            },
          },
          {
            $unwind: { path: '$productDoc', preserveNullAndEmptyArrays: true },
          },
          {
            $match: categorySelector,
          },
          {
            $group: {
              _id: {
                _id: '$saleDetailDoc.itemId',
                price: '$saleDetailDoc.price',
                discount: '$saleDetailDoc.discount',
              },
              totalByItem: { $sum: '$saleDetailDoc.amount' },
              qtyByItem: { $sum: '$saleDetailDoc.qty' },
              productName: { $last: '$productDoc.name' },
            },
          },
          {
            $group: {
              _id: '$productName',
              totalQty: { $sum: '$qtyByItem' },
              grandTotal: { $sum: '$totalByItem' },
              data: { $push: '$$ROOT' },
            },
          },
          {
            $sort: {
              'data.productName': 1,
            },
          },
          {
            $group: {
              _id: null,
              grandTotal: { $sum: '$grandTotal' },
              data: { $push: '$$ROOT' },
            },
          },
        ])
        if (data[0]) {
          data[0].data = data[0].data.filter(function(item) {
            return item._id !== null
          })

          itemsDoc = data[0]
        } else {
          itemsDoc = []
        }
      }

      return itemsDoc
    }
  },
})

// Sale Receipt methods
export const saleReceiptReport = new ValidatedMethod({
  name: 'rest.saleReceiptReport',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    reportPeriod: Array,
    'reportPeriod.$': Date,
    guestId: {
      type: Array,
      optional: true,
    },
    'guestId.$': {
      type: String,
    },
    status: {
      type: Array,
      optional: true,
    },
    'status.$': {
      type: String,
    },
    employeeId: {
      type: Array,
      optional: true,
    },
    'employeeId.$': {
      type: String,
    },
    paperSize: {
      type: String,
      optional: true,
    },
  }).validator(),
  run(params) {
    if (Meteor.isServer) {
      Meteor._sleepForMs(500)

      const dateF = moment(params.reportPeriod[0]).toDate()

      const dateT = moment(params.reportPeriod[1]).toDate()

      // Pick query
      let selector = {
        date: {
          $gte: dateF,
          $lte: dateT,
        },
      }
      if (!_.isEmpty(params.guestId)) {
        selector.guestId = { $in: params.guestId }
      }
      if (!_.isEmpty(params.status)) {
        selector.status = { $in: params.status }
      }
      if (!_.isEmpty(params.employeeId)) {
        selector.employeeId = { $in: params.employeeId }
      }

      // Aggregate with sales
      let data = SaleReceipts.aggregate([
        { $match: selector },
        {
          $lookup: {
            from: 'rest_guests',
            localField: 'guestId',
            foreignField: '_id',
            as: 'guestDoc',
          },
        },

        { $unwind: { path: '$guestDoc', preserveNullAndEmptyArrays: true } },
        {
          $lookup: {
            from: 'users',
            localField: 'employeeId',
            foreignField: '_id',
            as: 'employeeDoc',
          },
        },

        { $unwind: { path: '$employeeDoc', preserveNullAndEmptyArrays: true } },

        {
          $addFields: {
            guestName: '$guestDoc.name',
            employeeName: '$employeeDoc.profile.fullName',
          },
        },
        { $project: { guestDoc: 0, employeeDoc: 0 } },
        {
          $sort: {
            date: -1,
          },
        },
      ])

      return data
    }
  },
})

rateLimit({
  methods: [],
})
