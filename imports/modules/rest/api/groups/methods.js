import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'

import { throwError } from '../../../../utils/security'
import rateLimit from '/imports/utils/rate-limit'
import getNextSeq from '/imports/utils/get-next-seq'

import GroupSchema from './schema'
import Groups from './collection'
import Products from '../products/collection'

// Find
export const findGroups = new ValidatedMethod({
  name: 'rest.findGroups',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
    options: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector, options }) {
    if (Meteor.isServer) {
      selector = selector || {}
      options = options || {}

      return Groups.find(selector, options).fetch()
    }
  },
})

// Find One
export const findOneGroup = new ValidatedMethod({
  name: 'rest.findOneGroup',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return Groups.findOne({ _id })
    }
  },
})

// Upsert Group
export const upsertGroup = new ValidatedMethod({
  name: 'rest.upsertGroup',
  mixins: [CallPromiseMixin],
  validate: _.clone(GroupSchema)
    .extend({
      _id: {
        type: String,
        optional: true,
      },
    })
    .validator(),
  run(doc) {
    if (Meteor.isServer) {
      try {
        return Groups.upsert({ _id: doc._id }, { $set: doc })
      } catch (error) {
        throwError(error)
      }
    }
  },
})

// Soft remove
export const softRemoveGroup = new ValidatedMethod({
  name: 'rest.softRemoveGroup',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return Groups.softRemove(_id)
    }
  },
})

// Restore
export const restoreGroup = new ValidatedMethod({
  name: 'rest.restoreGroup',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return Groups.restore(_id)
    }
  },
})

// Remove
export const removeGroup = new ValidatedMethod({
  name: 'rest.removeGroup',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return Groups.remove(_id)
    }
  },
})

// usedGroup
export const usedGroup = new ValidatedMethod({
  name: 'rest.usedGroup',
  mixins: [CallPromiseMixin],
  validate: null,
  run(_id) {
    if (Meteor.isServer) {
      let used = false
      let productDoc = Products.findOne({ groupId: _id })
      if (productDoc) {
        used = true
      } else {
        used = false
      }

      return { used }
    }
  },
})

rateLimit({
  methods: [
    findGroups,
    findOneGroup,
    softRemoveGroup,
    restoreGroup,
    removeGroup,
    usedGroup,
  ],
})
