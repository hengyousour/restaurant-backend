import { Mongo } from 'meteor/mongo'
import Schema from './schema'

const Groups = new Mongo.Collection('rest_groups')
Groups.attachSchema(Schema)
Groups.timestamp()

export default Groups
