// Card
import SaleCard from './pages/SaleCard.vue'
// Discount
import Discount from './pages/Discount.vue'
// import DiscountNew from './pages/DiscountNew.vue'
// Setting
import Setting from './pages/Setting.vue'
// Group
import Group from './pages/Group.vue'
// Category
import Category from './pages/Category.vue'
// Station
import Station from './pages/Station.vue'
// Unit
import Unit from './pages/Unit.vue'
// Ingredient
import Ingredient from './pages/Ingredient.vue'
// Product
import Product from './pages/Product.vue'
import ProductNew from './pages/ProductNew.vue'
import ProductEdit from './pages/ProductEdit.vue'
// Dishes
import DishNew from './pages/DishNew.vue'
import DishEdit from './pages/DishEdit.vue'
// Service
import ServiceNew from './pages/ServiceNew.vue'
import ServiceEdit from './pages/ServiceEdit.vue'
// Catalog
import CatalogNew from './pages/CatalogNew.vue'
import CatalogEdit from './pages/CatalogEdit.vue'

// Pos
import Post from './pages/Post.vue'
// Data
import Data from './pages/Data.vue'
//supplier
import Supplier from './pages/Supplier.vue'
//guest
import Guest from './pages/Guest.vue'

//Floor
import Floor from './pages/Floor.vue'
//Table
import Table from './pages/Table.vue'
// Purchase
import Purchase from './pages/Purchase.vue'
import PurchaseNew from './pages/PurchaseNew.vue'
import PurchaseEdit from './pages/PurchaseEdit.vue'
// Purchase payment
import PaymentQuick from './pages/PaymentQuick.vue'

// Sale Table
import SaleTable from './pages/SaleTable.vue'
// Sale
import Sale from './pages/Sale.vue'

// Invoice
import Invoice from './pages/Invoice.vue'
// InvoiceToKitchen report
import InvoiceToKitchen from './pages/InvoiceToKitchen.vue'
// Reports
import IndexReport from './reports/Index.vue'
// product Report
import ProductReport from './reports/ProductReport.vue'
// Purchase Report
import PurchaseReport from './reports/PurchaseReport.vue'
import PurchasePaymentReport from './reports/PurchasePaymentReport.vue'
// Sale Report
import SaleReport from './reports/SaleReport.vue'
import SaleDetail from './reports/SaleDetail.vue'
import SaleDetailByItem from './reports/SaleDetailByItem.vue'
import SaleReceiptReport from './reports/SaleReceiptReport.vue'
// Stock Report
import StockReport from './reports/StockReport.vue'
// Customer Owe Report
import CustomerOweReport from './reports/CustomerOweReport.vue'

// Import data
import ImportData from './pages/ImportData'

const routes = [
  {
    path: '/rest',
    component: {
      render(h) {
        return h('router-view')
      },
    },
    children: [
      /**************
       SaleCard
       *************/
      {
        path: 'saleCard',
        name: 'rest.saleCard',
        component: SaleCard,
        meta: {
          pageTitle: 'SaleCard',
          breadcrumb: {
            title: 'SaleCard',
            parent: 'home',
          },
        },
      },

      /**************
       * Setting
       *************/
      {
        path: 'setting',
        name: 'rest.setting',
        component: Setting,
        meta: {
          pageTitle: 'Setting',
          breadcrumb: {
            title: 'Setting',
            parent: 'home',
          },
        },
      },
      // Group
      {
        path: 'group',
        name: 'rest.group',
        component: Group,
        meta: {
          pageTitle: 'Group',
          linkActiveClass: 'rest.setting',
          breadcrumb: {
            title: 'Group',
            parent: 'rest.setting',
          },
        },
      },
      // Category
      {
        path: 'category',
        name: 'rest.category',
        component: Category,
        meta: {
          pageTitle: 'Category',
          linkActiveClass: 'rest.setting',
          breadcrumb: {
            title: 'Category',
            parent: 'rest.setting',
          },
        },
      },
      // Station
      {
        path: 'station',
        name: 'rest.station',
        component: Station,
        meta: {
          pageTitle: 'Station',
          linkActiveClass: 'rest.setting',
          breadcrumb: {
            title: 'Station',
            parent: 'rest.setting',
          },
        },
      },
      // Unit
      {
        path: 'unit',
        name: 'rest.unit',
        component: Unit,
        meta: {
          pageTitle: 'Unit',
          linkActiveClass: 'rest.setting',
          breadcrumb: {
            title: 'Unit',
            parent: 'rest.setting',
          },
        },
      },
      // Ingredient
      {
        path: 'ingredient',
        name: 'rest.ingredient',
        component: Ingredient,
        meta: {
          pageTitle: 'Ingredient',
          linkActiveClass: 'rest.setting',
          breadcrumb: {
            title: 'Ingredient',
            parent: 'rest.setting',
          },
        },
      },
      // All Product
      {
        path: 'product/type',
        name: 'rest.product',
        component: Product,
        meta: {
          pageTitle: 'Product',
          linkActiveClass: 'rest.setting',
          breadcrumb: {
            title: 'Product',
            parent: 'rest.setting',
          },
        },
      },
      /**************
       * Import data
       *************/
      {
        path: 'importData',
        name: 'rest.importData',
        component: ImportData,
        meta: {
          pageTitle: 'ImportData',
          linkActiveClass: 'rest.setting',
          breadcrumb: {
            title: 'ImportData',
            parent: 'rest.setting',
          },
        },
      },
      // Product New
      {
        path: 'productNew/type',
        name: 'rest.productNew',
        component: ProductNew,
        meta: {
          pageTitle: 'New Product',
          linkActiveClass: 'rest.product',
          breadcrumb: {
            title: 'New Product',
            parent: 'rest.product',
          },
        },
      },
      // Product Edit
      {
        path: 'productEdit/:_id',
        name: 'rest.productEdit',
        component: ProductEdit,
        meta: {
          pageTitle: 'Edit Product',
          linkActiveClass: 'rest.product',
          breadcrumb: {
            title: 'Edit Product',
            parent: 'rest.product',
          },
        },
      },
      // Dish New
      {
        path: 'dishNew/type',
        name: 'rest.dishNew',
        component: DishNew,
        meta: {
          pageTitle: 'New Dish',
          linkActiveClass: 'rest.product',
          breadcrumb: {
            title: 'New Dish',
            parent: 'rest.product',
          },
        },
      },
      // Dish Edit
      {
        path: 'dishEdit/:_id',
        name: 'rest.dishEdit',
        component: DishEdit,
        meta: {
          pageTitle: 'Edit Dish',
          linkActiveClass: 'rest.product',
          breadcrumb: {
            title: 'Edit Dish',
            parent: 'rest.product',
          },
        },
      },
      // Service New
      {
        path: 'serviceNew/type',
        name: 'rest.serviceNew',
        component: ServiceNew,
        meta: {
          pageTitle: 'New Service',
          linkActiveClass: 'rest.product',
          breadcrumb: {
            title: 'New Service',
            parent: 'rest.product',
          },
        },
      },
      // Service Edit
      {
        path: 'serviceEdit/:_id',
        name: 'rest.serviceEdit',
        component: ServiceEdit,
        meta: {
          pageTitle: 'Edit Service',
          linkActiveClass: 'rest.product',
          breadcrumb: {
            title: 'Edit Service',
            parent: 'rest.product',
          },
        },
      },
      // Catalog New
      {
        path: 'catalogNew/type',
        name: 'rest.catalogNew',
        component: CatalogNew,
        meta: {
          pageTitle: 'New Catalog',
          linkActiveClass: 'rest.product',
          breadcrumb: {
            title: 'New Catalog',
            parent: 'rest.product',
          },
        },
      },
      // Catalog Edit
      {
        path: 'catalogEdit/:_id',
        name: 'rest.catalogEdit',
        component: CatalogEdit,
        meta: {
          pageTitle: 'Edit Catalog',
          linkActiveClass: 'rest.product',
          breadcrumb: {
            title: 'Edit Catalog',
            parent: 'rest.product',
          },
        },
      },

      /****************
       * Post
       ***************/
      // Post
      {
        path: 'post',
        name: 'rest.post',
        component: Post,
        meta: {
          pageTitle: 'Post',
          breadcrumb: {
            title: 'Post',
            parent: 'home',
          },
        },
      },
      /**************
       * Data
       *************/
      {
        path: 'data',
        name: 'rest.data',
        component: Data,
        meta: {
          pageTitle: 'Data',
          breadcrumb: {
            title: 'Data',
            parent: 'home',
          },
        },
      },

      // Supplier
      {
        path: 'supplier',
        name: 'rest.supplier',
        component: Supplier,
        meta: {
          pageTitle: 'Supplier',
          linkActiveClass: 'rest.data',
          breadcrumb: {
            title: 'Supplier',
            parent: 'rest.data',
          },
        },
      },
      // Guest
      {
        path: 'guest',
        name: 'rest.guest',
        component: Guest,
        meta: {
          pageTitle: 'Guest',
          linkActiveClass: 'rest.data',
          breadcrumb: {
            title: 'Guest',
            parent: 'rest.data',
          },
        },
      },
      // Floor
      {
        path: 'floor',
        name: 'rest.floor',
        component: Floor,
        meta: {
          pageTitle: 'Floor',
          linkActiveClass: 'rest.data',
          breadcrumb: {
            title: 'Floor',
            parent: 'rest.data',
          },
        },
      },
      // Table
      {
        path: 'table',
        name: 'rest.table',
        component: Table,
        meta: {
          pageTitle: 'Table',
          linkActiveClass: 'rest.table',
          breadcrumb: {
            title: 'Table',
            parent: 'rest.data',
          },
        },
      },

      // Purchase
      {
        path: 'purchase',
        name: 'rest.purchase',
        component: Purchase,
        meta: {
          pageTitle: 'Purchase',
          linkActiveClass: 'rest.data',
          breadcrumb: {
            title: 'Purchase',
            parent: 'rest.data',
          },
        },
      },
      {
        path: 'purchaseNew',
        name: 'rest.purchaseNew',
        component: PurchaseNew,
        meta: {
          pageTitle: 'PurchaseNew',
          linkActiveClass: 'rest.purchase',
          breadcrumb: {
            title: 'PurchaseNew',
            parent: 'rest.purchase',
          },
        },
      },
      {
        path: 'purchaseEdit/:_id',
        name: 'rest.purchaseEdit',
        component: PurchaseEdit,
        meta: {
          pageTitle: 'PurchaseEdit',
          linkActiveClass: 'rest.purchase',
          breadcrumb: {
            title: 'PurchaseEdit',
            parent: 'rest.purchase',
          },
        },
      },

      // Purchase payment quick
      {
        path: 'paymentQuick',
        name: 'rest.paymentQuick',
        component: PaymentQuick,
        meta: {
          pageTitle: 'QuickPayment',
          linkActiveClass: 'rest.purchase',
          breadcrumb: {
            title: 'QuickPayment',
            parent: 'rest.purchase',
          },
        },
      },

      /**************
       * Discount
       *************/
      {
        path: 'discount',
        name: 'rest.discount',
        component: Discount,
        meta: {
          pageTitle: 'Discount',
          linkActiveClass: 'rest.discount',
          breadcrumb: {
            title: 'Discount',
            parent: 'rest.data',
          },
        },
      },

      // SaleTable
      {
        path: 'saleTable',
        name: 'rest.saleTable',
        component: SaleTable,
        meta: {
          layout: 'sale',
          pageTitle: 'SaleTable',
        },
      },

      // Sale
      {
        path: 'sale/:table/:employeeId/:guestId',
        name: 'rest.sale',
        component: Sale,
        meta: {
          layout: 'sale',
          pageTitle: 'Sale',
        },
      },

      // Invoice
      {
        path: 'invoice',
        name: 'rest.invoice',
        component: Invoice,
        meta: {
          layout: 'sale',
          pageTitle: 'Invoice',
        },
      },

      // InvoiceToKitchen
      {
        path: 'invoiceToKitchen',
        name: 'rest.invoiceToKitchen',
        component: InvoiceToKitchen,
        meta: {
          layout: 'sale',
          pageTitle: 'InvoiceToKitchen',
        },
      },

      /*****************
       * Report
       ****************/
      {
        path: 'report',
        name: 'rest.report',
        component: IndexReport,
        meta: {
          pageTitle: 'Report',
          breadcrumb: {
            title: 'Report',
            parent: 'home',
          },
        },
      },
      //  Product Report
      {
        path: 'product-report',
        name: 'rest.productReport',
        component: ProductReport,
        meta: {
          pageTitle: 'Product Report',
          linkActiveClass: 'rest.report',
          breadcrumb: {
            title: 'Product Report',
            parent: 'rest.report',
          },
        },
      },
      //  Purchase Report
      {
        path: 'purchase-report',
        name: 'rest.purchaseReport',
        component: PurchaseReport,
        meta: {
          pageTitle: 'Purchase Report',
          linkActiveClass: 'rest.report',
          breadcrumb: {
            title: 'Purchase Report',
            parent: 'rest.report',
          },
        },
      },
      //  Purchase Payment Report
      {
        path: 'purchasePayment-report',
        name: 'rest.purchasePaymentReport',
        component: PurchasePaymentReport,
        meta: {
          pageTitle: 'Purchase Payment Report',
          linkActiveClass: 'rest.report',
          breadcrumb: {
            title: 'Purchase Payment Report',
            parent: 'rest.report',
          },
        },
      },
      // Sale Report
      {
        path: 'sale-report',
        name: 'rest.saleReport',
        component: SaleReport,
        meta: {
          pageTitle: 'Sale Report',
          linkActiveClass: 'rest.report',
          breadcrumb: {
            title: 'Sale Report',
            parent: 'rest.report',
          },
        },
      },
      // Sale Detail Report
      {
        path: 'saleDetail-report',
        name: 'rest.saleDetailReport',
        component: SaleDetail,
        meta: {
          pageTitle: 'Sale Detail Report',
          linkActiveClass: 'rest.report',
          breadcrumb: {
            title: 'Sale Detail Report',
            parent: 'rest.report',
          },
        },
      },
      // Sale Detail Report
      {
        path: 'saleDetailByItem-report',
        name: 'rest.saleDetailByItemReport',
        component: SaleDetailByItem,
        meta: {
          pageTitle: 'Sale Detail By Item Report',
          linkActiveClass: 'rest.report',
          breadcrumb: {
            title: 'Sale Detail By Item Report',
            parent: 'rest.report',
          },
        },
      },
      // Sale Receipt Report
      {
        path: 'saleReceipt-report',
        name: 'rest.saleReceiptReport',
        component: SaleReceiptReport,
        meta: {
          pageTitle: 'Sale Receipt Report',
          linkActiveClass: 'rest.report',
          breadcrumb: {
            title: 'Sale Receipt Report',
            parent: 'rest.report',
          },
        },
      },
      // Stock Report
      {
        path: 'stock-report',
        name: 'rest.stockReport',
        component: StockReport,
        meta: {
          pageTitle: 'Stock Report',
          linkActiveClass: 'rest.report',
          breadcrumb: {
            title: 'Stock Report',
            parent: 'rest.report',
          },
        },
      },
      // Customer Owe Report
      {
        path: 'customerOwe-report',
        name: 'rest.customerOweReport',
        component: CustomerOweReport,
        meta: {
          pageTitle: 'Customer Owe Report',
          linkActiveClass: 'rest.report',
          breadcrumb: {
            title: 'Customer Owe Report',
            parent: 'rest.report',
          },
        },
      },
    ],
  },
]

export default routes
