const reportCSSPrinting = `
  .invoice-table {
    position: relative;
    overflow: hidden;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    -webkit-box-flex: 1;
    -ms-flex: 1;
    flex: 1;
    width: 100%;
    max-width: 100%;
    font-size: 14px;
    color: #606266;
  }

  .resize-triggers, .resize-triggers > div, .contract-trigger:before {
    content: " ";
    display: block;
    position: absolute;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
    overflow: hidden;
    z-index: -1;
  }

  .invoice-table th{
    background-color: #e0e0e0;
    -webkit-print-color-adjust: exact; 
    border-bottom: 1px solid #ebeef5;
  }
  .invoice-table td{
    border-bottom: 1px solid #ebeef5;
  }

  .invoice-table .cell-table-invoice .cell {
      text-align: right;
  }

  .invoice-table .cell-table-invoice-center .cell {
    text-align: center;
  }  
  .invoice-table .header-row-table-invoice th {
    font-size: 13px ;
    padding: 0 ;
  }

  .invoice-table .header-row-table-invoice th .cell{
    background-color: #e0e0e0;
    -webkit-print-color-adjust: exact; 
    color: #000;
  }
  
  .invoice-table .row-table-invoice td .cell{
    color: #000 ;
    padding: 3px 3px  !important;
    font-size: 13px ;
    line-height: 22px;
  }

 
  .text-right {
    text-align: right;
  }

  .text-center{
    text-align: center;
  }

  .text-left {
    text-align: left;
  }
  
  .invoice-container {
    position: relative;
    width: 100mm;
    min-height: 150px;
    display: block;
    padding-right: 35px;
    margin: 0 auto;
    top: 20px;
    border: 1px solid #eee;
  }

  .header {
    padding: 5px 5px 0px 5px;
    text-align: center;
    margin: 0;
    font-size: 17px;
  }

  .company-name {
    font-size: 22px ;
    margin: 0 ;
    }

  .location-name {
    margin: 0;
    font-size: 15px;
    line-height: 25px;
   }

  .tel-text {
    margin: 0;
    line-height: 15px;
    font-size: 12px;
  }
  .invoice-text {
    padding: 3px 3px 0px 5px;
    text-align: center;
    margin: 0;
    font-size: 17px;
    line-height: 25px;
  }

  .table-content {
    width: 100%;
    height: auto;
  }

  .full-side {
    width: 90%;
    display: block;
  }

  .full-size-text{
    font-size: 12px;
    text-align: left;
    margin: 0 0;
    word-wrap: break-word;
    font-weight: bold;
  }

  .left-side {
    width: 53%;
    display: inline-block;
    vertical-align:top;
  }

  .left-size-text{
    font-size: 14px;
    text-align: left;
    margin: 0 0;
    word-wrap: break-word;
    display: block;
    font-weight: bold;
    line-height: 22px;
  }

  .right-side {
    width: 45%;
    display: inline-block;
    vertical-align:top;
  }

  .right-size-text {
    font-size: 14px;
    text-align: right;
    margin: 0 0;
    word-wrap: break-word;
    display: block;
    font-weight: bold;
    line-height: 22px;
  }

  .full-size-text {
    font-size: 14px;
    text-align: left;
    margin: 0 0;
    word-wrap: break-word;
    display: block;
    font-weight: bold;
    line-height: 22px;
  }

  .span-text{
    font-size: 14px;
    font-weight: normal;
  }

  .detail-content {
    width: 100%;
    height: auto;
    display: inline-block;
  }

  .order-name-text {
    margin: 0;
    word-break: keep-all;
    line-height:21px;
  }

  .content {
    position: relative;
    width: 60%;
    height: auto;
    left: 40%;
  }
  
  .text-panel {
    display: block;
    padding-right: 5px;
  }

  .detail-print-text {
    width: 100%;
    display: inline-block;
    margin: 0;
    text-align: left;
    line-height: 23px !important;
    font-size: 12px;
    font-weight: bold;
  }

  .pay-amount-text{
    width: 100%;
    display: inline-block;
    margin: 0;
    text-align: left;
    line-height: 23px !important;
    font-size: 14px;
    font-weight: bold;
  }
  .pay-amount-sub-text{
    display:inline-block;
    width:50%;
    float: right !important;
    margin: 0;
    font-weight: bold;
    font-size: 16px;
    text-align: right;
    line-height: 22px;
    background-color: #e0e0e0;
    -webkit-print-color-adjust: exact;
  }

  .sub-text {
    float: right !important;
    margin: 0;
    font-weight: normal;
    font-size: 13px;
    text-align: right;
    line-height: 22px;

  }
  
  .pay-text{
    background-color: #e0e0e0;
    -webkit-print-color-adjust: exact; 
  }

  .thank-text {
    margin: 0;
    margin-top: 20px;
    font-size: 13px;
    text-align: center;
    line-height: 12px;
    font-weight: 600;
  }
  .footer-address {
    margin: 0;
    margin-top: 8px;
    font-size: 11px;
    text-align: center;
    line-height: 11px;
  }
  .power-by-text {
    margin: 0;
    margin-top: 5px;
    text-align: center;
    margin-bottom: 5px;
    font-size: 13px;
  }

  .hr-bottom {
    background-color: #fff;
    border-top: 1px dashed #8c8b8b;
  }
  hr {
    margin: 5px 0;
  }`

export default reportCSSPrinting
