const reportCSSPrinting = `
  .el-table {
    position: relative;
    overflow: hidden;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    -webkit-box-flex: 1;
    -ms-flex: 1;
    flex: 1;
    width: 100%;
    max-width: 100%;
    font-size: 14px;
    color: #606266;
  }

  .resize-triggers, .resize-triggers > div, .contract-trigger:before {
    content: " ";
    display: block;
    position: absolute;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
    overflow: hidden;
    z-index: -1;
  }

  .el-table th{
    background-color: #e0e0e0;
    border-bottom: 1px solid #ebeef5;
  }

  .el-table td{
    border-bottom: 1px solid #ebeef5;
  }

  .el-table .cell-table-invoice .cell {
      text-align: right;
  }

  .el-table .header-row-table-invoice th {
    font-size: 15px ;
    padding: 0 ;
  }

  .el-table .header-row-table-invoice th .cell{
    background-color: #e0e0e0;
    color: #000;
    padding: 1px 5px !important;
  }
  
  .el-table .row-table-invoice td .cell{
    color: #000 ;
    padding: 3px 3px !important;
    font-size: 18px ;
    font-weight: normal;
  }

 
  .text-right {
    text-align: right;
  }

  .text-left {
    text-align: left;
  }
  
  .invoice-container {
    position: relative;
    width: 100mm;
    min-height: 150px;
    display: block;
    padding-right: 35px;
    margin: 0 auto;
    top: 20px;
    border: 1px solid #eee;
  }


  .table-content {
    width: 100%;
    height: auto;
  }

  .full-side {
    width: 90%;
    display: block;
  }

  .full-size-text{
    font-size: 13px;
    text-align: left;
    margin: 0 0;
    word-wrap: break-word;
    font-weight: bold;
  }
  

  .left-side {
    width: 49%;
    display: inline-block;
    vertical-align:top;
  }

  .left-size-text{
    font-size: 15px;
    text-align: left;
    margin: 0 0;
    word-wrap: break-word;
    display: block;
    font-weight: bold;
    line-height: 22px;
  }

  .right-side {
    width: 49%;
    display: inline-block;
    vertical-align:top;
  }

  .right-size-text {
    font-size: 15px;
    text-align: right;
    margin: 0 0;
    word-wrap: break-word;
    display: block;
    font-weight: bold;
    line-height: 22px;
  }

  .full-size-text {
    font-size: 15px;
    text-align: left;
    margin: 0 0;
    word-wrap: break-word;
    display: block;
    font-weight: bold;
    line-height: 22px;
  }

  .span-text{
    font-size: 15px;
    font-weight: normal;
  }

  .detail-content {
    width: 100%;
    height: auto;
    display: inline-block;
  }

  .content {
    position: relative;
    width: 50%;
    height: auto;
    left: 50%;
  }

  .order-name-text{
    margin:0;
    word-break: keep-all;
  }

  .text-panel {
    display: block;
    padding-right: 5px;
  }

  .detail-print-text {
    width: 100%;
    display: inline-block;
    margin: 0;
    text-align: left;
    font-size: 12px;
    font-weight: bold;
  }

  .sub-text {
    float: right !important;
    margin: 0;
    font-weight: normal;
    font-size: 14px;
    text-align: right ;
  }
  
  .thank-text {
    margin: 0;
    text-align: center;
  }

  .power-by-text {
    margin: 0;
    text-align: center;
    margin-bottom: 5px;
    font-size: 14px;
  }

  .hr-bottom {
    background-color: #fff;
    border-top: 1px dashed #8c8b8b;
  }
  hr {
    margin: 5px 0;
  }`

export default reportCSSPrinting
