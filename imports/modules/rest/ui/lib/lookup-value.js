const LookupValue = {
  status: [
    { label: 'Public', value: 'Public' },
    { label: 'Private', value: 'Private' },
  ],
  productTypeOpts: [
    { label: 'Product', value: 'Product' },
    { label: 'Dish', value: 'Dish' },
    { label: 'Service', value: 'Service' },
    { label: 'Ingredient', value: 'Ingredient' },
  ],
  productRouteTypeOpts: [
    { label: 'Product', value: 'Product' },
    { label: 'Dish', value: 'Dish' },
    { label: 'Service', value: 'Service' },
    { label: 'Catalog', value: 'Catalog' },
  ],
  markupTypeOpts: [
    { label: 'Amount', value: 'Amount' },
    { label: 'Percentage', value: 'Percentage' },
  ],
  productStatus: [
    { label: 'Active', value: 'Active' },
    { label: 'Inactive', value: 'Inactive' },
  ],
}

export default LookupValue
