/**
 * Report
 */
import '../../api/reports'

/**
 * API
 */
// Post
import '../../api/posts/methods'
import '../../api/posts/server/hooks'
import '../../api/posts/server/publications'

// Category
import '../../api/categories/methods'
import '../../api/categories/server/hooks'
import '../../api/categories/server/publications'

// Group
import '../../api/groups/methods'
import '../../api/groups/server/hooks'
import '../../api/groups/server/publications'

// Station
import '../../api/stations/methods'
import '../../api/stations/server/hooks'
import '../../api/stations/server/publications'

// Unit
import '../../api/units/methods'
import '../../api/units/server/hooks'
import '../../api/units/server/publications'

// Product
import '../../api/products/methods'
import '../../api/products/server/hooks'
import '../../api/products/server/publications'

/**************
 * Data
 *************/
// Employee
import '../../api/employees/methods'
import '../../api/employees/server/hooks'
import '../../api/employees/server/publications'

//Supplier
import '../../api/suppliers/methods'
//Guest
import '../../api/guests/methods'

//Floor
import '../../api/floors/methods'
//Table
import '../../api/tables/methods'

// Purchase
import '../../api/purchases/methods'
import '../../api/purchases/server/hooks'
import '../../api/purchases/server/publications'
// Inventory
import '../../api/inventories/methods'
// Purchase Payment
import '../../api/purchasePayments/methods'
// Sale
import '../../api/sales/methods'
// Sale Receipt
import '../../api/saleReceipts/methods'

// Import
import '../../api/files/methods'

// Discount Product
import '../../api/discounts/methods'
