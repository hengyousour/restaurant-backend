import { Meteor } from 'meteor/meteor'

// Category
import Categories from '../../api/categories/collection'

// Group
import Groups from '../../api/groups/collection'

// Unit
import Units from '../../api/units/collection'

// Station
import Stations from '../../api/stations/collection'

// Guest
import Suppliers from '../../api/suppliers/suppliers'

// Floors
import Floors from '../../api/floors/floors'

// Tables
import Tables from '../../api/tables/tables'
import { upsertTable } from '../../api/tables/methods'

// Guest
import Guests from '../../api/guests/guests'
import { upsertGuest } from '../../api/guests/methods'

Meteor.startup(function() {
  // // Category
  if (Categories.find({ productType: 'ExtraFood' }).count() === 0) {
    const data = [{ name: 'Extra Food', productType: 'ExtraFood' }]

    data.forEach(doc => {
      Categories.insert(doc)
    })
  }

  // // Group
  // if (Groups.find().count() === 0) {
  //   const data = [{ name: 'Group 1' }, { name: 'Group 2' }]
  //   data.forEach(doc => {
  //     Groups.insert(doc)
  //   })
  // }

  // // Unit
  // if (Units.find().count() === 0) {
  //   const data = [{ name: 'ដប' }, { name: 'កំប៉ុង' }]
  //   data.forEach(doc => {
  //     Units.insert(doc)
  //   })
  // }

  // // Stations
  // if (Stations.find().count() === 0) {
  //   const data = [{ name: 'Bar' }, { name: 'ផ្ទះបាយ' }]
  //   data.forEach(doc => {
  //     Stations.insert(doc)
  //   })
  // }

  // // Suppliers
  // if (Suppliers.find().count() === 0) {
  //   const data = [
  //     { name: 'Suppliers 1', telephone: '000000000' },
  //     { name: 'Suppliers 2', telephone: '000000000' },
  //   ]
  //   data.forEach(doc => {
  //     Suppliers.insert(doc)
  //   })
  // }

  // Guest
  if (Guests.find().count() === 0) {
    const doc = { name: 'General' }
    upsertGuest.run(doc)
  }

  // Floors
  if (Floors.find().count() === 0) {
    const data = [{ name: 'ជាន់ទី​ 1' }, { name: 'ជាន់ទី​ 2' }]
    data.forEach(doc => {
      Floors.insert(doc)
    })
  }

  // Tables
  if (Tables.find().count() === 0) {
    let floors = Floors.find().fetch()

    const data = [
      {
        name: 'តុទី 1',
        floorId: floors[0]._id,
        numOfGuest: 1,
        status: 'Open',
      },
      {
        name: 'តុទី 2',
        floorId: floors[1]._id,
        numOfGuest: 1,
        status: 'Open',
      },
    ]

    data.forEach(doc => {
      upsertTable.run(doc)
    })
  }
})
