/**
 * Report
 */
// import '../../api/reports'

/**
 * API
 */
// Users
import '../../api/users/methods'
import '../../api/users/server/api-service'
//Floors
import '../../api/floors/methods'
import '../../api/floors/server/api-server'
//Tables
import '../../api/tables/methods'
import '../../api/tables/server/api-server'
//Products
import '../../api/products/methods'
import '../../api/products/server/api-server'
//Guest
import '../../api/guests/methods'
import '../../api/guests/server/api-server'
//Categories
import '../../api/categories/methods'
import '../../api/categories/server/api-server'
//Discount
import '../../api/discount/methods'
import '../../api/discount/server/api-server'
//Discount
import '../../api/sales/methods'
import '../../api/sales/server/api-server'
//Company
import '../../api/company/methods'
import '../../api/company/server/api-server'