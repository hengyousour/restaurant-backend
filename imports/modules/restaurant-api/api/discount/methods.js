import _ from 'lodash'
import rateLimit from '/imports/utils/rate-limit'
// Table
import Discounts from '../../../rest/api/discounts/collection'
export class DISCOUNT {
    static findDiscount(selector, options) {
        return Discounts.find(selector, options).fetch()
    }
}