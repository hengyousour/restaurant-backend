import { Meteor } from 'meteor/meteor'
import { Accounts } from 'meteor/accounts-base'
import { Roles } from 'meteor/alanning:roles'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'

import rateLimit from '/imports/utils/rate-limit'

//model
export class USERS {
    static findUser(selector, options) {
        let users = Meteor.users.findOne({ username: selector.username }, options)
        var checkPassword = ''
        if (users) {
            let digest = Package.sha.SHA256(selector.password || {})

            let passwordOpts = { digest: digest, algorithm: 'sha-256' }

            checkPassword = Accounts._checkPassword(users, passwordOpts)
        }
        if (!checkPassword.error) {
            return users
        }
    }
}

rateLimit({
    methods: [USERS.findUser],
})