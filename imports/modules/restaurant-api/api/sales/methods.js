import _ from 'lodash'
import moment from 'moment'
import math from 'mathjs'

import { throwError } from '../../../../utils/security'
import getNextSeq from '/imports/utils/get-next-seq'

// Sales
import Sales from '../../../rest/api/sales/sales'
import SaleDetails from '../../../rest/api/sales/saleDetails'
// discount
import Discounts from '../../../rest/api/discounts/collection'

export class SALES {
    static findSales(selector, options) {
        // **selector example** //
        // selector: { tableId: '01',status: 'Open'},
        // Find data
        let data = Sales.find(selector, options).fetch()
        _.forEach(data, o => {
                o.details = SaleDetails.aggregate([
                    { $match: { invoiceId: o._id } },
                    {
                        $lookup: {
                            from: 'rest_products',
                            localField: 'itemId',
                            foreignField: '_id',
                            as: 'productDoc',
                        },
                    },
                    {
                        $unwind: {
                            path: '$productDoc',
                            preserveNullAndEmptyArrays: true,
                        },
                    },
                    {
                        $addFields: {
                            itemName: '$productDoc.name',
                            itemType: '$productDoc.type',
                        },
                    },
                    { $project: { productDoc: 0 } },
                ])
                o.details = _.reverse(o.details)
            })
            // Sort data
        data.sort()
            // Return
        return data
    }

    static findSaleAggregate(tableQuery, params) {
        tableQuery = tableQuery || {}
        params = params || {}

        // Selector
        const selector = {}
        if (!_.isEmpty(params.status)) {
            selector.status = params.status
        }
        if (params.billed != undefined) {
            selector.billed = {
                $gte: params.billed,
            }
            selector.status = 'Open'
        }

        // Filter data
        let filterSelector = {}
        if (!_.isEmpty(params.filters)) {
            filterSelector = {
                $or: [{
                        _id: new RegExp(params.filters.value, 'i'), //Can I put a limit here?
                    },
                    {
                        tableName: new RegExp(params.filters.value, 'i'), //Can I put a limit here?
                    },
                    {
                        guestName: new RegExp(params.filters.value, 'i'), //Can I put a limit here?
                    },
                ],
            }
        }
        // Options
        const sort = {
            [tableQuery.sort.prop]: tableQuery.sort.order === 'ascending' ? 1 : -1,
        }
        const skip = (tableQuery.page - 1) * tableQuery.pageSize
        const limit = tableQuery.pageSize
        const options = { sort, skip, limit }

        // Find data
        const data = Sales.aggregate([{
                $match: selector,
            },
            {
                $lookup: {
                    from: 'rest_guests',
                    localField: 'guestId',
                    foreignField: '_id',
                    as: 'guestDocs',
                },
            },
            {
                $unwind: {
                    path: '$guestDocs',
                    preserveNullAndEmptyArrays: true,
                },
            },
            {
                $lookup: {
                    from: 'rest_tables',
                    localField: 'tableId',
                    foreignField: '_id',
                    as: 'tableDoc',
                },
            },
            {
                $unwind: {
                    path: '$tableDoc',
                    preserveNullAndEmptyArrays: true,
                },
            },
            {
                $addFields: {
                    tableName: '$tableDoc.name',
                    guestName: '$guestDoc.name',
                },
            },
            {
                $match: filterSelector,
            },
            {
                $sort: options.sort,
            },
            {
                $skip: options.skip,
            },
            {
                $limit: options.limit,
            },
            { $project: { tableDoc: 0, guestDoc: 0 } },
        ])

        // Details
        _.forEach(data, o => {
                o.details = SaleDetails.aggregate([
                    { $match: { invoiceId: o._id } },
                    {
                        $lookup: {
                            from: 'rest_products',
                            localField: 'itemId',
                            foreignField: '_id',
                            as: 'productDoc',
                        },
                    },
                    {
                        $unwind: {
                            path: '$productDoc',
                            preserveNullAndEmptyArrays: true,
                        },
                    },
                    {
                        $addFields: {
                            itemName: '$productDoc.name',
                        },
                    },
                    { $project: { productDoc: 0 } },
                ])
            })
            // Total record
        let total = Sales.find(selector).count()

        return { data: data, total: total }
    }

    /*===========
  ----insertSale-----
=============*/
    static insertSale(doc, details) {

        const invoiceId = getNextSeq({
            // Mandatory
            filter: {
                _id: 'rest_sales',
            },
            // Optional
            opts: {
                seq: 1,
                paddingType: 'start',
                paddingLength: 4,
                paddingChar: '0',
                prefix: moment(doc.date).format('YYMMDD'),
            },
        })
        try {
            doc._id = invoiceId.toString()
            const newDate = new Date()
            const checkDate = moment(newDate).toDate()
            let selector = {
                startDate: { $lte: checkDate },
                endDate: { $gte: checkDate },
                type: 'Invoice',
            }

            // Find discount
            let discount = Discounts.aggregate([{
                    $match: selector,
                },
                {
                    $group: {
                        _id: '$_id',
                        eventName: { $last: '$eventName' },
                        startDate: { $last: '$startDate' },
                        endDate: { $last: '$endDate' },
                        discountValue: { $last: '$discountValue' },
                        discountType: { $last: '$discountType' },
                        type: { $last: '$type' },
                    },
                },
            ])

            let discountDoc = null
            let checkTime = moment(newDate).format('HH:mm:ss')

            _.forEach(discount, o => {
                let startDate = moment(o.startDate).format('HH:mm:ss')
                let endDate = moment(o.endDate).format('HH:mm:ss')

                if (
                    Date.parse('01 Jan 2019 ' + checkTime) >=
                    Date.parse('01 Jan 2019 ' + startDate) &&
                    Date.parse('01 Jan 2019 ' + checkTime) <=
                    Date.parse('01 Jan 2019 ' + endDate)
                ) {
                    discountDoc = o
                }
            })

            // Add discount Doc
            if (!_.isEmpty(discountDoc)) {
                if (discountDoc.discountType == 'Amount') {
                    doc.discountValue = discountDoc.discountValue
                    doc.total = -discountDoc.discountValue
                } else {
                    doc.discountRate = discountDoc.discountValue
                }
                doc.discountDetail = {
                    _id: discountDoc._id,
                    discountValue: discountDoc.discountValue,
                    discountType: discountDoc.discountType,
                    type: discountDoc.type,
                }
            }

            // Insert
            Sales.insert(doc, error => {
                if (!error) {
                    if (doc.details && doc.details.length) {
                        _.forEach(doc.details, o => {
                            o.invoiceId = doc._id
                            SaleDetails.insert(o)
                        })
                    }
                }
            })

            return doc._id
        } catch (error) {
            // Decrement seq
            getNextSeq({
                filter: { _id: 'rest_sales' },
                opts: { seq: -1 },
            })
            SaleDetails.remove({ invoiceId: invoiceId })
            Sales.remove({ _id: invoiceId })
            throwError(error)
        }
    }

    /*===========
  ----insertSaleDetail-----
=============*/
    static insertSaleDetail(doc) {
        const invoiceId = doc.invoiceId
        try {
            const newDate = new Date()
            const checkDate = moment(newDate).toDate()
            let matchDate = {
                startDate: { $lte: checkDate },
                endDate: { $gte: checkDate },
                type: 'Item',
            }
            let matchItem = {
                'products.productId': doc.itemId,
            }

            // Find discount by product
            let discount = Discounts.aggregate([{
                    $match: matchDate,
                },
                {
                    $unwind: { path: '$products', preserveNullAndEmptyArrays: true },
                },
                {
                    $match: matchItem,
                },
                {
                    $group: {
                        _id: '$_id',
                        eventName: { $last: '$eventName' },
                        startDate: { $last: '$startDate' },
                        endDate: { $last: '$endDate' },
                        type: { $last: '$type' },
                        products: { $last: '$products' },
                    },
                },
            ])

            let discountDoc = null
            let checkTime = moment(newDate).format('HH:mm:ss')

            _.forEach(discount, o => {
                let startDate = moment(o.startDate).format('HH:mm:ss')
                let endDate = moment(o.endDate).format('HH:mm:ss')

                if (
                    Date.parse('01 Jan 2019 ' + checkTime) >=
                    Date.parse('01 Jan 2019 ' + startDate) &&
                    Date.parse('01 Jan 2019 ' + checkTime) <=
                    Date.parse('01 Jan 2019 ' + endDate)
                ) {
                    discountDoc = o
                }
            })

            // Selector
            let selector = {
                invoiceId: doc.invoiceId,
                itemId: doc.itemId,
                price: doc.price,
                discount: doc.discount,
                checkPrint: doc.checkPrint,
                extraItemDoc: { $exists: false },
                child: { $exists: false },
            }

            // discount doc exist
            if (!_.isEmpty(discountDoc)) {
                // Selector
                selector.discount = discountDoc.products.discount
            }
            // Find exist doc
            let checkExist = SaleDetails.findOne(selector)

            // Check exist doc
            if (checkExist) {
                doc.discount = checkExist.discount
                doc.price = checkExist.price
                let discountEachPrice = math
                    .chain(math.bignumber(checkExist.price))
                    .multiply(math.bignumber(checkExist.discount))
                    .divide(math.bignumber(100))
                    .done()
                    .toNumber()
                let priceAfterDiscount = math
                    .subtract(
                        math.bignumber(checkExist.price),
                        math.bignumber(discountEachPrice)
                    )
                    .toNumber()

                // Update detail
                SaleDetails.update({ _id: checkExist._id }, {
                        $inc: {
                            qty: doc.qty,
                            amount: _.round(priceAfterDiscount, 2),
                        },
                    },
                    error => {
                        if (!error) {
                            Sales.update({ _id: invoiceId }, { $inc: { total: _.round(priceAfterDiscount, 2) } })
                        }
                    }
                )
            } else {
                // If discount
                if (discountDoc) {
                    // Calculate discount
                    let discountEachPrice = math
                        .chain(math.bignumber(doc.price))
                        .multiply(math.bignumber(discountDoc.products.discount))
                        .divide(math.bignumber(100))
                        .done()
                        .toNumber()

                    // Assign value to amount
                    let amount = math
                        .subtract(
                            math.bignumber(doc.price),
                            math.bignumber(discountEachPrice)
                        )
                        .toNumber()
                    doc.discount = discountDoc.products.discount
                    doc.amount = amount
                } else {
                    // If not discount
                    // Amount
                    doc.amount = math
                        .multiply(math.bignumber(doc.qty), math.bignumber(doc.price))
                        .toNumber()
                }
                // Round amount before insert
                doc.amount = _.round(doc.amount, 2)
                    // Insert detail
                SaleDetails.insert(doc, error => {
                    if (!error) {
                        Sales.update({ _id: invoiceId }, { $inc: { total: doc.amount } })
                    }
                })
            }

            return doc
        } catch (error) {
            // Decrement seq
            throwError(error)
        }
    }

    /*===========
 ----updateSale-----
=============*/
    static updateSale(selector, modifier, options, callback) {

        let details = modifier.details || [];

        // Update sale
        return Sales.update(selector, { $set: modifier }, options, error => {
            if (!error) {
                if (details && details.length) {
                    _.forEach(details, o => {
                        SaleDetails.update({ _id: o._id }, { $set: o })
                    })
                }
            }
        }, callback)
    }

    /*===========
----insertSaleDetailExtra-----
=============*/
    static async insertSaleDetailExtra(doc, itemList) {
        const invoiceId = doc.invoiceId
        try {
            const newDate = new Date()
            const checkDate = moment(newDate).toDate()
            let matchDate = {
                startDate: { $lte: checkDate },
                endDate: { $gte: checkDate },
                type: 'Item',
            }
            let matchItem = {
                'products.productId': doc.itemId,
            }

            // Find discount
            let discount = Discounts.aggregate([{
                    $match: matchDate,
                },
                {
                    $unwind: { path: '$products', preserveNullAndEmptyArrays: true },
                },
                {
                    $match: matchItem,
                },
                {
                    $group: {
                        _id: '$_id',
                        eventName: { $last: '$eventName' },
                        startDate: { $last: '$startDate' },
                        endDate: { $last: '$endDate' },
                        type: { $last: '$type' },
                        products: { $last: '$products' },
                    },
                },
            ])

            let discountDoc = null
            let checkTime = moment(newDate).format('HH:mm:ss')

            _.forEach(discount, o => {
                let startDate = moment(o.startDate).format('HH:mm:ss')
                let endDate = moment(o.endDate).format('HH:mm:ss')

                if (
                    Date.parse('01 Jan 2019 ' + checkTime) >=
                    Date.parse('01 Jan 2019 ' + startDate) &&
                    Date.parse('01 Jan 2019 ' + checkTime) <=
                    Date.parse('01 Jan 2019 ' + endDate)
                ) {
                    discountDoc = o
                }
            })

            var data = null;

            itemList.forEach(async item => {
                if (item.itemType !== 'ExtraFood') {
                    // Clone Deep newItemDoc
                    let newItemDoc = _.cloneDeep(item)
                    let extraDoc = _.cloneDeep(doc)
                        //  If Discount doc exist
                    if (discountDoc) {
                        // Check discount type
                        let discountVal = math
                            .chain(math.bignumber(extraDoc.price))
                            .multiply(math.bignumber(discountDoc.products.discount))
                            .divide(math.bignumber(100))
                            .done()
                            .toNumber()
                            // Amount
                        let amount = extraDoc.price - discountVal

                        extraDoc.discount = discountDoc.products.discount
                        extraDoc.amount = _.round(amount, 2)
                    } else {
                        // If discount doc not exist
                        let amount = math
                            .multiply(
                                math.bignumber(extraDoc.qty),
                                math.bignumber(extraDoc.price)
                            )
                            .toNumber()

                        extraDoc.amount = _.round(amount, 2)
                    }

                    // Check if extraItemDetail already exist in array
                    if (item.extraItemDoc && item.extraItemDoc.length) {
                        // Find exist item in extraItemDetail
                        let extraItemDetail = _.find(item.extraItemDoc, function(obj) {
                                return obj.itemId === extraDoc.itemId
                            })
                            // Check if this item is exist in extraItemDetail
                        if (extraItemDetail) {
                            // Selector
                            let selector = {
                                invoiceId: invoiceId,
                                itemId: extraDoc.itemId,
                                price: extraDoc.price,
                                discount: extraDoc.discount,
                                checkPrint: extraDoc.checkPrint,
                                extraItemDoc: { $exists: false },
                                child: { $exists: false },
                            }

                            // Check exists
                            let checkExist = SaleDetails.findOne(selector)

                            if (checkExist) {
                                extraDoc.discount = checkExist.discount
                                extraDoc.price = checkExist.price
                                    // discountEachPrice
                                let discountEachPrice = math
                                    .chain(math.bignumber(checkExist.price))
                                    .multiply(math.bignumber(checkExist.discount))
                                    .divide(math.bignumber(100))
                                    .done()
                                    .toNumber()
                                    // priceAfterDiscount
                                let priceAfterDiscount = _.round(
                                    checkExist.price - discountEachPrice,
                                    2
                                )

                                // Update sale detail
                                SaleDetails.update({ _id: checkExist._id }, {
                                        $inc: {
                                            qty: extraDoc.qty,
                                            amount: priceAfterDiscount,
                                        },
                                    },
                                    error => {
                                        if (!error) {
                                            Sales.update({ _id: invoiceId }, { $inc: { total: priceAfterDiscount } })
                                        }
                                    }
                                )
                            } else {
                                await this.insertDetailAndExtraDetailSync(extraDoc, invoiceId)
                            }
                        } else {
                            // Check if item qty > 1
                            if (item.qty > 1) {
                                // Break item
                                item.qty = item.qty - 1
                                    // discountEachPrice
                                let discountEachPrice = math
                                    .chain(math.bignumber(item.price))
                                    .multiply(math.bignumber(item.discount))
                                    .divide(math.bignumber(100))
                                    .done()
                                    .toNumber()
                                    // priceAfterDiscount
                                let priceAfterDiscount = _.round(
                                    item.price - discountEachPrice,
                                    2
                                )

                                item.amount = _.round(item.amount - priceAfterDiscount, 2)
                                    // Assign value
                                delete newItemDoc._id
                                newItemDoc.qty = 1
                                newItemDoc.amount = priceAfterDiscount

                                // New item extra doc
                                if (newItemDoc.extraItemDoc && newItemDoc.extraItemDoc.length) {
                                    newItemDoc.extraItemDoc.forEach(e => {
                                        // extraDiscountEachPrice
                                        let extraDiscountEachPrice = math
                                            .chain(math.bignumber(e.price))
                                            .multiply(math.bignumber(e.discount))
                                            .divide(math.bignumber(100))
                                            .done()
                                            .toNumber()
                                            // extraDiscountEachPrice
                                        let extraPriceAfterDiscount =
                                            e.price - extraDiscountEachPrice

                                        e.amount = _.round(
                                                extraPriceAfterDiscount * newItemDoc.qty,
                                                2
                                            )
                                            // Delete extra item _id

                                        // Find extra doc to compare
                                        let newExtraDoc = SaleDetails.findOne({ _id: e._id })
                                        if (newExtraDoc) {
                                            newExtraDoc.qty = newItemDoc.qty
                                            newExtraDoc.amount = e.amount
                                            delete newExtraDoc._id
                                                // Update extra detail
                                            SaleDetails.insert(newExtraDoc, (err, res) => {
                                                if (res) {
                                                    e._id = res
                                                }
                                            })
                                        }
                                    })
                                }
                                // Item extra doc
                                if (item.extraItemDoc && item.extraItemDoc.length) {
                                    item.extraItemDoc.forEach(e => {
                                        let extraDiscountEachPrice = (e.price * e.discount) / 100
                                        let extraPriceAfterDiscount =
                                            e.price - extraDiscountEachPrice

                                        e.amount = _.round(e.amount - extraPriceAfterDiscount, 2)
                                            // Update extra detail
                                        SaleDetails.update({ _id: e._id }, { $set: { qty: newItemDoc.qty, amount: e.amount } })
                                    })
                                }
                                // Update item
                                SaleDetails.update({ _id: item._id }, { $set: item })
                            }

                            // Assign child = true for compare detail
                            extraDoc.child = true
                                // Check if this item not yet exist in extraItemDetail
                            SaleDetails.insert(extraDoc, (error, res) => {
                                if (!error) {
                                    // Update newItemDoc.extraItemDoc
                                    newItemDoc.extraItemDoc.push({
                                        _id: res,
                                        itemId: doc.itemId,
                                        itemName: doc.itemName,
                                        price: extraDoc.price,
                                        discount: extraDoc.discount,
                                        amount: extraDoc.amount,
                                    })

                                    if (newItemDoc._id) {
                                        // Update detail extraItemDoc
                                        SaleDetails.update({ _id: newItemDoc._id }, { $set: { extraItemDoc: newItemDoc.extraItemDoc } })
                                    } else {
                                        // Insert new detail
                                        SaleDetails.insert(newItemDoc)
                                    }

                                    // Update invoice amount
                                    Sales.update({ _id: invoiceId }, { $inc: { total: extraDoc.amount } })
                                }
                            })
                        }
                    } else {
                        // Check if item qty > 1
                        if (item.qty > 1) {
                            // Break item
                            item.qty = item.qty - 1
                            let discountEachPrice = (item.price * item.discount) / 100
                            let priceAfterDiscount = parseFloat(
                                item.price - discountEachPrice
                            ).toFixed(2)
                            priceAfterDiscount = parseFloat(priceAfterDiscount)
                            item.amount = item.amount - priceAfterDiscount
                                // Assign value
                            delete newItemDoc._id
                            newItemDoc.qty = 1
                            newItemDoc.amount = priceAfterDiscount

                            // Update item
                            SaleDetails.update({ _id: item._id }, { $set: item })
                        }

                        // Assign child = true for compare detail
                        extraDoc.child = true
                            // Insert detail
                        SaleDetails.insert(extraDoc, (error, res) => {
                            if (!error) {
                                // Update newItemDoc.extraItemDoc
                                newItemDoc.extraItemDoc = [{
                                    _id: res,
                                    itemId: doc.itemId,
                                    itemName: doc.itemName,
                                    price: extraDoc.price,
                                    discount: extraDoc.discount,
                                    amount: extraDoc.amount,
                                }, ]

                                if (newItemDoc._id) {
                                    // update detail
                                    SaleDetails.update({ _id: newItemDoc._id }, { $set: newItemDoc })
                                } else {
                                    // Insert new detail
                                    SaleDetails.insert(newItemDoc)
                                }
                                // Update Amount
                                Sales.update({ _id: invoiceId }, { $inc: { total: extraDoc.amount } })
                            }
                        })
                    }
                }
                data = SaleDetails.find({ invoiceId: invoiceId }).fetch();
            })

            // let data = SaleDetails.find({ invoiceId: invoiceId }).fetch();
            // console.log(itemList)
            console.log(data)
            return 'success';
        } catch (error) {
            // Decrement seq
            throwError(error)
        }
    }

    // Insert extra detail function helper
    // Insert sale detail
    static insertDetailAndExtraDetailSync(extraDoc, invoiceId) {
        SaleDetails.insert(extraDoc, error => {
            if (!error) {
                Sales.update({ _id: invoiceId }, { $inc: { total: extraDoc.amount } })
            }
        })
    }

    /*===========
      ----insertSaleDetailList-----
    =============*/
    static insertSaleDetailList(doc) {
        try {
            if (doc.products.length) {
                const invoiceId = doc.invoiceId

                // Each product
                _.forEach(doc.products, o => {
                    let detail = {
                        itemId: o.productId,
                        qty: o.qty,
                        price: o.amount,
                        invoiceId: invoiceId,
                        discount: 0,
                        amount: 0,
                        checkPrint: false,
                    }

                    const newDate = new Date()
                    const checkDate = moment(newDate).toDate()
                    let matchDate = {
                        startDate: { $lte: checkDate },
                        endDate: { $gte: checkDate },
                        type: 'Item',
                    }
                    let matchItem = {
                        'products.productId': detail.itemId,
                    }

                    let discount = Discounts.aggregate([{
                            $match: matchDate,
                        },
                        {
                            $unwind: {
                                path: '$products',
                                preserveNullAndEmptyArrays: true,
                            },
                        },
                        {
                            $match: matchItem,
                        },
                        {
                            $group: {
                                _id: '$_id',
                                eventName: { $last: '$eventName' },
                                startDate: { $last: '$startDate' },
                                endDate: { $last: '$endDate' },
                                discountValue: { $last: '$discountValue' },
                                discountType: { $last: '$discountType' },
                                type: { $last: '$type' },
                                products: { $last: '$products' },
                            },
                        },
                    ])

                    let discountDoc = null
                    let checkTime = moment(newDate).format('HH:mm:ss')

                    _.forEach(discount, o => {
                        let startDate = moment(o.startDate).format('HH:mm:ss')
                        let endDate = moment(o.endDate).format('HH:mm:ss')

                        if (
                            Date.parse('01 Jan 2019 ' + checkTime) >=
                            Date.parse('01 Jan 2019 ' + startDate) &&
                            Date.parse('01 Jan 2019 ' + checkTime) <=
                            Date.parse('01 Jan 2019 ' + endDate)
                        ) {
                            discountDoc = o
                        }
                    })

                    // Check item, price and discount exist
                    let selector = {
                        invoiceId: detail.invoiceId,
                        itemId: detail.itemId,
                        price: detail.price,
                        discount: detail.discount,
                        checkPrint: detail.checkPrint,
                        extraItemDoc: { $exists: false },
                        child: { $exists: false },
                    }
                    if (!_.isEmpty(discountDoc)) {
                        selector.discount = discountDoc.products.discount
                    }
                    // check exist
                    let checkExist = SaleDetails.findOne(selector)
                        // if exist
                    if (checkExist) {
                        let discountEachPrice =
                            (checkExist.price * checkExist.discount) / 100
                        let priceAfterDiscount = parseFloat(
                            checkExist.price - discountEachPrice
                        ).toFixed(2)
                        priceAfterDiscount = parseFloat(priceAfterDiscount)
                        let detailAmount = parseFloat(
                            priceAfterDiscount * detail.qty
                        ).toFixed(2)
                        detailAmount = parseFloat(detailAmount)

                        SaleDetails.update({ _id: checkExist._id }, {
                                $inc: {
                                    qty: detail.qty,
                                    amount: detailAmount,
                                },
                            },
                            error => {
                                if (!error) {
                                    Sales.update({ _id: invoiceId }, { $inc: { total: detailAmount } })
                                }
                            }
                        )
                    } else {
                        if (discountDoc) {
                            let discountVal =
                                (discountDoc.products.discount * detail.price) / 100
                            let amount = parseFloat(
                                (detail.price - discountVal) * detail.qty
                            ).toFixed(2)
                            amount = parseFloat(amount)
                            detail.discount = discountDoc.products.discount
                            detail.amount = amount
                        } else {
                            // Amount
                            detail.amount = (detail.qty * detail.price).toFixed(2)
                            detail.amount = parseFloat(detail.amount)
                        }

                        // Insert
                        SaleDetails.insert(detail, error => {
                            if (!error) {
                                Sales.update({ _id: invoiceId }, { $inc: { total: detail.amount } })
                            }
                        })
                    }
                })
            }

            return doc
        } catch (error) {
            // Decrement seq
            throwError(error)
        }
    }

    /*===========
      ----updateSaleDetail-----
    =============*/
    static updateSaleDetail(details, invoiceData) {

        try {
            _.forEach(details, o => {
                    // Update extra items
                    if (o.extraItemDoc && o.extraItemDoc.length) {
                        // Foreach extra doc
                        o.extraItemDoc.forEach(e => {
                                // Find extra detail
                                let extraDoc = SaleDetails.findOne({ _id: e._id })
                                if (extraDoc) {
                                    extraDoc.qty = o.qty
                                    let discountEachPrice = (extraDoc.price * extraDoc.discount) / 100
                                    let priceAfterDiscount = parseFloat(
                                        extraDoc.price - discountEachPrice
                                    ).toFixed(2)
                                    priceAfterDiscount = parseFloat(priceAfterDiscount)
                                    extraDoc.amount = extraDoc.qty * priceAfterDiscount

                                    // Update extra detail
                                    SaleDetails.update({ _id: extraDoc._id }, { $set: extraDoc })
                                }
                            })
                            // Update detail
                        SaleDetails.update({ _id: o._id }, { $set: o })
                    } else {
                        // Update detail
                        SaleDetails.update({ _id: o._id }, { $set: o })
                    }
                })
                // Update total
            Sales.update({ _id: invoiceData._id }, { $set: { total: invoiceData.total } })

            return invoiceData._id
        } catch (error) {
            throwError(error)
        }
    }

    /*===========
  ----cancelCopySale-----
=============*/
    static cancelCopySale(selector, modifier, options, callback) {

        let doc = _.omit(modifier, ['details']);
        let details = modifier.details;

        const invoiceId = getNextSeq({
            // Mandatory
            filter: {
                _id: 'rest_sales',
            },
            // Optional
            opts: {
                seq: 1,
                paddingType: 'start',
                paddingLength: 4,
                paddingChar: '0',
                prefix: moment(doc.date).format('YYMMDD'),
            },
        })
        try {
            const currentInvoice = doc.refId
                // Update invoice status
            Sales.update({ _id: currentInvoice }, { $set: { status: 'Cancel', 'statusDate.close': doc.date } }, callback)

            // Insert a copy invoice
            doc._id = invoiceId.toString()
            Sales.insert(doc, error => {
                if (!error) {
                    if (details && details.length) {
                        _.forEach(details, o => {
                            o.invoiceId = doc._id
                            delete o._id
                            SaleDetails.insert(o)
                        })
                    }
                }
            }, callback)

            return doc._id
        } catch (error) {
            // Decrement seq
            getNextSeq({
                filter: { _id: 'rest_sales' },
                opts: { seq: -1 },
            })
            throwError(error)
        }
    }

    /*===========
  ----removeSale-----
=============*/
    static removeSale(selector, callback) {

        try {
            return Sales.remove({ _id: selector._id }, callback, error => {
                if (!error) {
                    SaleDetails.remove({ invoiceId: selector._id })
                }
            })

        } catch (error) {
            throwError(error)
        }
    }

    /*===========
  ----removeSaleDetail-----
=============*/
    static removeSaleDetail(details, invoiceData) {
        try {
            // Foreach detail
            details.forEach(o => {
                    SaleDetails.remove({ _id: o._id }, error => {
                        if (!error) {
                            if (o.extraItemDoc && o.extraItemDoc.length) {
                                o.extraItemDoc.forEach(e => {
                                    SaleDetails.remove({ _id: e._id })
                                })
                            }
                        }
                    })
                })
                // Update total
            Sales.update({ _id: invoiceData._id }, { $set: { total: invoiceData.total } })
            return invoiceData._id
        } catch (error) {
            throwError(error)
        }
    }

    /*===========
  ----removeEmptySale-----
=============*/
    static removeEmptySale(selector, callback) {
        try {
            let saleDoc = Sales.find(selector).fetch()
            _.forEach(saleDoc, o => {
                if (
                    SaleDetails.find({ invoiceId: o._id }).count() === 0 &&
                    o.numOfGuest === 0
                ) {
                    Sales.remove({ _id: o._id }, callback)
                }
            })

            return 'success'
        } catch (error) {
            throwError(error)
        }
    }
}