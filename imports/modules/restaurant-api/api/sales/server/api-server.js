import { SALES } from '../methods'

JsonRoutes.add('get', '/find_sales/:selector/:options', function(
    req,
    res,
    next
) {
    res.charset = 'utf-8'
    const selector = req.params.selector ? JSON.parse(req.params.selector) : {}
    const options = req.params.options ? JSON.parse(req.params.options) : {}
    let data = {}
    data.result = SALES.findSales(selector, options)
    data.code = '200'
    JsonRoutes.sendResult(res, {
        data: data,
    })
})

JsonRoutes.add('get', '/find_sale_aggregate/:selector/:options', function(
    req,
    res,
    next
) {
    res.charset = 'utf-8'
    const selector = req.params.selector ? JSON.parse(req.params.selector) : {}
    const options = req.params.options ? JSON.parse(req.params.options) : {}
    let data = {}
    data.result = SALES.findSaleAggregate(selector, options)
    data.code = '200'
    JsonRoutes.sendResult(res, {
        data: data,
    })
})

JsonRoutes.add('post', '/insert_sale', function(req, res, next) {
    res.charset = 'utf-8'
    const doc = req.body ? req.body : {}

    let data = {}
    data.result = SALES.insertSale(doc)
    data.code = '200'
    JsonRoutes.sendResult(res, {
        data: data,
    })
})

JsonRoutes.add('post', '/insert_sale_detail', function(req, res, next) {
    res.charset = 'utf-8'
    const doc = req.body ? req.body : {}
    let data = {}
    data.result = SALES.insertSaleDetail(doc)
    data.code = '200'
    JsonRoutes.sendResult(res, {
        data: data,
    })
})

JsonRoutes.add('post', '/insert_sale_detail_extra', function(
    req,
    res,
    next
) {
    res.charset = 'utf-8'
    const doc = req.body[0] ? req.body[0] : {}
    const itemList = req.body[1] ? req.body[1] : []
    let data = {}
    data.result = SALES.insertSaleDetailExtra(doc, itemList)
    data.code = '200'
    JsonRoutes.sendResult(res, {
        data: data,
    })
})

JsonRoutes.add('post', '/insert_sale_detail_list', function(
    req,
    res,
    next
) {
    res.charset = 'utf-8'
    const doc = req.body ? req.body : {}
    let data = {}
    data.result = SALES.insertSaleDetailList(doc)
    data.code = '200'
    JsonRoutes.sendResult(res, {
        data: data,
    })
})

JsonRoutes.add('put', '/update_sale/:selector/:options', function(
    req,
    res,
    next
) {
    res.charset = 'utf-8'
    const selector = req.params.selector ? JSON.parse(req.params.selector) : {}
    const modifier = req.body
    const options = req.params.options ? JSON.parse(req.params.options) : {}
    SALES.updateSale(selector, modifier, options, (error, result) => {
        let data = {}
        if (error) {
            data.code = '403'
            data.msg = error.message
            data.result = ''
        } else {
            data.code = result ? '201' : '304'
            data.result = result
        }
        JsonRoutes.sendResult(res, {
            data: data,
        })
    })
})

JsonRoutes.add('put', '/update_sale_detail', function(
    req,
    res,
    next
) {
    res.charset = 'utf-8'
    const details = req.body[0] ? req.body[0] : []
    const invoiceData = req.body[1] ? req.body[1] : {}
    let data = {}
    data.result = SALES.updateSaleDetail(details, invoiceData)
    data.code = '200'
    JsonRoutes.sendResult(res, {
        data: data,
    })
})

JsonRoutes.add('put', '/cancel_copy_sale/:selector/:options', function(
    req,
    res,
    next
) {
    res.charset = 'utf-8'
    const selector = req.params.selector ? JSON.parse(req.params.selector) : {}
    const modifier = req.body
    const options = req.params.options ? JSON.parse(req.params.options) : {}
    SALES.cancelCopySale(selector, modifier, options, (error, result) => {
        let data = {}
        if (error) {
            data.code = '403'
            data.msg = error.message
            data.result = ''
        } else {
            data.code = result ? '201' : '304'
            data.result = result
        }
        JsonRoutes.sendResult(res, {
            data: data,
        })
    })
})

JsonRoutes.add('delete', '/remove_sale/:selector', function(req, res, next) {
    res.charset = 'utf-8'
    const selector = req.params.selector ? JSON.parse(req.params.selector) : {}

    SALES.removeSale(selector, (error, result) => {
        let data = {}
        if (error) {
            data.code = '403'
            data.msg = error.message
            data.result = ''
        } else {
            data.code = result ? '201' : '304'
            data.result = result
        }
        JsonRoutes.sendResult(res, {
            data: data,
        })
    })
})

JsonRoutes.add('put', '/remove_sale_detail', function(
    req,
    res,
    next
) {
    res.charset = 'utf-8'
    const details = req.body[0] ? req.body[0] : []
    const invoiceData = req.body[1] ? req.body[1] : {}
    let data = {}
    data.result = SALES.removeSaleDetail(details, invoiceData)
    data.code = '200'
    JsonRoutes.sendResult(res, {
        data: data,
    })
})

JsonRoutes.add('delete', '/remove_empty_sale/:selector', function(
    req,
    res,
    next
) {
    res.charset = 'utf-8'
    const selector = req.params.selector ? JSON.parse(req.params.selector) : {}

    SALES.removeEmptySale(selector, (error, result) => {
        let data = {}
        if (error) {
            data.code = '403'
            data.msg = error.message
            data.result = ''
        } else {
            data.code = result ? '201' : '304'
            data.result = result
        }

        JsonRoutes.sendResult(res, {
            data: data,
        })
    })
})