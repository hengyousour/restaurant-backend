import _ from 'lodash'
// Category
import Categories from '../../../rest/api/categories/collection'
// Product
import Products from '../../../rest/api/products/collection'

export class CATEGORIES {
    // Find
    static findCategories(selector, options) {
        let data = Categories.find(selector, options).fetch()
            // Remove empty categories
        _.remove(data, o => {
            let existProduct = Products.findOne({ categoryId: o._id })
            if (!existProduct) {
                return o._id == o._id
            }
        })

        return data
    }
}