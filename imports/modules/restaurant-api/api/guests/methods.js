// Guests
import Guests from '../../../rest/api/guests/guests'

export class GUESTS {
    // Find
    static findOneGuest(selector, options) {
        return Guests.findOne(selector, options)
    }
}