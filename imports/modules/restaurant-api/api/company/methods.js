import _ from 'lodash'

import Company from '../../../../api/company/company'

export class COMPANY {
    // Find One
    static findOneCompany(selector, options) {
        return Company.findOne(selector, options)
    }
}