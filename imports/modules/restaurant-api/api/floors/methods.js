import _ from 'lodash'
import Floors from '../../../rest/api/floors/floors'
export class FLOORS {
    static findFloors(selector, options) {
        return Floors.find(selector, options).fetch()
    }
    static findOneFloor(selector, options) {
        return Floors.findOne(selector, options)
    }
}