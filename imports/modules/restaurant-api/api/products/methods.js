import _ from 'lodash'
import moment from 'moment'
// Product
import Products from '../../../rest/api/products/collection'
// Dish details
import Images from '../../../../api/files/files'
import Discounts from '../../../rest/api/discounts/collection'
import Company from '/imports/api/company/company'
import Exchanges from '/imports/api/exchanges/exchanges'

export class PRODUCTS {
    // findProductForSale
    static findProducts(selector, options) {
        let query = {}
        if (!_.isEmpty(selector.name)) {
            query.name = { $regex: selector.name, $options: 'i' }
        }

        if (!_.isEmpty(selector) && selector.categoryId != '') {
            query.categoryId = selector.categoryId
        }

        if (!_.isEmpty(selector) && selector.type != '') {
            query.type = selector.type
        }

        // Product
        let data = Products.find(query, options).fetch()
            // Exchange
        let exchangeDoc = Exchanges.findOne()
            // Company
        let companyDoc = Company.findOne()

        const newDate = new Date()
        const checkDate = moment(newDate).toDate()
        let matchDate = {
            startDate: { $lte: checkDate },
            endDate: { $gte: checkDate },
            type: 'Item',
        }

        data = _.sortBy(data, [
            function(item) {
                // Assign exchange
                item.exchangeDoc = exchangeDoc
                    // Assign baseCurrency
                item.baseCurrency = companyDoc.accounting.baseCurrency

                let matchItem = {
                    'products.productId': item._id,
                }

                let discount = Discounts.aggregate([{
                        $match: matchDate,
                    },
                    {
                        $unwind: {
                            path: '$products',
                            preserveNullAndEmptyArrays: true,
                        },
                    },
                    {
                        $match: matchItem,
                    },
                    {
                        $group: {
                            _id: '$_id',
                            eventName: { $last: '$eventName' },
                            startDate: { $last: '$startDate' },
                            endDate: { $last: '$endDate' },
                            type: { $last: '$type' },
                            products: { $last: '$products' },
                        },
                    },
                ])
                let discountRevers = _.reverse(discount)
                let discountDoc = null
                let checkTime = moment(newDate).format('HH:mm:ss')
                _.forEach(discountRevers, o => {
                    let startDate = moment(o.startDate).format('HH:mm:ss')
                    let endDate = moment(o.endDate).format('HH:mm:ss')

                    if (
                        Date.parse('01 Jan 2019 ' + checkTime) >=
                        Date.parse('01 Jan 2019 ' + startDate) &&
                        Date.parse('01 Jan 2019 ' + checkTime) <=
                        Date.parse('01 Jan 2019 ' + endDate)
                    ) {
                        discountDoc = o
                    }
                })

                if (!_.isEmpty(discountDoc)) {
                    item.discounts = discountDoc.products
                }

                if (!_.isEmpty(item.photo) && Images.findOne({ _id: item.photo })) {
                    Images.findOne({ _id: item.photo })
                    let photoUrl = Images.findOne({ _id: item.photo }).url()
                    if (!_.isEmpty(photoUrl)) {
                        item.photoUrl = photoUrl
                    }
                }
                return item.name
            },
        ])

        //  Sort floors
        data.sort(function(a, b) {
            return a.name.localeCompare(b.name, undefined, {
                numeric: true,
                sensitivity: 'base',
            })
        })
        let dataCount = Products.find(selector).count()
        return { data, dataCount }
    }
}