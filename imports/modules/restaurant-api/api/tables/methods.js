import _ from 'lodash'
// Table
import Tables from '../../../rest/api/tables/tables'
// Floor
import Floors from '../../../rest/api/floors/floors'
// Sale
import Sales from '../../../rest/api/sales/sales'

export class TABLES {
    static findTableAndFloor(selector, options) {
        // Find data
        let doc = Floors.aggregate([
            { $match: selector },
            {
                $lookup: {
                    from: 'rest_tables',
                    localField: '_id',
                    foreignField: 'floorId',
                    as: 'tableDoc',
                },
            },
            {
                $unwind: {
                    path: '$tableDoc',
                    preserveNullAndEmptyArrays: true,
                },
            },
            {
                $group: {
                    _id: { tableId: '$tableDoc._id', floorId: '$_id' },
                    floorId: { $last: '$_id' },
                    name: { $last: '$name' },
                    tableDoc: { $last: '$tableDoc' },
                },
            },
            {
                $group: {
                    _id: '$_id.floorId',
                    name: { $last: '$name' },
                    tableDocs: {
                        $push: {
                            _id: '$tableDoc._id',
                            floorId: '$tableDoc._floorId',
                            name: '$tableDoc.name',
                            numOfGuest: '$tableDoc.numOfGuest',
                            floorName: '$name',
                        },
                    },
                },
            },
        ])

        let filterFloors = [],
            filterTables = []

        //  Sort floors
        doc.sort(function(a, b) {
            return a.name.localeCompare(b.name, undefined, {
                numeric: true,
                sensitivity: 'base',
            })
        })

        // Finish aggregate
        _.forEach(doc, o => {
            // Remove undefined
            _.remove(o.tableDocs, function(tableDoc) {
                return tableDoc._id === undefined
            })

            // Foreach to get sale doc
            _.forEach(o.tableDocs, tableProp => {
                if (!_.isEmpty(tableProp._id)) {
                    let saleDoc = Sales.find({
                        tableId: tableProp._id,
                        status: 'Open',
                    }).fetch()

                    if (saleDoc.length > 0) {
                        tableProp.total = _.sumBy(saleDoc, function(prop) {
                            return prop.total
                        })
                    } else if (saleDoc.length == 0) {
                        tableProp.total = 0
                    }
                    tableProp.guestCount = _.sumBy(saleDoc, function(prop) {
                        return prop.numOfGuest
                    })
                }
            })

            // Push doc
            filterTables.push(o.tableDocs)

            // Sort tables
            o.tableDocs.sort(function(a, b) {
                return a.name.localeCompare(b.name, undefined, {
                    numeric: true,
                    sensitivity: 'base',
                })
            })
        })

        // Assign
        filterTables = _.flatten(filterTables)
        filterFloors = doc
            // RETURn
        return { filterFloors, filterTables }
    }

    static findTables(selector, options) {
        return Tables.find(selector, options).fetch()
    }

    static findOneTable(tableId) {
        // Find data
        let data = Tables.aggregate([
            { $match: { _id: tableId } },
            {
                $lookup: {
                    from: 'rest_floors',
                    localField: 'floorId',
                    foreignField: '_id',
                    as: 'floorDoc',
                },
            },
            {
                $unwind: {
                    path: '$floorDoc',
                    preserveNullAndEmptyArrays: true,
                },
            },
            {
                $addFields: {
                    floorName: '$floorDoc.name',
                    tableName: '$name',
                },
            },
            { $project: { floorName: 1, tableName: 1 } },
        ])

        return data[0]
    }
}