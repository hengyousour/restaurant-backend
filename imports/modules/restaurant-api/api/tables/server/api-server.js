import { TABLES } from '../methods'

// ** Use in table page ** //
JsonRoutes.add('get', '/find_table_and_floor/:selector/:options', function(
    req,
    res,
    next
) {
    res.charset = 'utf-8'
    const selector = req.params.selector ? JSON.parse(req.params.selector) : {}
    const options = req.params.options ? JSON.parse(req.params.options) : {}
    let data = {}
    data.result = TABLES.findTableAndFloor(selector, options)
    data.code = '200'
    JsonRoutes.sendResult(res, {
        data: data,
    })
})
JsonRoutes.add('get', '/find_tables/:selector/:options', function(
    req,
    res,
    next
) {
    res.charset = 'utf-8'
    const selector = req.params.selector ? JSON.parse(req.params.selector) : {}
    const options = req.params.options ? JSON.parse(req.params.options) : {}
    let data = {}
    data.result = TABLES.findTables(selector, options)
    data.code = '200'
    JsonRoutes.sendResult(res, {
        data: data,
    })
})

JsonRoutes.add('get', '/find_one_table/:selector/:options', function(
    req,
    res,
    next
) {
    res.charset = 'utf-8'
    const selector = req.params.selector ? JSON.parse(req.params.selector) : {}
    const options = req.params.options ? JSON.parse(req.params.options) : {}

    let data = {}
    data.result = TABLES.findOneTable(selector, options)
    data.code = '200'
    JsonRoutes.sendResult(res, {
        data: data,
    })
})