import Vue from 'vue'

// Router
import VueRouter from 'vue-router'
Vue.use(VueRouter)

// Vuex
import Vuex from 'vuex'
Vue.use(Vuex)

// Element UI
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import i18n from './i18n'
Vue.use(ElementUI, {
  size: 'small',
  i18n: (key, value) => i18n.t(key, value),
})

// Data table
import { DataTables, DataTablesServer } from 'vue-data-tables'
Vue.use(DataTables)
Vue.use(DataTablesServer)

// Tracker
import VueMeteorTracker from 'vue-meteor-tracker'
Vue.use(VueMeteorTracker)
Vue.config.meteor.freeze = true

// Vue Google Map
import VueGoogleMaps from 'vue-googlemaps'
import 'vue-googlemaps/dist/vue-googlemaps.css'
Vue.use(VueGoogleMaps, {
  load: {
    apiKey: 'AIzaSyBVH-aNNlZKmjRYxjt_2c0-m5AxYhF-iqU',
    libraries: ['places'],
  },
})

// Meta
import Meta from 'vue-meta'
Vue.use(Meta)

// NProgress
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'

// Vue Progressbar
import VueProgressBar from 'vue-progressbar'
const vProgressOpts = {
  color: '#409eff',
  failedColor: '#f56c6c',
  thickness: '3px',
  transition: {
    speed: '0.2s',
    opacity: '0.6s',
    termination: 300,
  },
  autoRevert: true,
  location: 'top',
  inverse: false,
}
Vue.use(VueProgressBar, vProgressOpts)

// Json Excel
// import JsonExcel from 'vue-json-excel';
// Vue.component('downloadExcel', JsonExcel);

// Font Awesome 5
import '/imports/ui/styles/fontawesome-all.css'
// import FontAwesomeIcon from '@fortawesome/vue-fontawesome';
// import fontawesome from '@fortawesome/fontawesome'
// import solid from '@fortawesome/fontawesome-free-solid'
// import regular from '@fortawesome/fontawesome-free-regular'
// import brands from '@fortawesome/fontawesome-free-brands'
// fontawesome.library.add(solid, regular, brands)

// Clipboard
import VueClipboard from 'vue-clipboard2'
Vue.use(VueClipboard)

// Local Plugin
import VueLodash from '/imports/ui/plugins/vue-lodash'
Vue.use(VueLodash)
import VueMoment from '/imports/ui/plugins/vue-moment'
Vue.use(VueMoment)
import VueNumeral from '/imports/ui/plugins/vue-numeral'
Vue.use(VueNumeral)
