//------ Module -----
import LoginLayout from '/imports/ui/layouts/LoginLayout.vue'
import MainLayout from '/imports/ui/layouts/MainLayout.vue'
import SaleLayout from '/imports/modules/rest/ui/layouts/SaleLayout.vue'

const layout = {
  login: LoginLayout,
  main: MainLayout,
  sale: SaleLayout,
}

export default layout
