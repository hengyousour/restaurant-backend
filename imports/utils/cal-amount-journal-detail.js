import SimpleSchema from 'simpl-schema'

// Collection
import ChartAccounts from '../api/chart-accounts/chart-accounts'

const calAmountOfJournalDetail = function ({chartAccountId, dr, cr}) {
    new SimpleSchema({
        chartAccountId: String,
        dr: {
            type: Number,
            min: 0,
        },
        cr: {
            type: Number,
            min: 0,
        },
    }).validator({chartAccountId, dr, cr})

    let doc = ChartAccounts.aggregate([
        {
            $match: {_id: chartAccountId},
        },
        {
            $lookup: {
                from: 'accountTypes',
                localField: 'accountTypeId',
                foreignField: '_id',
                as: 'accountTypeDoc',
            },
        },
        {
            $unwind: '$accountTypeDoc',
        },
    ])[0]

    let amount = 0
    if (doc && doc.accountTypeDoc.nature) {
        switch (doc.accountTypeDoc.nature) {
            case 'Asset' || 'Expense':
                if (dr > 0) {
                    cr = 0
                    amount = dr
                } else {
                    if (cr > 0) {
                        dr = 0
                        amount = -cr
                    } else {
                        dr = 0
                        cr = 0
                        amount = 0
                    }
                }
                break
            case 'Liability' || 'Equity' || 'Revenue':
                if (dr > 0) {
                    cr = 0
                    amount = -dr
                } else {
                    if (cr > 0) {
                        dr = 0
                        amount = cr
                    } else {
                        dr = 0
                        cr = 0
                        amount = 0
                    }
                }
                break
        }

        return {doc, dr, cr, amount}
    }

    return null
}

export default calAmountOfJournalDetail
