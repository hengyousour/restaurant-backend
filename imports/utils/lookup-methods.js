import { Meteor } from 'meteor/meteor'
import { Roles } from 'meteor/alanning:roles'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import { check } from 'meteor/check'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'

// Lib
import { userIsInRole } from './security'
import rateLimit from './rate-limit'

// Collection
import Branches from '../api/branches/branches'
import AccountType from '../api/account-types/account-types'
import ChartAccounts from '../api/chart-accounts/chart-accounts'
import Classifications from '../api/classifications/classifications'

// Roles
export const lookupRole = new ValidatedMethod({
  name: 'app.lookupRole',
  mixins: [CallPromiseMixin],
  validate: null,
  run() {
    if (Meteor.isServer) {
      Meteor._sleepForMs(100)

      // Check roles
      let notEqualSelector = ['super']
      if (!Roles.userIsInRole(Meteor.userId(), 'super')) {
        notEqualSelector.push('admin')
      }

      let list = []
      let data = Meteor.roles.find(
        { name: { $nin: notEqualSelector } },
        { sort: { name: 1 } }
      )
      data.forEach(o => {
        list.push({ label: o.name, value: o.name })
      })

      return list
    }
  },
})

// Branch
export const lookupBranch = new ValidatedMethod({
  name: 'app.lookupBranch',
  mixins: [CallPromiseMixin],
  validate(selector) {
    selector = selector || {}
    check(selector, Object)
  },
  run(selector = {}) {
    if (Meteor.isServer) {
      Meteor._sleepForMs(100)
      selector = selector || {}

      let list = []
      let data = Branches.find(selector, { sort: { _id: 1 } })
      data.forEach(o => {
        list.push({ label: o.name, value: o._id })
      })

      return list
    }
  },
})

// Account Type
export const lookupAccountType = new ValidatedMethod({
  name: 'app.lookupAccountType',
  mixins: [CallPromiseMixin],
  validate(selector) {
    selector = selector || {}
    check(selector, Object)
  },
  run(selector) {
    if (Meteor.isServer) {
      Meteor._sleepForMs(100)
      selector = selector || {}
      let sort = { code: 1 }

      let list = []
      let data = AccountType.find(selector, { sort })
      data.forEach(o => {
        let custom = `
                    <span class="custom-select-opts-left">${o.name}</span>
                    <span class="custom-select-opts-right">${o.nature}</span>
                    `
        list.push({
          label: o.name,
          value: o._id,
          labelCustom: custom,
          memo: o.memo,
        })
      })

      return list
    }
  },
})

// Chart Account
export const lookupChartAccount = new ValidatedMethod({
  name: 'app.lookupChartAccount',
  mixins: [CallPromiseMixin],
  validate(selector) {
    selector = selector || {}
    check(selector, Object)
  },
  run(selector) {
    if (Meteor.isServer) {
      Meteor._sleepForMs(100)
      selector = selector || {}
      let sort = { order: 1 }

      let list = []
      let doc = ChartAccounts.aggregate([
        {
          $match: selector,
        },
        {
          $lookup: {
            from: 'accountTypes',
            localField: 'accountTypeId',
            foreignField: '_id',
            as: 'accountTypeDoc',
          },
        },
        {
          $unwind: '$accountTypeDoc',
        },
        {
          $sort: sort,
        },
      ])

      doc.forEach(o => {
        let indent = ''
        let parent = o.accountTypeDoc.name

        // Check Parent
        if (o.parent) {
          let countAncestors = (o.ancestors && o.ancestors.length) || 0
          indent = _.repeat('&nbsp;', countAncestors * 3)
          parent = `-> ` + _.find(doc, { _id: o.parent }).name
        }

        let customLabel = `
                            <span class="custom-select-opts-left">
                            ${indent}${o.code} : ${o.name}
                            </span>
                            <span class="custom-select-opts-right">${parent}</span>
                            `

        list.push({
          label: `${o.code} : ${o.name}`,
          value: o._id,
          labelCustom: customLabel,
        })
      })

      return list
    }
  },
})

// Classification
export const lookupClassification = new ValidatedMethod({
  name: 'app.lookupClassification',
  mixins: [CallPromiseMixin],
  validate(selector) {
    selector = selector || {}
    check(selector, Object)
  },
  run(selector) {
    if (Meteor.isServer) {
      Meteor._sleepForMs(100)
      selector = selector || {}
      let sort = { code: 1 }

      let list = []
      let data = Classifications.find(selector, { sort })
      data.forEach(o => {
        list.push({ label: o.name, value: o._id })
      })

      return list
    }
  },
})

rateLimit({
  methods: [],
})
