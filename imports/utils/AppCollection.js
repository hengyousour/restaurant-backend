import { Meteor } from 'meteor/meteor'
import { Mongo } from 'meteor/mongo'
import SimpleSchema from 'simpl-schema'

class AppCollection extends Mongo.Collection {
  // Current user
  _getUserId() {
    return Meteor.userId() // Don't work on fixture startup
  }

  // Timestamp
  insert(doc, callback) {
    const schema = new SimpleSchema({
      createdAt: {
        type: Date,
        optional: true,
      },
      createdBy: {
        type: String,
        optional: true,
      },
    })
    this.attachSchema(schema)
    doc.createdAt = doc.createdAt || new Date()
    doc.createdBy = doc.createdBy || this._getUserId()

    return super.insert(doc, callback)
  }

  update(selector, modifier, ...rest) {
    const schema = new SimpleSchema({
      updatedAt: {
        type: Date,
        optional: true,
      },
      updatedBy: {
        type: String,
        optional: true,
      },
    })
    this.attachSchema(schema)
    modifier.$set = modifier.$set || {}
    modifier.$set.updatedAt = modifier.$set.updatedAt || new Date()
    modifier.$set.updatedBy = modifier.$set.updatedBy || this._getUserId()

    return super.update(selector, modifier, ...rest)
  }

  // Soft Remove
  softRemove2(selector, callback) {
    const schema = new SimpleSchema({
      removed: {
        type: Boolean,
        optional: true,
      },
      removedAt: {
        type: Date,
        optional: true,
      },
      removedBy: {
        type: String,
        optional: true,
      },
    })
    // this.attachSchema(schema, { selector: { type: 'softRemove' } })
    this.attachSchema(schema)

    const modifier = {
      $set: {
        removed: true,
        removedAt: new Date(),
        removedBy: this._getUserId(),
      },
    }
    return this.update(
      selector,
      modifier,
      {
        multi: true,
        // selector: { type: 'softRemove' },
      },
      callback
    )
  }

  // Restore
  restore2(selector, callback) {
    const schema = new SimpleSchema({
      removed: {
        type: Boolean,
        optional: true,
      },
      restoredAt: {
        type: Date,
        optional: true,
      },
      restoreBy: {
        type: String,
        optional: true,
      },
    })
    // this.attachSchema(schema, {selector: {type: 'restore'}});
    this.attachSchema(schema)

    const modifier = {
      $set: {
        removed: false,
        restoredAt: new Date(),
        restoreBy: this._getUserId(),
      },
    }

    return this.update(
      selector,
      modifier,
      {
        multi: true,
        // selector: {type: 'restore'}
      },
      callback
    )
  }
}

export default AppCollection
