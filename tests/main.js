import { Meteor } from 'meteor/meteor'

/**
 * Server
 */
if (Meteor.isServer) {
  require('/tests/server')
  require('/imports/modules/blog/tests/server')
}

// Client
if (Meteor.isClient) {
  require('/tests/client')
  require('/imports/modules/blog/tests/client')
}
